package tools.mapEditor.Components.TexturePanelComponents.ComboBoxes;

import javax.swing.*;
import java.awt.*;

public class TextureComboBox extends JComboBox<String> {

    /**
     * Creates the combobox that is used.
     */
    public TextureComboBox() {
        setMaximumSize(new Dimension(200, 24));
    }
}
