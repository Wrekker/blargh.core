package tools.mapEditor.Components.Menus;

import tools.mapEditor.EditorEvents.MenuArgs;
import engine.events.Event;
import engine.events.ISubscriber;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class MainFrameMenu extends JMenuBar {

    private MainFrameMenu _thisMenu = this;
    private Event<MenuArgs> _menuArgsEvent = new Event<MenuArgs>();

    public MainFrameMenu() {
        this.add(createFileMenu());
    }

    /**
     * Creates the file tab of the menu, also listens to menuevents.
     *
     * @return Returns the file tab of the menu.
     */
    private JMenu createFileMenu() {
        JMenu File = new JMenu("File");
        File.setMnemonic('F');
        JMenuItem New = new JMenuItem("New", 'N');
        JMenuItem Load = new JMenuItem("Load", 'L');
        JMenuItem Save = new JMenuItem("Save", 'S');
        JMenuItem Exit = new JMenuItem("Exit", 'X');

        New.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                _menuArgsEvent.raiseEvent(new MenuArgs(_thisMenu, "New"));
            }
        });

        Load.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                _menuArgsEvent.raiseEvent(new MenuArgs(_thisMenu, "Load"));
            }
        });

        Save.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                _menuArgsEvent.raiseEvent(new MenuArgs(_thisMenu, "Save"));
            }
        });

        Exit.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                _menuArgsEvent.raiseEvent(new MenuArgs(_thisMenu, "Exit"));
            }
        });

        File.add(New);
        File.add(Load);
        File.add(Save);
        File.addSeparator();
        File.add(Exit);
        return File;
    }

    public void addMenuListener(ISubscriber<MenuArgs> listener){
        _menuArgsEvent.addSubscriber(listener);
    }
}
