package tools.mapEditor.Components.Panels;

import engine.resources.bitmap.BitmapLoader;
import tools.mapEditor.Components.TexturePanelComponents.Buttons.EditorButton;
import tools.mapEditor.Components.TexturePanelComponents.ComboBoxes.TextureComboBox;
import tools.mapEditor.EditorEvents.ButtonArgs;
import tools.mapEditor.EditorEvents.ChangeDrawArgs;
import tools.mapEditor.EditorEvents.TextureChangeArgs;
import engine.events.Event;
import engine.events.ISubscriber;

import javax.swing.*;
import javax.swing.border.BevelBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

/**
 * Created with IntelliJ IDEA.
 * User: Andreas
 * Date: 2012-09-12
 * Time: 19:18
 * To change this template use File | Settings | File Templates.
 */
public class TexturePanel extends JPanel {

    private TexturePanel _thisPanel = this;
    private DrawArgsButtonPanel _drawArgsButtonPanel = new DrawArgsButtonPanel();
    private EditorButton _cleanButton = new EditorButton("Clean");
    private EditorButton _browseButton = new EditorButton("Browse...");
    private PictureBoxPanel _pictureBoxPanel = new PictureBoxPanel();
    private JPanel _cleanBrowsePanel = new JPanel();
    private TextureComboBox _textureComboBox = new TextureComboBox();
    private BitmapLoader _bitmapLoader;

    private Event<ButtonArgs> _cleanMapArgsEvent = new Event<ButtonArgs>();
    private Event<ButtonArgs> _browseArgsEvent = new Event<ButtonArgs>();
    private Event<TextureChangeArgs> _textureChangeArgsEvent = new Event<TextureChangeArgs>();

    /**
     * Constructor, it also has som struts which makes it a bit more handsome.
     */
    public TexturePanel(BitmapLoader bitmapLoader) {
        _bitmapLoader = bitmapLoader;
        setBackground(Color.gray);
        setBorder(new BevelBorder(BevelBorder.RAISED, Color.gray, Color.gray));
        buttonPanelInit();
        setLayout(new BoxLayout(_thisPanel, BoxLayout.Y_AXIS));
    }

    public void initialize(int width, int height) {
        setPreferredSize(new Dimension(width, height));
        add(Box.createVerticalGlue());
        add(_pictureBoxPanel, TOP_ALIGNMENT);
        add(Box.createVerticalGlue());
        add(Box.createVerticalStrut(400));
        add(_drawArgsButtonPanel);
        add(_textureComboBox);
        addComboBoxAction();
        addBrowseButtonAction();
        addCleanButtonAction();
        add(_cleanBrowsePanel);
    }

    public void buttonPanelInit(){
        _cleanBrowsePanel.setPreferredSize(new Dimension(_browseButton.getWidth()*2, 24));
        _cleanBrowsePanel.setLayout(new GridLayout(1,2));
        _cleanBrowsePanel.add(_browseButton);
        _cleanBrowsePanel.add(_cleanButton);
    }

    private void addCleanButtonAction() {
        _cleanButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                _cleanMapArgsEvent.raiseEvent(new ButtonArgs(_thisPanel, "Clean"));
            }
        });
    }

    private void addBrowseButtonAction() {
        _browseButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                _browseArgsEvent.raiseEvent(new ButtonArgs(_thisPanel, "Browse"));
            }
        });
    }

    private void addComboBoxAction() {
        _textureComboBox.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
                _textureChangeArgsEvent.raiseEvent(new TextureChangeArgs(
                        _thisPanel,
                        _textureComboBox.getSelectedItem().toString(),
                        _textureComboBox.getSelectedIndex())
                );
            }
        });
    }

    public void clearComboBox() {
        _textureComboBox.removeAllItems();
    }

    public void addItemToComboBox(String path) {
        _textureComboBox.addItem(path);
    }

    public void changeLabelPicture(String path) {
        _pictureBoxPanel.setText(null);
        Image img = _bitmapLoader.load(path).getBuffer();
        _pictureBoxPanel.setIcon(new ImageIcon(img.getScaledInstance(100, 100, Image.SCALE_SMOOTH)));
    }

    public void addDrawMethodChangeListener(ISubscriber<ChangeDrawArgs> listener){
        _drawArgsButtonPanel.addChangeDrawListener(listener);
    }

    public void addTextureChangeListener(ISubscriber<TextureChangeArgs> listener) {
        _textureChangeArgsEvent.addSubscriber(listener);
    }

    public void addCleanMapListener(ISubscriber<ButtonArgs> listener) {
        _cleanMapArgsEvent.addSubscriber(listener);
    }

    public void addBrowseListener(ISubscriber<ButtonArgs> listener) {
        _browseArgsEvent.addSubscriber(listener);
    }
}
