package tools.mapEditor.Components.Panels;

import tools.mapEditor.EditorEvents.MapChangedArgs;
import tools.mapEditor.EditorEvents.OnMouseDragArgs;
import tools.mapEditor.Model.Model;
import engine.events.ISubscriber;

import javax.swing.*;
import javax.swing.border.BevelBorder;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import java.util.HashMap;

/**
 * Created with IntelliJ IDEA.
 * User: Andreas
 * Date: 2012-09-17
 * Time: 14:42
 * To change this template use File | Settings | File Templates.
 */
public class MapPanel extends JPanel {

    private final MapPanel _thisPanel = this;
    private engine.events.Event<OnMouseDragArgs> _onMouseDragEvent = new engine.events.Event<OnMouseDragArgs>();
    private Model _model;
    private int _sizeX = 800;
    private int _sizeY = 800;

    public MapPanel(Model model) {
        _model = model;
        setLayout(new BorderLayout());
    }

    /**
     * Sets default map panel with mouselistening. Also sets preferred size so viewport can update its scrollbars.
     */
    public void initialize() {
        this.setBorder(new BevelBorder(BevelBorder.LOWERED, Color.gray, Color.BLACK));
        this.setPreferredSize(new Dimension(_sizeX,_sizeY));

        _model.addMapChangeListener(new ISubscriber<MapChangedArgs>() {
            @Override
            public void handleEvent(MapChangedArgs arg) {
                paintComponent(arg.getMap(), arg.getTileSize(), arg.getImageMap(), _thisPanel.getGraphics());

                if (_sizeX != arg.getMap()[0].length * arg.getTileSize() && _sizeY != arg.getMap().length * arg.getTileSize()) {
                    _sizeX = arg.getMap()[0].length * arg.getTileSize(); _sizeY = arg.getMap().length * arg.getTileSize();
                    _thisPanel.setPreferredSize(new Dimension(arg.getMap()[0].length * arg.getTileSize(), arg.getMap().length * arg.getTileSize()));
                }
            }
        });

        addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                _onMouseDragEvent.raiseEvent(new OnMouseDragArgs(_thisPanel, e.getX(), e.getY()));
            }
        });

        addMouseMotionListener(new MouseMotionAdapter() {
            @Override
            public void mouseDragged(MouseEvent e) {
                 _onMouseDragEvent.raiseEvent(new OnMouseDragArgs(_thisPanel, e.getX(), e.getY()));
            }
        });
    }

    public void addOnMouseDragListener(ISubscriber<OnMouseDragArgs> listener) {
        _onMouseDragEvent.addSubscriber(listener);
    }

    /**
     * Paints the map according to tilemap info.
     * @param _map
     * @param _tileSize
     * @param _imageMap
     * @param g
     */
    public void paintComponent(int[][] _map, int _tileSize, HashMap<Integer, Image> _imageMap, Graphics g) {
        Graphics2D g2 = (Graphics2D) g;
        if (_map != null) {
            for (int y = 0; y < _map.length; y++) {
                for (int x = 0; x < _map[y].length; x++) {
                    if (_map[y][x] == -1) {
                        g2.setColor(Color.MAGENTA);
                        g2.fillRect(x * _tileSize, y * _tileSize, _tileSize, _tileSize);
                    } else {
                        g2.setColor(Color.MAGENTA);
                        g2.fillRect(x * _tileSize, y * _tileSize, _tileSize, _tileSize);
                        g2.drawImage(_imageMap.get(_map[y][x]), x * _tileSize, y * _tileSize, _tileSize, _tileSize, null);
                    }
                }
            }
        }
    }
}
