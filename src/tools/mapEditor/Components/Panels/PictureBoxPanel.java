package tools.mapEditor.Components.Panels;

import tools.mapEditor.Components.TexturePanelComponents.Labels.PictureBox;

import javax.swing.*;
import java.awt.*;

public class PictureBoxPanel extends JPanel {

    private PictureBox _pictureBox = new PictureBox();

    /**
     * Creates the thumbnail panel of the current drawing texture.
     */
    public PictureBoxPanel() {
        setPreferredSize(new Dimension(100, 100));
        setBackground(Color.gray);
        add(_pictureBox, CENTER_ALIGNMENT);
    }

    public void setIcon(Icon icon){
        _pictureBox.setIcon(icon);
    }

    public void setText(String string){
        _pictureBox.setText(string);
    }
}
