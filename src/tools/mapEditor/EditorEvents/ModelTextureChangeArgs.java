package tools.mapEditor.EditorEvents;

import tools.mapEditor.Model.Model;
import engine.events.EventArgs;

public class ModelTextureChangeArgs extends EventArgs<Model> {
    public ModelTextureChangeArgs(Model source) {
        super(source);
    }
}
