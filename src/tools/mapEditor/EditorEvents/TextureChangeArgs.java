package tools.mapEditor.EditorEvents;

import tools.mapEditor.Components.Panels.TexturePanel;
import engine.events.EventArgs;

/**
 * Created with IntelliJ IDEA.
 * User: Andreas
 * Date: 2012-09-17
 * Time: 17:50
 * To change this template use File | Settings | File Templates.
 */

/**
 * Texture is changed from the component that contains all the texturepaths.
 */
public class TextureChangeArgs extends EventArgs<TexturePanel> {
    private String _filePath;
    private int _index;

    public TextureChangeArgs(TexturePanel source, String filePath, int index) {
        super(source);
        this._filePath = filePath;
        this._index = index;
    }

    public String getFilePath() {
        return _filePath;
    }

    public int getFileIndex(){
        return _index;
    }

}
