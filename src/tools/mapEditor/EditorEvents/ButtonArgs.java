package tools.mapEditor.EditorEvents;

import engine.events.EventArgs;

/**
 * Used for the buttons, gets the button type in string.
 */
public class ButtonArgs extends EventArgs<Object> {

    private String _type;

    public ButtonArgs(Object source, String type) {
        super(source);
        _type = type;
    }

    public String getType() {
        return _type;
    }
}
