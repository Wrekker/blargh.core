package tools.mapEditor.EditorEvents;

import engine.events.EventArgs;

/**
 * If menu item is chosen we get the item option as string.
 */
public class MenuArgs extends EventArgs<Object> {

    private String _option;

    public MenuArgs(Object source, String option) {
        super(source);
        _option = option;
    }

    public String getOption() {
        return _option;
    }

}
