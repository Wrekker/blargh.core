package tools.mapEditor.EditorEvents;

import tools.mapEditor.Components.Panels.DrawArgsButtonPanel;
import engine.events.EventArgs;

/**
 * Change of drawing method, it gets method as string.
 */
public class ChangeDrawArgs extends EventArgs<DrawArgsButtonPanel> {
    private String _method;

    public ChangeDrawArgs(DrawArgsButtonPanel source, String method) {
        super(source);
        _method = method;
    }

    public String getMethod() {
        return _method;
    }
}
