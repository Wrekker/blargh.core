package tools.mapEditor;

import engine.resources.bitmap.BitmapLoader;
import engine.resources.bitmap.BmpImageLoader;
import engine.resources.bitmap.JpgImageLoader;
import engine.resources.bitmap.PngImageLoader;
import tools.mapEditor.Components.Panels.MapPanel;
import tools.mapEditor.Components.Panels.TexturePanel;
import tools.mapEditor.Model.Model;
import tools.mapEditor.Model.TextureCatalogue;
import tools.mapEditor.Model.TileMap;
import tools.mapEditor.View.MainFrame;

public class FieldEditor {
    public static void main(String[] args) {
        run(args);
    }

    public static void run(String[] args) {
        TextureCatalogue tc = new TextureCatalogue();
        TileMap tm = new TileMap(10, 10, tc, 32, 512, 128);
        PngImageLoader pngImageLoader = new PngImageLoader();
        BmpImageLoader bmpImageLoader = new BmpImageLoader();
        JpgImageLoader jpgImageLoader = new JpgImageLoader();
        BitmapLoader bitmapLoader = new BitmapLoader();
        bitmapLoader.registerFormatLoader("png", pngImageLoader);
        bitmapLoader.registerFormatLoader("bmp", bmpImageLoader);
        bitmapLoader.registerFormatLoader("jpg", jpgImageLoader);

        Model _model = new Model(tm, bitmapLoader);

        TexturePanel _texturePanel = new TexturePanel(bitmapLoader);
        MapPanel _mapPanel = new MapPanel(_model);

        new MainFrame(_model,  _texturePanel, _mapPanel);
    }
}
