package tools.mapEditor.Tools;

import tools.mapEditor.EditorEvents.MapChangedArgs;
import engine.events.Event;

/**
 * A tile reference to a tile in the tile map, can set the whole map with information from the selected tile (texture reference)
 * or simply set one tile to a texture reference.
 */
public class TileReference {

    private final int _x;
    private final int _y;
    protected final int[][] _tileMap;
    protected Event<MapChangedArgs> _mapChangedEvent;
    protected MapChangedArgs _mapChangedArgs;

    public TileReference(int x, int y, int[][] map, Event<MapChangedArgs> mapChangedArgsEvent, MapChangedArgs mapChange) {
        _x = x;
        _y = y;
        _tileMap = map;
        _mapChangedArgs = mapChange;
        _mapChangedEvent = mapChangedArgsEvent;
    }

    public void setTile(TextureReference texture) {
        if (texture != null) {
            _tileMap[_y][_x] = texture.getId();
        }
        _mapChangedEvent.raiseEvent(_mapChangedArgs);
    }

    public void setMap(TextureReference texture) {
        if (texture != null && _tileMap != null) {
            int id = texture.getId();
            for (int i = 0; i < _tileMap.length; i++) {
                for (int j = 0; j < _tileMap[0].length; j++) {
                        _tileMap[i][j] = id;
                }
            }
            _mapChangedEvent.raiseEvent(_mapChangedArgs);
        }
    }
}
