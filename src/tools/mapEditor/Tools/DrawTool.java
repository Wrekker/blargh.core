package tools.mapEditor.Tools;

/**
 * The draw tool which overrides paint of ITool to set one tile to a texture reference.
 */
public class DrawTool implements ITool {

    private TextureReference _texture;

    public DrawTool(TextureReference texture) {
        if (texture != null){
        _texture = texture;
        }
    }

    @Override
    public void paint(TileReference tile) {
        if (tile != null){
        tile.setTile(_texture);
        } else return;
    }
}
