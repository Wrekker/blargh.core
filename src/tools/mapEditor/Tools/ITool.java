package tools.mapEditor.Tools;

/**
 * Paint tool interface which has a paint method.
 */
public interface ITool {

    public void paint(TileReference tile);

}
