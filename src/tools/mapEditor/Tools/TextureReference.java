package tools.mapEditor.Tools;

/**
 * The texture reference from a texture catalogue.
 */
public class TextureReference {
    private int _key;

    public TextureReference(int key) {
        _key = key;
    }

    public int getId(){
        return _key;
    }
}
