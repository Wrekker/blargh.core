package tools.mapEditor.Tools;

/**
 * Erase tool which overrides paint of ITool to set a tile to our "null" value.
 */
public class EraseTool implements ITool {
    @Override
    public void paint(TileReference tile) {
        if (tile != null){
            tile.setTile(new TextureReference(-1));
        } else return;
    }
}
