package tools.mapEditor.Controller;

import tools.mapEditor.Tools.ITool;

/**
 * The controller in our MVC design.
 */
public interface IController {
    public void Draw(int x, int y, boolean flag);
    public void onButtonClick(String option);
    public void onMenu(String option);
    public void onComboBoxSelection(String path, int index);
    public void onSelectTool(ITool tool);
}
