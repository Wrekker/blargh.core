package tools.baking;

import java.nio.ByteBuffer;

public interface IBaker<T> {
    ByteBuffer bake(T data);
}
