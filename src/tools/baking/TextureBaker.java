package tools.baking;

import org.lwjgl.BufferUtils;

import java.awt.image.BufferedImage;
import java.nio.ByteBuffer;

public class TextureBaker implements IBaker<BufferedImage> {
    public TextureBaker() {

    }

    @Override
    public ByteBuffer bake(BufferedImage img) {
        int[] pixels = new int[img.getWidth() * img.getHeight()];
        int w = img.getWidth(), h = img.getHeight();
        img.getRGB(0, 0, w, h, pixels, 0, w);

        int depth = img.getColorModel().getNumComponents();
        int bufferSize = w * h * depth;

        ByteBuffer buffer = BufferUtils.createByteBuffer(bufferSize);
        int c = w * h;

        for(int p = 0; p < c; ++p) {
            int px = pixels[p];
            buffer.put((byte)((px >> 16) & 0xFF));     // R
            buffer.put((byte)((px >> 8) & 0xFF));      // G
            buffer.put((byte)(px & 0xFF));             // B
            if (depth > 3) {
                buffer.put((byte)((px >> 24) & 0xFF)); // A
            }
        }

        buffer.flip();

        return buffer;
    }
}
