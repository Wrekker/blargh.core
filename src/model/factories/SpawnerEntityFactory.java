package model.factories;

import engine.audio.IAudioManager;
import engine.entities.Entity;
import engine.entities.IEntityDef;
import engine.entities.IEntityFactory;
import model.definitions.entities.SpawnerEntityDef;
import model.entities.SpawnerEntity;
import engine.entities.IEntityManager;
import engine.particles.IParticleManager;
import org.jbox2d.dynamics.World;

import java.lang.reflect.Type;

public class SpawnerEntityFactory implements IEntityFactory {
    @Override
    public Type getEntityType() {
        return SpawnerEntity.class;
    }

    @Override
    public Entity create(IEntityDef entityDef,
                         World world,
                         IEntityManager entityManager,
                         IAudioManager audioManager,
                         IParticleManager particleManager) {
        SpawnerEntityDef def = (SpawnerEntityDef)entityDef;
        def.spawnDef.position = def.position;
        return new SpawnerEntity(def.interval, def.spawnDef, entityManager);
    }
}