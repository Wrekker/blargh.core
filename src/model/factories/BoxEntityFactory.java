package model.factories;

import engine.audio.IAudioManager;
import model.definitions.entities.BoxEntityDef;
import model.definitions.particles.DirtParticleDef;
import model.entities.BoxEntity;
import model.entities.ShotEntity;
import model.EntityDestroyerSolver;
import model.Grip;
import model.GripContactSolver;
import model.Material;
import engine.collisions.ContactManager;
import engine.entities.Entity;
import engine.entities.IEntityDef;
import engine.entities.IEntityFactory;
import engine.entities.IEntityManager;
import engine.misc.ColorF;
import engine.particles.IParticleEmitter;
import engine.particles.IParticleManager;
import engine.physics.BodyBuilder;
import engine.resources.Loaders;
import org.jbox2d.dynamics.Body;
import org.jbox2d.dynamics.World;

import java.lang.reflect.Type;

public class BoxEntityFactory implements IEntityFactory {
    @Override
    public Type getEntityType() {
        return BoxEntity.class;
    }

    @Override
    public Entity create(IEntityDef entityDef, World world, IEntityManager entityManager,
                         IAudioManager audioManager, IParticleManager particleManager) {
        BoxEntityDef def = (BoxEntityDef)entityDef;
        final BoxEntity entity = new BoxEntity(
                Loaders.getInstance().texture("box.png"),
                def.size);

        Body b = BodyBuilder
                .createActor(2, entity)
                .addBox(0, 0, def.size, def.size, Material.Wood)
                .toBody(world);

        b.setTransform(def.position, 0);

        IParticleEmitter emitter = particleManager.createEmitter(new DirtParticleDef(entity, new ColorF(0.60f, 0.45f, 0.3f, 1f)));

        entity.initialize(b, new ContactManager());
        entity.getContactManager().registerSolver(Grip.class, new GripContactSolver(entity));
        entity.getContactManager().registerSolver(ShotEntity.class, new EntityDestroyerSolver(emitter));


        return entity;
    }
}