package model.factories;

import engine.audio.IAudioManager;
import model.definitions.particles.DirtParticleDef;
import model.definitions.entities.StaticEntityDef;
import model.entities.ShotEntity;
import model.entities.StaticEntity;
import model.Material;
import engine.collisions.ContactManager;
import engine.collisions.IContactSolver;
import engine.entities.Entity;
import engine.entities.IEntityDef;
import engine.entities.IEntityFactory;
import engine.entities.IEntityManager;
import engine.misc.ColorF;
import engine.particles.IParticleEmitter;
import engine.particles.IParticleManager;
import engine.physics.BodyBuilder;
import org.jbox2d.common.Vec2;
import org.jbox2d.dynamics.Body;
import org.jbox2d.dynamics.World;

import java.lang.reflect.Type;

public class StaticEntityFactory implements IEntityFactory {
    @Override
    public Type getEntityType() {
        return StaticEntity.class;
    }

    @Override
    public Entity create(IEntityDef entityDef,
                         World world,
                         final IEntityManager entityManager,
                         IAudioManager audioManager,
                         final IParticleManager particleManager) {
        StaticEntityDef def = (StaticEntityDef)entityDef;
        final StaticEntity entity = new StaticEntity();

        BodyBuilder bb = BodyBuilder
                .createDoodad(entity);

        switch (def.type) {
            case StaticEntityDef.BOX:
                bb.addBox(0,0,def.side,def.side, Material.Concrete);
                break;
            case StaticEntityDef.CIRCLE:
                bb.addCircle(0, 0, def.radius, Material.Concrete);
                break;
            case StaticEntityDef.POLY:
                bb.addPolygon(0,0, def.points, Material.Concrete);
                break;
        }

        Body b = bb.toBody(world);

        b.setTransform(def.position, 0);

        entity.initialize(b, new ContactManager());

        final IParticleEmitter dirt = particleManager.createEmitter(new DirtParticleDef(entity, new ColorF(0.44f, 0.35f, 0.3f, 1)));
        entity.getContactManager().registerSolver(ShotEntity.class, new IContactSolver() {
            @Override
            public void beginContact(Object data) {
                ShotEntity e = (ShotEntity) data;

                for (int i = 0; i < 3; i++) {
                    Vec2 v = e.getBody().getPosition();
                    dirt.emit(v.x, v.y);
                }

                e.setDestroyed();
            }

            @Override
            public void endContact(Object data) {

            }
        });

        return entity;
    }
}