package model.factories;

import engine.audio.IAudioManager;
import model.definitions.particles.BloodParticleDef;
import model.definitions.entities.ShotEntityDef;
import model.entities.ShotEntity;
import model.IVulnerable;
import model.Material;
import engine.collisions.ContactManager;
import engine.collisions.IContactSolver;
import engine.entities.Entity;
import engine.entities.IEntityDef;
import engine.entities.IEntityFactory;
import engine.entities.IEntityManager;
import engine.particles.IParticleEmitter;
import engine.particles.IParticleManager;
import engine.physics.BodyBuilder;
import engine.physics.Boundary;
import org.jbox2d.common.Vec2;
import org.jbox2d.dynamics.Body;
import org.jbox2d.dynamics.World;

import java.lang.reflect.Type;

public class ShotEntityFactory implements IEntityFactory {
    @Override
    public Type getEntityType() {
        return ShotEntity.class;
    }

    @Override
    public Entity create(IEntityDef entityDef,
                         World world,
                         final IEntityManager entityManager,
                         IAudioManager audioManager,
                         final IParticleManager particleManager) {
        final ShotEntityDef def = (ShotEntityDef)entityDef;
        final ShotEntity entity = new ShotEntity(def.lifetime);

        Body b = BodyBuilder
                .createBullet(entity)
                .addCircle(0,0,0.1f, Material.Wood)
                .toBody(world);

        b.setTransform(def.origin, 0);
        b.setLinearVelocity(def.direction.mul(def.velocity));

        entity.initialize(b, new ContactManager());
        entity.getContactManager().registerSolver(Boundary.class, new IContactSolver() {
            @Override
            public void beginContact(Object data) {
                entity.setDestroyed();
            }

            @Override
            public void endContact(Object data) {

            }
        });

        final IParticleEmitter emitter = particleManager.createEmitter(new BloodParticleDef());
        entity.getContactManager().registerSolver(Entity.class, new IContactSolver() {
            @Override
            public void beginContact(Object data) {
                Entity e = (Entity)data;

                if (e instanceof IVulnerable) {
                    ((IVulnerable)e).getHealthManager().applyDamage(1);

                    Vec2 v = e.getBody().getPosition();

                    for (int i = 0; i < 2; i++) {
                        emitter.emit(v.x, v.y);
                    }
                }

                entity.setDestroyed();
            }

            @Override
            public void endContact(Object data) {

            }
        } );



        return entity;
    }
}
