package model.factories;

import engine.ai.behavior.AiBehaviorTree;
import engine.ai.behavior.AiBehaviourNode;
import engine.ai.behavior.PathfindingBehavior;
import engine.animation.AnimationManager;
import engine.audio.channels.IAudioChannel;
import engine.audio.IAudioManager;
import engine.entities.Entity;
import engine.entities.IEntityDef;
import engine.entities.IEntityFactory;
import engine.entities.IEntityManager;
import engine.management.Audio;
import engine.misc.Rnd;
import engine.particles.IParticleManager;
import engine.physics.BodyBuilder;
import engine.resources.Loaders;
import model.ai.AttackBehavior;
import model.ai.MoanBehavior;
import model.definitions.entities.EnemyEntityDef;
import model.entities.ZombieEntity;
import model.Material;
import org.jbox2d.dynamics.Body;
import org.jbox2d.dynamics.World;

import java.lang.reflect.Type;

public class ZombieEntityFactory implements IEntityFactory {
    private final Entity _player;

    public ZombieEntityFactory(Entity player) {
        // This is needed to be able to home in on the player
        _player = player;
    }

    @Override
    public Type getEntityType() {
        return ZombieEntity.class;
    }

    @Override
    public Entity create(IEntityDef entityDef, World world, IEntityManager entityManager,
                         IAudioManager audioManager, IParticleManager particleManager) {
        EnemyEntityDef def = (EnemyEntityDef)entityDef;
        final ZombieEntity entity = new ZombieEntity(10, Rnd.nextFloat(1, 3));

        Body b = BodyBuilder
                .createActor(2, entity)
                .addCircle(0, 0, 0.5f, Material.Concrete)
                .toBody(world);

        b.setTransform(def.position, def.angle);


        AiBehaviourNode root = new AiBehaviourNode(new AttackBehavior(entity));
        root.getNodes().add(new AiBehaviourNode(new PathfindingBehavior(entity, _player)));
        root.getNodes().add(new AiBehaviourNode(new MoanBehavior(entity)));

        AiBehaviorTree tree = new AiBehaviorTree(root);

        entity.initialize(b, tree);

        entity.setAnimationManager(new AnimationManager(Loaders.getInstance().animationSet(def.animationSet)));
        entity.getAnimationManager().setTimescale(3);

        return entity;
    }
}
