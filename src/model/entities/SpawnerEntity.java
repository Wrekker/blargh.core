package model.entities;

import engine.rendering.IExplicitOrderView;
import model.definitions.entities.EnemyEntityDef;
import engine.entities.Entity;
import engine.entities.IEntityManager;
import engine.misc.GameTime;
import engine.misc.switches.PeriodicSwitch;

public class SpawnerEntity extends Entity {
    private final PeriodicSwitch _spawnTrigger;
    private final EnemyEntityDef _spawnDef;
    private final IEntityManager _entityManager;

    public SpawnerEntity(float period, EnemyEntityDef spawnDefPrototype, IEntityManager entityManager) {
        _entityManager = entityManager;
        _spawnTrigger = new PeriodicSwitch(period, true);
        _spawnDef = spawnDefPrototype;
    }

    @Override
    public void update(GameTime time) {
        if (_spawnTrigger.trigger(time)) {
            _entityManager.createEntity(_spawnDef);
        }
    }

    @Override
    public void render(IExplicitOrderView target, GameTime time) {
        // nada
    }
}
