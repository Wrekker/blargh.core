package model.entities;

import engine.animation.IAnimationManager;
import engine.audio.IAudioSample;
import engine.audio.channels.IAudioChannel;
import engine.collisions.ContactManager;
import engine.entities.Entity;
import engine.entities.IEntityManager;
import engine.input.*;
import engine.management.Audio;
import engine.misc.*;
import engine.rendering.ICamera;
import engine.rendering.IExplicitOrderView;
import engine.resources.Loaders;
import model.*;
import model.weapons.Machinegun;
import org.jbox2d.common.Vec2;
import org.jbox2d.dynamics.Body;

/**
 * The player-controlled character entity.
 */
public class PlayerEntity extends Entity implements IVulnerable {
    private IAnimationManager _animationManager;
    private IHealthManager _healthManager;

    private Vec2 _direction = new Vec2(0, 0);
    private final PlayerEntity _this = this;
    private ICamera _camera;
    private Grip _grip;

    private IImplement _weapon;
    private boolean _firing = false;
    private IAudioSample _fireSound = Loaders.getInstance().audio("effects\\machg_fire.wav");
    private IAudioChannel _fireChannel;
    private Score _score;

    public PlayerEntity() {

    }

    public void initialize(Body b, ContactManager manager, IEntityManager entityManager) {
        super._body = b;
        super._contactManager = manager;

        _score = new Score();
        _grip = new Grip(this);
        _weapon = new Machinegun(entityManager);

    }

    public IAnimationManager getAnimationManager() { return _animationManager; }
    public void setAnimationManager(IAnimationManager animationManager) { _animationManager = animationManager; }

    @Override
    public void setHealthManager(IHealthManager healthManager) { _healthManager = healthManager; }
    @Override
    public IHealthManager getHealthManager() { return _healthManager; }

    public void setCamera(ICamera camera) { _camera = camera; }
    public Grip getGrip() { return _grip; }
    public Score getScore() { return _score; }

    @Override
    public void update(GameTime time) {
        _grip.processGrip();

        Vec2 w = getBody().getLocalVector(_direction);
        w.normalize();
        w = Vector.rotate(w, getBody().getAngle());
        getBody().setLinearVelocity(w.mul(6f));

        if (!_grip.isGripping() && _firing && _weapon != null) {
            float a = _this.getBody().getAngle();
            Vec2 loc = _this.getBody().getPosition().add(Vector.angleToVector(a).mul(1f));

            if (_weapon.use(_this, loc, a, time)) {
                if (_fireChannel == null) {
                    _fireChannel = Audio.createChannel().atEntity(this);
                }

                _fireChannel.play(_fireSound);
            }
        }

        Vec2 p = getBody().getWorldCenter();
        _camera.centerOn(p.x, p.y);
    }

    @Override
    public void render(IExplicitOrderView target, GameTime time) {
        Vec2 pos = super.getBody().getWorldCenter();

        target.drawSubTexture(
                pos.x,
                pos.y,
                1.8f,
                1, 1, getBody().getAngle() + (float)(7f * Math.PI / 6f),
                _animationManager.currentFrame(time)
        );
    }

    public IKeyInteraction forwardInteraction() {
        return new IKeyInteraction() {
            @Override
            public boolean onKey(KeyEvent ev) {
                if (ev.isKeyDown()) { setDirection(0, 1); }
                else { setDirection(0, -1); }

                return true;
            }
        };
    }

    public IKeyInteraction backInteraction() {
        return new IKeyInteraction() {
            @Override
            public boolean onKey(KeyEvent ev) {
                if (ev.isKeyDown()) { setDirection(0, -1); }
                else { setDirection(0, 1); }

                return true;
            }
        };
    }

    public IKeyInteraction leftInteraction() {
        return new IKeyInteraction() {
            @Override
            public boolean onKey(KeyEvent ev) {
                if (ev.isKeyDown()) { setDirection(-1, 0); }
                else { setDirection(1, 0); }

                return true;
            }
        };
    }

    public IKeyInteraction rightInteraction() {
        return new IKeyInteraction() {
            @Override
            public boolean onKey(KeyEvent ev) {
                if (ev.isKeyDown()) { setDirection(1, 0); }
                else { setDirection(-1, 0); }
                return true;
            }
        };
    }

    public IMouseInteraction aimInteraction() {
        return new IMouseInteraction() {
            @Override
            public boolean onMouse(MouseEvent ev) {
                Vec2 pos = getBody().getPosition();
                Point2F mpoint = Convert.mouseToPhysics(ev.getAbsX(), ev.getAbsY(), _camera);
                Vec2 mouse = new Vec2(mpoint.x, mpoint.y);
                mouse.subLocal(pos);

                float maxTorque = 20f;

                float angle = getBody().getAngle();
                float inertia = getBody().getInertia();
                float w = getBody().getAngularVelocity();
                float targetAngle = Vector.vectorToAngle(mouse);

                float differenceAngle = MathHelper.normalizeAngle(targetAngle - angle);
                float brakingAngle = MathHelper.normalizeAngle((float)(Math.signum(w)*2.0*w*w*inertia/maxTorque));
                float torque = maxTorque;

                if (differenceAngle < 0.05f && differenceAngle > -0.05f) {
                    return true;
                }

                // two of these three conditions must be true
                boolean a = differenceAngle < 0,
                        b = brakingAngle > differenceAngle,
                        c = w > 0;

                if( (a && b) || (a && c) || (b && c)) {
                    torque *= -1;
                }

                getBody().applyTorque(torque);

                return true;
            }
        };
    }

    public IButtonInteraction shootInteraction() {
        return new IButtonInteraction() {
            @Override
            public boolean onButton(ButtonEvent ev) {
                _firing = ev.isButtonDown();
                return true;
            }
        };
    }

    public IButtonInteraction gripInteraction() {
        return new IButtonInteraction() {
            @Override
            public boolean onButton(ButtonEvent ev) {
                if (ev.isButtonDown()) {
                    _this.getGrip().startGrip();
                } else {
                    _this.getGrip().endGrip();
                }

                return true;
            }
        };
    }

    public IKeyInteraction pickWeaponInteraction(final IImplement weapon) {
        return new IKeyInteraction() {
            @Override
            public boolean onKey(KeyEvent ev) {
                if (ev.isKeyDown()) {
                    _weapon = weapon;
                    return true;
                }
                return false;
            }
        };
    }

    private void setDirection(float x, float y) {
        _direction.addLocal(x, y);

        if (_direction.lengthSquared() > 0) {
            _animationManager.setAnimation("walk");
            _animationManager.setTimescale(2f);
        } else {
            _animationManager.setAnimation("stand");
            _animationManager.setTimescale(1);
        }
    }

}
