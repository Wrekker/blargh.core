package model.entities;

import engine.collisions.ContactManager;
import engine.entities.Entity;
import engine.misc.GameTime;
import engine.rendering.IExplicitOrderView;
import org.jbox2d.dynamics.Body;

public class StaticEntity extends Entity {

    public void initialize(Body b, ContactManager manager) {
        super._body = b;
        super._contactManager = manager;
    }

    @Override
    public void update(GameTime time) { }

    @Override
    public void render(IExplicitOrderView target, GameTime time) {

    }
}
