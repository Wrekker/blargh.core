package model.ai;

import engine.ai.actions.IAiAction;
import engine.ai.behavior.IAiBehavior;
import engine.misc.GameTime;
import engine.misc.Rnd;
import model.entities.ZombieEntity;

import java.util.Deque;

/**
 * The cow says: Mooooh!
 * The zombie says: Mrrrrrghl!
 */
public class MoanBehavior implements IAiBehavior {
    float _lastMoan = 0;
    private final ZombieEntity _entity;

    private final String[] _moans = { "Mrrrrg...", "Mrrr!", "Grrrmgf..." };

    public MoanBehavior(ZombieEntity entity) {
        _entity = entity;
    }

    @Override
    public boolean think(GameTime gt, Deque<IAiAction> actionQueue) {
        if (gt.getTotalSeconds() - _lastMoan > 15) {
            if (Rnd.odds(1, 3)) {
                _lastMoan = gt.getTotalSeconds();
                actionQueue.add(new MoanAction(_entity));
            }
        }

        return false;
    }
}
