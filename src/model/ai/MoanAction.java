package model.ai;

import engine.ai.actions.IAiAction;
import engine.management.Audio;
import engine.misc.GameTime;
import engine.resources.Loaders;
import model.entities.ZombieEntity;

public class MoanAction implements IAiAction {
    private final ZombieEntity _entity;

    public MoanAction(ZombieEntity entity) {
        _entity = entity;
    }

    @Override
    public boolean isValid(GameTime gt) {
        return true;
    }

    @Override
    public boolean perform(GameTime gt) {
        Audio.play(Loaders.getInstance().audio("effects/z_idle.wav"))
            .atEntity(_entity);

        return false;
    }
}
