package model.ai;

import engine.ai.actions.IAiAction;
import engine.entities.Entity;
import engine.misc.GameTime;
import model.IVulnerable;

public class AttackAction implements IAiAction {
    private final Entity _source;
    private final IVulnerable _target;

    public AttackAction(Entity source, IVulnerable target) {
        _source = source;
        _target = target;
    }

    @Override
    public boolean isValid(GameTime gt) {
        return true;
    }

    @Override
    public boolean perform(GameTime gt) {
        _target.getHealthManager().applyDamage(1);
        return false;
    }
}
