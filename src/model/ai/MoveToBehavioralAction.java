package model.ai;

import engine.ai.actions.IAiAction;
import engine.ai.pathfinding.PathNode;
import engine.entities.Entity;
import engine.misc.GameTime;
import engine.misc.Vector;
import org.jbox2d.common.Vec2;

public class MoveToBehavioralAction implements IAiAction {
    private final Entity _self;
    private final PathNode _node;
    private GameTime _first = null;
    private float _progress;

    public MoveToBehavioralAction(Entity self, PathNode node) {
        _self = self;
        _node = node;
        _progress = _node.nodePos.sub(self.getBody().getPosition()).lengthSquared();
    }

    @Override
    public boolean isValid(GameTime gt) {
        if (_first == null) {
            _first = gt;
        }

        if (_node.nodePos.sub(_self.getBody().getPosition()).lengthSquared() < _progress &&
                _self.getBody().getPosition().sub(_node.nodePos).lengthSquared() > 0.3) {
            _progress = _node.nodePos.sub(_self.getBody().getPosition()).lengthSquared();
            return true;
        }

        return gt.getTotalSeconds() - _first.getTotalSeconds() < 3f &&
               _self.getBody().getPosition().sub(_node.nodePos).lengthSquared() > 0.1;
    }

    @Override
    public boolean perform(GameTime gt) {
        Vec2 dir = _node.nodePos.sub(_self.getBody().getPosition());
        dir.normalize();
        _self.getBody().setTransform(_self.getBody().getPosition(), Vector.vectorToAngle(dir));

        return gt.getTotalSeconds() - _first.getTotalSeconds() < 3;
    }
}
