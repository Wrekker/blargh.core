package model;

/**
 * Very simple score component for the player entity
 */
public class Score {
    private long _currentScore;

    public void Score() { }

    public void incrementScore(int increment){
        _currentScore += increment;
    }

    public long getScore(){
          return _currentScore;
    }
}
