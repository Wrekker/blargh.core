package model;

import engine.collisions.IContactSolver;
import engine.entities.Entity;
import engine.particles.IParticleEmitter;
import org.jbox2d.common.Vec2;

/**
 * Contact solver that destroys the other object on contact end
 * (On end is used as it improves force accuracy)
 */
public class EntityDestroyerSolver implements IContactSolver {
    private final IParticleEmitter _effectEmitter;

    public EntityDestroyerSolver(IParticleEmitter effectEmitter) {
        _effectEmitter = effectEmitter;
    }

    public void beginContact(Object data) {
    }


    @Override
    public void endContact(Object data) {
        Entity e = (Entity)data;
        e.setDestroyed();

        Vec2 v = e.getBody().getPosition();

        for (int i = 0; i < 3; i++) {
            _effectEmitter.emit(v.x, v.y);
        }
    }
}
