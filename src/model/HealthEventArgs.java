package model;

import engine.events.EventArgs;

public class HealthEventArgs extends EventArgs<IHealthManager> {
    private final int _current;
    private final int _change;

    protected HealthEventArgs(IHealthManager source, int current, int change) {
        super(source);
        _current = current;
        _change = change;
    }

    public int getCurrent() {
        return _current;
    }

    public int getChange() {
        return _change;
    }
}
