package model.definitions.entities;

import engine.entities.IEntityDef;
import model.entities.StaticEntity;
import org.jbox2d.common.Vec2;

import java.lang.reflect.Type;

public class StaticEntityDef implements IEntityDef {
    public final static int BOX = 0;
    public final static int CIRCLE = 1;
    public final static int POLY = 2;

    public Vec2 position = new Vec2(0,0);
    public float angle = 0;
    public int type = BOX;
    public Vec2[] points = { new Vec2(0,1), new Vec2(-1,-1), new Vec2(1,-1) };
    public float radius = 1;
    public float side = 1;

    @Override
    public Type getEntityType() {
        return StaticEntity.class;
    }
}
