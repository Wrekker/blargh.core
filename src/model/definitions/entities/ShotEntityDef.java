package model.definitions.entities;

import engine.entities.IEntityDef;
import model.entities.ShotEntity;
import org.jbox2d.common.Vec2;

import java.lang.reflect.Type;

public class ShotEntityDef implements IEntityDef {
    public float damage = 1;
    public float velocity = 10;
    public Vec2 origin = new Vec2(0,0);
    public Vec2 direction = new Vec2(0,0);
    public float lifetime = 0.5f;

    @Override
    public Type getEntityType() {
        return ShotEntity.class;
    }
}
