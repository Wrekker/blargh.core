package model.definitions.entities;

import engine.entities.IEntityDef;
import model.entities.PlayerEntity;
import org.jbox2d.common.Vec2;

import java.lang.reflect.Type;

public class PlayerEntityDef implements IEntityDef {
    public Vec2 position = new Vec2(0,0);
    public float angle = (float) (Math.PI / 2);
    public String animationSet = "player.animationset";

    @Override
    public Type getEntityType() {
        return PlayerEntity.class;
    }
}
