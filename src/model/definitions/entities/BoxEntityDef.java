package model.definitions.entities;

import engine.entities.IEntityDef;
import model.entities.BoxEntity;
import org.jbox2d.common.Vec2;

import java.lang.reflect.Type;

public class BoxEntityDef implements IEntityDef {
    public Vec2 position = new Vec2(0,0);
    public float angle = 0;
    public Vec2[] points = new Vec2[0];
    public float size = 1;

    @Override
    public Type getEntityType() {
        return BoxEntity.class;
    }

}
