package model.definitions.entities;

import engine.entities.IEntityDef;
import model.entities.SpawnerEntity;
import org.jbox2d.common.Vec2;

import java.lang.reflect.Type;

public class SpawnerEntityDef implements IEntityDef {
    public EnemyEntityDef spawnDef = null;
    public Vec2 position = new Vec2(0,0);
    public float interval = 10;

    @Override
    public Type getEntityType() {
        return SpawnerEntity.class;
    }
}
