package model.definitions.particles;

import engine.misc.*;
import engine.particles.IParticleDefinition;
import engine.particles.Particle;
import engine.rendering.BlendMode;
import engine.resources.Loaders;
import engine.resources.texture.SubTexture;

public class FireParticleDef implements IParticleDefinition {
    private final static SubTexture _tx = new SubTexture(Loaders.getInstance().texture("Flame_4.png"), 0, 0, 128, 128);
    private final static LinearGradient _gradient = new LinearGradient(new ColorF[] {
            new ColorF(1f, 0.15f, 0.1f, 0.3f),
            new ColorF(1f, 0.3f, 0.1f, 0.3f),
            new ColorF(1f, 0.6f, 0.1f, 0.8f),
            new ColorF(1f, 1f, 0.1f, 0.8f),
            new ColorF(1f, 1f, 0.5f, 0.3f),
            new ColorF(0.3f, 0.3f, 0.3f, 0.1f),
            new ColorF(0f, 0f, 0f, 0f)
    });

    private final Point2F _direction;

    public FireParticleDef(Point2F direction) {
        _direction = direction;
    }

    @Override
    public void initialize(Particle p) {
        p.lifespan = 5f;

        float size = Rnd.nextFloat(0.4f, 0.6f);
        p.dimensions = new Point2F(size, size);

        float c = Rnd.nextFloat(0.1f, 0.3f);

        p.texture = _tx;
        p.flags = Particle.ALPHA_LIFESPAN;
        p.blendMode = BlendMode.ADDITIVE;

        p.color = new ColorF(c * 3, c * 1, 0.0f, 0.9f);
        p.angle = Rnd.nextFloat((float)(Math.PI * 2f));

        float spread = 0.25f;
        float speed = 0.8f;
        p.velocity = new Point3F(
                _direction.x * speed + Rnd.nextFloat(-spread, spread),
                _direction.y * speed + Rnd.nextFloat(-spread, spread),
                4f);
    }

    @Override
    public void update(Particle p, float age) {
        p.dimensions = p.dimensions.add(0.003f);
        p.color = _gradient.getColor(age);
        p.angle += (1f - age) * 0.1f;
    }
}
