package model.definitions.particles;

import engine.entities.Entity;
import engine.misc.*;
import engine.particles.IParticleDefinition;
import engine.particles.Particle;
import engine.rendering.BlendMode;
import engine.resources.Loaders;
import engine.resources.texture.SubTexture;
import org.jbox2d.common.Vec2;

public class DirtParticleDef implements IParticleDefinition {
    private static final SubTexture _tx = new SubTexture(Loaders.getInstance().texture("splinters.png"), 0, 0, 64, 64);

    private final Entity _emitter;
    private final ColorF _color;

    public DirtParticleDef(Entity emitter, ColorF color) {
        _emitter = emitter;
        _color = color;
    }

    @Override
    public void initialize(Particle p) {
        p.lifespan = 1f;

        float size = Rnd.nextFloat(0.4f, 0.6f);
        p.dimensions = new Point2F(size, size);
        p.texture = _tx;
        p.blendMode = BlendMode.ADDITIVE;

        float c = Rnd.nextFloat(0.5f, 0.7f);
        p.color = _color.mul(c);

        Vec2 v = Vector.angleToVector((float)(_emitter.getAngle() + Math.PI));

        float speed = 10f;
        p.velocity = new Point3F(
                v.x + Rnd.nextFloat(-speed, speed),
                v.y + Rnd.nextFloat(-speed, speed),
                0.01f);
        p.angle = Rnd.nextFloat((float) (Math.PI * 2d));
    }

    @Override
    public void update(Particle p, float age) {
        if (p.dimensions.x > 0) {
            p.dimensions = p.dimensions.add(-0.03f);
        }

        p.color = new ColorF(p.color.r, p.color.g, p.color.b, p.color.a * 0.8f);
        p.velocity = new Point3F(
                p.velocity.x * 0.9f,
                p.velocity.y * 0.9f,
                p.velocity.z);

        p.angle = p.angle + 0.01f;
    }
}
