package model.definitions.particles;

import engine.misc.ColorF;
import engine.misc.Point2F;
import engine.misc.Point3F;
import engine.misc.Rnd;
import engine.particles.IParticleDefinition;
import engine.particles.Particle;
import engine.resources.Loaders;
import engine.resources.texture.SubTexture;

public class BloodParticleDef implements IParticleDefinition {
    private final static SubTexture _tx = new SubTexture(Loaders.getInstance().texture("blood.png"), 0, 0, 64, 64);

    @Override
    public void initialize(Particle p) {
        p.flags |= Particle.ALPHA_LIFESPAN;
        p.lifespan = 6;

        float size = Rnd.nextFloat(0.1f, 0.3f);
        p.dimensions = new Point2F(size, size);

        p.texture = _tx;

        float intensity = Rnd.nextFloat(0.3f, 1f);
        p.color = new ColorF(
                intensity,
                intensity,
                intensity,
                Rnd.nextFloat(0.25f, 0.5f));

        float v = 6.0f;
        p.velocity = new Point3F(
                Rnd.nextFloat(-v, v),
                Rnd.nextFloat(-v, v),
                0);

        p.angle = Rnd.nextFloat((float) (Math.PI * 2));
    }

    @Override
    public void update(Particle p, float age) {
        if (age < 0.1f)
            p.dimensions = p.dimensions.add(0.008f);

        if (age > 0.5)
            p.color = new ColorF(p.color.r, p.color.g, p.color.b, p.color.a * 0.99f);

        p.velocity = new Point3F(
                p.velocity.x * 0.85f,
                p.velocity.y * 0.85f,
                p.velocity.z);
    }
}
