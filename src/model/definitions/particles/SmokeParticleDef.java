package model.definitions.particles;

import engine.misc.*;
import engine.particles.IParticleDefinition;
import engine.particles.Particle;
import engine.rendering.BlendMode;
import engine.resources.Loaders;
import engine.resources.texture.SubTexture;

public class SmokeParticleDef implements IParticleDefinition {
    private static final SubTexture _tx = new SubTexture(Loaders.getInstance().texture("smoke_4.png"), 0, 0, 128, 128);
    private static LinearGradient _gradient = new LinearGradient(new ColorF[] {
            new ColorF(1f, 1f, 1f, 0.1f),
            new ColorF(0.3f, 0.3f, 0.35f, 0.3f),
            new ColorF(0, 0, 0, 0)
    });

    private final Point2F _direction;

    public SmokeParticleDef(Point2F direction) {
        _direction = direction;
    }

    @Override
    public void initialize(Particle p) {
        p.lifespan = 6f;

        float size = Rnd.nextFloat(0.1f, 0.2f);
        p.dimensions = new Point2F(size, size);

        float c = Rnd.nextFloat(0.1f, 0.3f);

        p.texture = _tx;
        p.flags = Particle.ALPHA_LIFESPAN;
        p.color = new ColorF(c * 3, c * 1, 0.0f, 0.9f);
        p.angle = Rnd.nextFloat((float)(Math.PI * 2f));
        p.blendMode = BlendMode.ALPHA;

        float spread = 0.25f;
        float speed = 0.8f;
        p.velocity = new Point3F(
                _direction.x * speed + Rnd.nextFloat(-spread, spread),
                _direction.y * speed + Rnd.nextFloat(-spread, spread),
                5f);
    }

    @Override
    public void update(Particle p, float age) {
        p.dimensions = p.dimensions.add(0.005f);
        p.color = _gradient.getColor(age);
        p.angle = p.angle + 0.005f;
    }
}
