package model;

import engine.entities.Entity;
import org.jbox2d.collision.shapes.MassData;
import org.jbox2d.dynamics.World;
import org.jbox2d.dynamics.joints.Joint;
import org.jbox2d.dynamics.joints.JointDef;
import org.jbox2d.dynamics.joints.WeldJointDef;

/**
 * Contact class to allow an entity (i.e. the player) to grab another entity
 */
public class Grip {
    private World _world;
    private final Entity _entity;
    private boolean _isGripping;
    private Joint _joint;
    private JointDef _create;
    private boolean _release;

    public Grip(Entity gripper) {
        _entity = gripper;
    }

    public Entity getSource() { return _entity; }
    public Entity getTarget() {
        if (_joint == null) return null;
        return (Entity)_joint.getBodyB().getUserData();
    }

    public boolean isGripping() { return _joint != null; }

    public void processGrip() {
        if (_world == null) {
            _world = _entity.getBody().getWorld();
        }

        if (_release) {
            if (_joint != null) {
                if (!getTarget().isDestroyed()) {
                    getTarget().getBody().resetMassData();
                }
                _world.destroyJoint(_joint);
            }
            _release = false;
            _joint = null;
            _create = null;

            return;
        }

        if (_create != null) {
            _joint = _world.createJoint(_create);
            if (_joint != null) {
                MassData md = new MassData();
                getTarget().getBody().getMassData(md);
                md.I /= 500;
                md.mass /= 500;
                getTarget().getBody().setMassData(md);

                _isGripping = false;
                _release = false;
                _create = null;
            }
        }
    }

    public void startGrip() {
        _isGripping = _joint == null;
    }

    public boolean tryGrip(Entity gripped) {
        if (!_isGripping) {
            return false;
        }

        WeldJointDef jd = new WeldJointDef();
        jd.initialize(
                _entity.getBody(),
                gripped.getBody(),
                _entity.getBody().getLocalCenter()
                );

        jd.collideConnected = true;

        _create = jd;
        _release = false;

        return true;
    }

    public void endGrip() {
        if (_joint == null) {
            return;
        }

        _release = true;
    }
}
