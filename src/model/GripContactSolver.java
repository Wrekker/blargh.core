package model;

import engine.collisions.IContactSolver;
import engine.entities.Entity;

public class GripContactSolver implements IContactSolver {
    private Entity _entity;

    public GripContactSolver(Entity entity) {
        _entity = entity;
    }

    @Override
    public void beginContact(Object data) {
        ((Grip)data).tryGrip(_entity);
    }

    @Override
    public void endContact(Object data) {
        ((Grip)data).endGrip();
    }
}
