package model;

import engine.entities.Entity;
import engine.misc.GameTime;
import org.jbox2d.common.Vec2;


/**
 * Interface for player-equippable items.
 */
public interface IImplement {
    boolean use(Entity source, Vec2 location, float direction, GameTime time);
}
