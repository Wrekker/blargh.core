package model;

import engine.events.ISubscriber;
import engine.events.Unsubscriber;

public interface IHealthManager {
    void setMaximumHealth(int health);
    int getMaximumHealth();
    int getHealth();
    void applyDamage(int dmg);
    boolean isDead();
    Unsubscriber<HealthEventArgs> onHealthChange(ISubscriber<HealthEventArgs> subscriber);
}
