package model;

import engine.events.Event;
import engine.events.ISubscriber;
import engine.events.Unsubscriber;

/**
 * Simple case of health manager. No mitigation, no frills.
 */
public class DirectHealthManager implements IHealthManager {
    private int _maxHealth;
    private int _health;
    private Event<HealthEventArgs> _healthChange = new Event<HealthEventArgs>();

    public DirectHealthManager(int maxHealth){
        setMaximumHealth(maxHealth);
    }

    @Override
    public void setMaximumHealth(int health) {
        int chg = health - _health;

        _health = health;
        _maxHealth = health;

        _healthChange.raiseEvent(new HealthEventArgs(this, _health, chg));
    }

    @Override
    public int getMaximumHealth() {
        return _maxHealth;
    }

    @Override
    public int getHealth() {
        return _health;
    }

    @Override
    public void applyDamage(int dmg) {
        _health -= dmg;
        _healthChange.raiseEvent(new HealthEventArgs(this, _health, dmg));
    }

    @Override
    public boolean isDead(){
        return _health <= 0;
    }

    public Unsubscriber<HealthEventArgs> onHealthChange(ISubscriber<HealthEventArgs> subscriber) {
        return _healthChange.addSubscriber(subscriber);
    }
}
