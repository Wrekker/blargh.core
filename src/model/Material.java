package model;

import engine.physics.MaterialDef;

/**
 * List of basic materials
 */
public class Material {
    public static final MaterialDef None = new MaterialDef(0, 0, 0);
    public static final MaterialDef Concrete = new MaterialDef(0.1f, 100, 0f);
    public static final MaterialDef Metal = new MaterialDef(0.01f, 20, 0.00f);
    public static final MaterialDef Wood = new MaterialDef(0.1f, 5, 0.1f);
}
