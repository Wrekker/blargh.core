package model;

import engine.misc.ColorF;
import engine.misc.GameTime;
import engine.misc.RectangleF;
import engine.rendering.IImplicitOrderView;
import engine.resources.Loaders;
import engine.ui.nodes.TextUiElement;
import model.entities.PlayerEntity;

/**
 * UI element to display player score
 */
public class ScoreBar extends TextUiElement {
    private final PlayerEntity _player;

    public ScoreBar(PlayerEntity player) {
        _player = player;
        setColor(new ColorF(1f,1,1,1f));
        setPadding(new RectangleF(10,10,10,10));
        setTypeface(Loaders.getInstance().typeface("trebuchet24.typeface"));
    }

    @Override
    public void render(IImplicitOrderView view, GameTime time) {
        setText("SCORE:\n" + _player.getScore().getScore());
        super.render(view, time);
    }
}
