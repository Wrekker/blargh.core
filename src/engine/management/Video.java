package engine.management;

import engine.rendering.IRenderView;

public class Video {
    private static IRenderView _view;

    public static void initialize(IRenderView view) {
        _view = view;
    }

    public static IRenderView getView() { return _view; }
}
