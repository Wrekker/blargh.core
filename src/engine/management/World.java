package engine.management;

import engine.entities.Entity;
import engine.entities.IEntityDef;
import engine.entities.IEntityManager;
import engine.misc.GameTime;
import engine.misc.Point2F;
import org.jbox2d.common.Vec2;

public class World {
    IEntityManager _entityManager;
    org.jbox2d.dynamics.World _b2dWorld;

    public void initialize(IEntityManager entityManager, Point2F gravity) {
        _b2dWorld = new org.jbox2d.dynamics.World(new Vec2(gravity.x, gravity.y), true);
        _entityManager = entityManager;
    }

    public void setGravity(Point2F v) {
        _b2dWorld.setGravity(new Vec2(v.x, v.y));
    }

    public Point2F getGravity() {
        Vec2 v = _b2dWorld.getGravity();
        return new Point2F(v.x, v.y);
    }

    public Entity createEntity(IEntityDef def) {
        return _entityManager.createEntity(def);
    }

    public void update(GameTime time) {
        _entityManager.update(time);
        _b2dWorld.step(time.getFrameTime(), 8, 4);
    }
}
