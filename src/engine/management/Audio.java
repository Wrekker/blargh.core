package engine.management;

import engine.audio.IAudioManager;
import engine.audio.IAudioSample;
import engine.audio.channels.ParametricAudioChannel;
import engine.misc.GameTime;

public class Audio {
    private static IAudioManager _manager;

    public static void initialize(IAudioManager manager) {
        _manager = manager;
    }

    public static ParametricAudioChannel play(IAudioSample sample) {
        ParametricAudioChannel ret = _manager.createAutoChannel();
        ret.play(sample);
        return ret;
    }

    public static ParametricAudioChannel createChannel() {
        return _manager.createManualChannel();
    }

    public static void update(GameTime time) {
        _manager.update(time);
    }
}
