package engine.animation;

import engine.misc.GameTime;
import engine.resources.texture.SubTexture;

public interface IAnimationManager {
    void setAnimation(String name);
    void registerFrameTrigger(String name, IAnimationTrigger trigger);
    SubTexture currentFrame(GameTime time);
    void setTimescale(float scale);

    IAnimationInstance getAnimation();

    float getTimescale();
}
