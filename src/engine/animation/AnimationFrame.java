package engine.animation;

import engine.resources.texture.SubTexture;

public class AnimationFrame {
    private final String _name;
    private final SubTexture _texture;

    public AnimationFrame(String name, SubTexture texture) {
        _name = name;
        _texture = texture;
    }

    public String getName() { return _name; }
    public SubTexture getTexture() { return _texture; }
}
