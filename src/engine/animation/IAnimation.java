package engine.animation;

import engine.resources.IResource;

public interface IAnimation extends IResource {
    IAnimationInstance getInstance();
}
