package engine.animation;

import engine.misc.GameTime;

public interface IAnimationTrigger {
    public void invoke(GameTime time);
}
