package engine.animation;

import engine.resources.IResourceLoader;
import engine.resources.ManagedResource;

import java.util.HashMap;
import java.util.Map;

public class AnimationSet extends ManagedResource {
    private Map<String, IAnimation> _animationMap = new HashMap<String, IAnimation>();
    private IAnimation _defaultAnimation;
    private final String _path;

    public AnimationSet(String path, String[] names, IAnimation[] animations) {
        super(path);
        _path = path;
        for (int i = 0, animationsLength = animations.length; i < animationsLength; i++) {
            if (i == 0) {
                _defaultAnimation = animations[i];
            }
            _animationMap.put(names[i], animations[i]);
        }
    }

    public IAnimation getDefault() {
        return _defaultAnimation;
    }

    public IAnimation getAnimation(String name) {
        return _animationMap.get(name);
    }

    @Override
    public String getKey() { return _path; }

    @Override
    public void dispose(IResourceLoader<?> loader) { }
}
