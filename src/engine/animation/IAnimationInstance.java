package engine.animation;

import engine.misc.GameTime;

public interface IAnimationInstance {
    AnimationFrame getFrame(GameTime time, float timescale);
}
