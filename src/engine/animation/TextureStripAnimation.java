package engine.animation;

import engine.misc.GameTime;
import engine.resources.IResourceLoader;
import engine.resources.ManagedResource;
import engine.resources.texturesheet.TextureSheet;

import java.util.Map;
import java.util.NavigableMap;
import java.util.TreeMap;

public class TextureStripAnimation extends ManagedResource implements IAnimation {
    private final TextureSheet.StripInstance _strip;
    private final float _animationDuration;
    private final NavigableMap<Float, AnimationFrame> _frameMap = new TreeMap<Float, AnimationFrame>();
    private String _path;

    public TextureStripAnimation(String path, TextureSheet.StripInstance strip, float animationDuration, FrameTiming[] timings) {
        super(path);
        _path = path;
        _strip = strip;
        _animationDuration = animationDuration;

        for (FrameTiming timing : timings) {
            _frameMap.put(timing.getTiming(), new AnimationFrame(timing.getName(), _strip.getSubTexture(timing.getIndex())));
        }
    }

    public Instance getInstance() {
        return new Instance();
    }

    @Override
    public void dispose(IResourceLoader<?> loader) { }

    public class Instance implements IAnimationInstance {
        private float _elapsed = -1;

        public AnimationFrame getFrame(GameTime time, float timescale) {
            float scaledDuration = _animationDuration / timescale;
            float secs = time.getTotalSeconds();

            if (_elapsed < 0) {
                _elapsed = secs;
            }

            if (secs - _elapsed > scaledDuration) {
                _elapsed = secs;
            }

            float offset = (secs - _elapsed)  / scaledDuration;
            Map.Entry<Float, AnimationFrame> frame = _frameMap.floorEntry(offset);

            return frame.getValue();
        }
    }

}
