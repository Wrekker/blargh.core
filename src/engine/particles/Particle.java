package engine.particles;

import engine.misc.ColorF;
import engine.misc.Point2F;
import engine.misc.Point3F;
import engine.rendering.BlendMode;
import engine.resources.texture.SubTexture;

public class Particle {
    public final static int ALPHA_LIFESPAN = 1;

    public IParticleDefinition definition;
    public Point3F position = new Point3F(0,0, 0);
    public Point2F dimensions = new Point2F(0,0);
    public ColorF color = new ColorF(0,0,0);
    public Point3F velocity = new Point3F(1,1,1);
    public float angle = 0f;

    public int flags = 0;

    public boolean isNew = true;
    public float lifespan = 0;
    public float spawnedTime = 0;
    public BlendMode blendMode = BlendMode.ALPHA;
    public SubTexture texture = null;
}
