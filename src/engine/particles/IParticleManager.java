package engine.particles;

import engine.misc.GameTime;
import engine.rendering.IExplicitOrderView;

public interface IParticleManager {
    void render(IExplicitOrderView view, GameTime time);
    IParticleEmitter createEmitter(IParticleDefinition definition);
}
