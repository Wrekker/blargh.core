package engine.particles;

public interface IParticleDefinition {
    void initialize(Particle p);
    void update(Particle p, float age);
}
