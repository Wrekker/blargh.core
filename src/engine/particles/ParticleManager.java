package engine.particles;

import engine.misc.GameTime;
import engine.misc.Point3F;
import engine.rendering.IExplicitOrderView;
import engine.rendering.RenderingParameters;

import java.util.Iterator;
import java.util.LinkedList;

public class ParticleManager implements IParticleManager {
    private LinkedList<Particle> _particles;

    public ParticleManager() {
        _particles = new LinkedList<Particle>();
    }

    @Override
    public void render(IExplicitOrderView view, GameTime time) {
        for (Iterator<Particle> iterator = _particles.iterator(); iterator.hasNext(); ) {
            Particle pcl = iterator.next();

            if (pcl.isNew) {
                pcl.spawnedTime = time.getTotalSeconds();
                pcl.isNew = false;
            }

            float age = (time.getTotalSeconds() - pcl.spawnedTime) / pcl.lifespan;

            if (age >= 1f) {
                iterator.remove();
                continue;
            }

            pcl.definition.update(pcl, age);

            if ((pcl.flags & Particle.ALPHA_LIFESPAN) == Particle.ALPHA_LIFESPAN &&
                    pcl.color.a < 0.001f) {
                iterator.remove();
                continue;
            }

            Point3F v = pcl.velocity.mul(time.getFrameTime());

            pcl.position = new Point3F(
                    pcl.position.x + v.x,
                    pcl.position.y + v.y,
                    pcl.position.z + v.z);

            RenderingParameters params = new RenderingParameters();
            params.color = pcl.color;
            params.blendMode = pcl.blendMode;

            view.drawSubTexture(
                    pcl.position.x,
                    pcl.position.y,
                    pcl.position.z,

                    pcl.dimensions.x, pcl.dimensions.y,
                    pcl.angle,
                    pcl.texture,
                    params
            );
        }
    }

    public IParticleEmitter createEmitter(IParticleDefinition definition) {
        return new ParticleEmitter(definition);
    }

    public class ParticleEmitter implements IParticleEmitter {
        private final IParticleDefinition _definition;

        public ParticleEmitter(IParticleDefinition definition) {
            _definition = definition;
        }

        @Override
        public void emit(float x, float y) {
            emit(x, y, 0);
        }

        @Override
        public void emit(float x, float y, float angle) {
            Particle p = new Particle();
            p.isNew = true;
            p.position = new Point3F(x, y, 0);
            p.angle = angle;
            p.definition = _definition;

            _definition.initialize(p);

            _particles.add(p);
        }
    }
}
