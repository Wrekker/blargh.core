package engine.particles;


public interface IParticleEmitter {
    void emit(float x, float y);
    void emit(float x, float y, float angle);
}
