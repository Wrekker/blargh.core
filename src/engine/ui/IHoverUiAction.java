package engine.ui;

import engine.misc.Point2F;
import model.menu.Bubble;

public interface IHoverUiAction {
    Bubble onHover(Point2F point);
}
