package engine.ui.layouts;

public class GridLayoutPriority {


    private GridPriorityDirections _firstPriority;
    private GridPriorityDirections _secondaryPriority;

    public GridLayoutPriority() {
    }

    public GridPriorityDirections getFirstPriority() {
        return _firstPriority;
    }

    public GridPriorityDirections getSecondaryPriority() {
        return _secondaryPriority;
    }

    public void setFirstPriority(GridPriorityDirections firstPriority) {
        if (_secondaryPriority != null){
            if (_secondaryPriority == GridPriorityDirections.LEFT || _secondaryPriority == GridPriorityDirections.RIGHT)
            {
                if (firstPriority == GridPriorityDirections.UP || firstPriority == GridPriorityDirections.DOWN) {
                    _firstPriority = firstPriority;
                } else {
                    _firstPriority = GridPriorityDirections.RIGHT;
                }
            }
        } else {
            _firstPriority = firstPriority;
        }
    }

    public void setSecondaryPriority(GridPriorityDirections secondaryPriority) {
        if (_firstPriority != null) {
            if (_firstPriority == GridPriorityDirections.LEFT || _firstPriority == GridPriorityDirections.RIGHT) {
                if (secondaryPriority == GridPriorityDirections.UP || secondaryPriority == GridPriorityDirections.DOWN) {
                    _secondaryPriority = secondaryPriority;
                } else {
                    _secondaryPriority = GridPriorityDirections.DOWN;
                }
            } else {
                if (secondaryPriority == GridPriorityDirections.LEFT || secondaryPriority == GridPriorityDirections.RIGHT) {
                    _secondaryPriority = secondaryPriority;
                } else {
                    _secondaryPriority = GridPriorityDirections.RIGHT;
                }
            }
        }
    }
}
