package engine.ui.layouts;

import engine.misc.Point2F;
import engine.misc.RectangleF;
import engine.ui.ILayout;
import engine.ui.IUiElement;
import engine.ui.nodes.UiContainer;
import engine.ui.nodes.UiElement;

import java.util.Iterator;
import java.util.LinkedList;

public class FlowLayout implements ILayout {

    private FlowDirections _direction;
    private Overflow _overflow;

    LinkedList<RectangleF> boxes = new LinkedList<RectangleF>();

    public FlowLayout(FlowDirections direction, Overflow overflow) {
        _direction = direction;
        _overflow = overflow;
    }

    public FlowDirections getDirection() {
        return _direction;
    }

    public void setDirection(FlowDirections direction) {
        _direction = direction;
    }

    public Overflow isOverflow() {
        return _overflow;
    }

    public void setOverflow(Overflow overflow) {
        _overflow = overflow;
    }

    @Override
    public LinkedList<RectangleF> arrange(UiContainer container) {
        boxes.clear();

        if (_direction == FlowDirections.RIGHT)
            return flowRight(container, container.getActualBounds());
        return flowLeft(container, container.getActualBounds());
    }

    private LinkedList<RectangleF> flowLeft (UiContainer container, RectangleF containerBounds) {

        Iterator<UiElement> iterator = container.getChildren().descendingIterator();

        Point2F startPosition = new Point2F(containerBounds.x + containerBounds.w, containerBounds.y);

        float w, h;

        while (iterator.hasNext()) {

            IUiElement element = iterator.next();

            w = element.getPreferredSize().x + element.getPadding().x - element.getPadding().w;
            h = element.getPreferredSize().y + element.getPadding().y - element.getPadding().h;

            if (startPosition.x - w < containerBounds.x) {
                if (_overflow == Overflow.TRUE || startPosition.y + h < containerBounds.y + containerBounds.h) {
                    startPosition = new Point2F(containerBounds.x + containerBounds.w - w, startPosition.y + h);
                } else {
                    w = 0;
                    h = 0;
                }
            } else {
                startPosition = new Point2F(startPosition.x - w, startPosition.y);
            }

            boxes.add(new RectangleF(startPosition.x, startPosition.y, w, h));

        }

        return boxes;
    }

    private LinkedList<RectangleF> flowRight (UiContainer container, RectangleF containerBounds) {

        Point2F startPosition = new Point2F(containerBounds.x, containerBounds.y);

        float w, h;

        for (IUiElement element : container.getChildren()) {

            w = element.getPreferredSize().x + element.getPadding().x - element.getPadding().w;
            h = element.getPreferredSize().y + element.getPadding().y - element.getPadding().h;


            if (containerBounds.x + containerBounds.w - startPosition.x < w) {
                if (_overflow == Overflow.TRUE || containerBounds.y + containerBounds.h > startPosition.y + h) {
                    startPosition = new Point2F(containerBounds.x, startPosition.y + h);
                } else {
                   w = 0;
                   h = 0;
                }
            }

            boxes.add(new RectangleF(startPosition.x, startPosition.y, w, h));

            startPosition = new Point2F(startPosition.x + w, startPosition.y);
        }

        return boxes;
    }
}
