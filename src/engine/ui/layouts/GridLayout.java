package engine.ui.layouts;

import engine.misc.RectangleF;
import engine.ui.ILayout;
import engine.ui.nodes.UiContainer;
import engine.ui.nodes.UiElement;

import java.util.LinkedList;

public class GridLayout implements ILayout {

    public static LayoutProperty X = LayoutProperty.create("X", Integer.class, 0);
    public static LayoutProperty Y = LayoutProperty.create("Y", Integer.class, 0);

    private int _x;
    private int _y;
    private GridLayoutPriority _layoutPriority;

    LinkedList<RectangleF> boxes = new LinkedList<RectangleF>();

    public GridLayout(int x, int y, GridLayoutPriority layoutPriority) {
        _layoutPriority = layoutPriority;
        setX(x);
        setY(y);
    }

    public GridLayoutPriority getLayoutPriority() {
        return _layoutPriority;
    }

    public void setLayoutPriority(GridLayoutPriority layoutPriority) {
        _layoutPriority = layoutPriority;
    }

    public int getX() {
        return _x;
    }

    public void setX(int x) {
        if (x != 0) {
            _x = x;
        } else _x = 1;
    }

    public int getY() {
        return _y;
    }

    public void setY(int y) {
        if (y != 0) {
            _y = y;
        } else _y = 1;
    }

    @Override
    public LinkedList<RectangleF> arrange(UiContainer container) {
        boxes.clear();

        float gridBoxWidth = container.getActualBounds().w / _x;
        float gridBoxHeight = container.getActualBounds().h / _y;

        //This only checks for the correct priority and then executes.
        if (_layoutPriority.getFirstPriority() == GridPriorityDirections.DOWN) {

            if (_layoutPriority.getSecondaryPriority() == GridPriorityDirections.RIGHT)
                return downRight(container, container.getActualBounds(), gridBoxWidth, gridBoxHeight);
            return downLeft(container, container.getActualBounds(), gridBoxWidth, gridBoxHeight);

        } else if (_layoutPriority.getFirstPriority() == GridPriorityDirections.UP) {

            if (_layoutPriority.getSecondaryPriority() == GridPriorityDirections.RIGHT)
                return upRight(container, container.getActualBounds(), gridBoxWidth, gridBoxHeight);
            return upLeft(container, container.getActualBounds(), gridBoxWidth, gridBoxHeight);

        } else if (_layoutPriority.getFirstPriority() == GridPriorityDirections.LEFT) {

            if (_layoutPriority.getSecondaryPriority() == GridPriorityDirections.DOWN)
                return leftDown(container, container.getActualBounds(), gridBoxWidth, gridBoxHeight);
            return leftUp(container, container.getActualBounds(), gridBoxWidth, gridBoxHeight);

        } else if (_layoutPriority.getFirstPriority() == GridPriorityDirections.RIGHT) {

            if (_layoutPriority.getSecondaryPriority() == GridPriorityDirections.DOWN)
                return rightDown(container, container.getActualBounds(), gridBoxWidth, gridBoxHeight);
            return rightUp(container, container.getActualBounds(), gridBoxWidth, gridBoxHeight);
        }

        return null;
    }

    private LinkedList<RectangleF> rightUp(UiContainer container, RectangleF containerBounds, float gridBoxWidth, float gridBoxHeight) {

        for (UiElement e : container.getChildren()) {
            float w = e.getPreferredSize().x, h = e.getPreferredSize().y;
            for (float y = containerBounds.y + containerBounds.h - h; y > containerBounds.y; y -= gridBoxHeight) {
                for (float x = containerBounds.x; x < containerBounds.x + containerBounds.h; x += gridBoxWidth) {
                    boxes.add(new RectangleF(x, y, w, h));
                }
            }
            boxes.add(new RectangleF(0, 0, 0, 0));
        }
        return boxes;
    }

    private LinkedList<RectangleF> leftUp(UiContainer container, RectangleF containerBounds, float gridBoxWidth, float gridBoxHeight) {

        for (UiElement e : container.getChildren()) {
            float w = e.getPreferredSize().x, h = e.getPreferredSize().y;
            for (float y = containerBounds.y + containerBounds.h - h; y > containerBounds.y; y -= gridBoxHeight) {
                for (float x = containerBounds.x + containerBounds.w - w; x > containerBounds.x; x -= gridBoxWidth) {
                    boxes.add(new RectangleF(x, y, w, h));
                }
            }
            boxes.add(new RectangleF(0, 0, 0, 0));
        }
        return boxes;
    }

    private LinkedList<RectangleF> leftDown(UiContainer container, RectangleF containerBounds, float gridBoxWidth, float gridBoxHeight) {

        for (UiElement e : container.getChildren()) {
            float w = e.getPreferredSize().x, h = e.getPreferredSize().y;
            for (float y = containerBounds.y; y < containerBounds.y + containerBounds.h; y += gridBoxHeight) {
                for (float x = containerBounds.x + containerBounds.w - w; x > containerBounds.x; x -= gridBoxWidth) {
                    boxes.add(new RectangleF(x, y, w, h));
                }
            }
            boxes.add(new RectangleF(0, 0, 0, 0));
        }
        return boxes;
    }

    private LinkedList<RectangleF> upLeft(UiContainer container, RectangleF containerBounds, float gridBoxWidth, float gridBoxHeight) {

        for (UiElement e : container.getChildren()) {
            float w = e.getPreferredSize().x, h = e.getPreferredSize().y;
            for (float x = containerBounds.x + containerBounds.w - w; x > containerBounds.x; x -= gridBoxWidth) {
                for (float y = containerBounds.y + containerBounds.h - h; y > containerBounds.y; y -= gridBoxHeight) {
                    boxes.add(new RectangleF(x, y, w, h));
                }
            }
            boxes.add(new RectangleF(0, 0, 0, 0));
        }
        return boxes;
    }

    private LinkedList<RectangleF> upRight(UiContainer container, RectangleF containerBounds, float gridBoxWidth, float gridBoxHeight) {

        for (UiElement e : container.getChildren()) {
            float w = e.getPreferredSize().x, h = e.getPreferredSize().y;
            for (float x = containerBounds.x; x < containerBounds.x + containerBounds.h; x += gridBoxWidth) {
                for (float y = containerBounds.y + containerBounds.h - h; y > containerBounds.y; y -= gridBoxHeight) {
                    boxes.add(new RectangleF(x, y, w, h));
                }
            }
            boxes.add(new RectangleF(0, 0, 0, 0));
        }
        return boxes;
    }

    private LinkedList<RectangleF> downLeft(UiContainer container, RectangleF containerBounds, float gridBoxWidth, float gridBoxHeight) {

        for (UiElement e : container.getChildren()) {
            float w = e.getPreferredSize().x, h = e.getPreferredSize().y;
            for (float x = containerBounds.x + containerBounds.w - w; x > containerBounds.x; x -= gridBoxWidth) {
                for (float y = containerBounds.y; y < containerBounds.y + containerBounds.h; y += gridBoxHeight) {
                    boxes.add(new RectangleF(x, y, w, h));
                }
            }
            boxes.add(new RectangleF(0, 0, 0, 0));
        }
        return boxes;
    }

    private LinkedList<RectangleF> downRight(UiContainer container, RectangleF containerBounds, float gridBoxWidth, float gridBoxHeight) {

        for (UiElement e : container.getChildren()) {
            float w = e.getPreferredSize().x, h = e.getPreferredSize().y;
            for (float x = containerBounds.x; x < containerBounds.x + containerBounds.w; x += gridBoxWidth) {
                for (float y = containerBounds.y; y < containerBounds.y + containerBounds.h; y += gridBoxHeight) {
                    boxes.add(new RectangleF(x, y, w, h));
                }
            }
            boxes.add(new RectangleF(0, 0, 0, 0));
        }
        return boxes;
    }

    private LinkedList<RectangleF> rightDown(UiContainer container, RectangleF containerBounds, float gridBoxWidth, float gridBoxHeight) {

        for (UiElement e : container.getChildren()) {
            float w = e.getPreferredSize().x, h = e.getPreferredSize().y;
            for (float y = containerBounds.y; y < containerBounds.y + containerBounds.h; y += gridBoxHeight) {
                for (float x = containerBounds.x; x < containerBounds.x + containerBounds.w; x += gridBoxWidth) {
                    boxes.add(new RectangleF(x, y, w, h));
                }
            }
            boxes.add(new RectangleF(0, 0, 0, 0));
        }
        return boxes;
    }
}
