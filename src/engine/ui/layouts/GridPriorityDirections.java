package engine.ui.layouts;

public enum GridPriorityDirections {
    LEFT,
    RIGHT,
    UP,
    DOWN,
}
