package engine.ui.layouts;

import engine.misc.RectangleF;
import engine.ui.ILayout;
import engine.ui.nodes.UiContainer;
import engine.ui.nodes.UiElement;

import java.util.LinkedList;

public class AbsoluteLayout implements ILayout {
    public static final LayoutProperty X = LayoutProperty.create("X", float.class, 0f);
    public static final LayoutProperty Y = LayoutProperty.create("Y", float.class, 0f);

    @Override
    public LinkedList<RectangleF> arrange(UiContainer container) {
        LinkedList<RectangleF> ret = new LinkedList<RectangleF>();
        RectangleF p = container.getActualBounds();

        for (UiElement e : container.getChildren()) {
            RectangleF pad = e.getPadding();

            ret.add(new RectangleF(
                    p.x + pad.x + (Float)e.getLayoutProperty(X),
                    p.y + pad.y + (Float)e.getLayoutProperty(Y),
                    e.getPreferredSize().x - pad.w,
                    e.getPreferredSize().y - pad.h));
        }

        return ret;
    }
}
