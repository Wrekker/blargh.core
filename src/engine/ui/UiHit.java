package engine.ui;

import engine.misc.Point2F;
import engine.ui.nodes.UiElement;

public class UiHit {
    private final UiElement _element;
    private final Point2F _location;

    public UiHit(UiElement element, Point2F location) {
        _element = element;
        _location = location;
    }

    public Point2F getLocation() { return _location; }
    public UiElement getElement() { return _element; }
}
