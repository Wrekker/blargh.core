package engine.ui;

public class AnchorPoint {
    public static final int TOP = 0;
    public static final int LEFT = 0;
    public static final int RIGHT = 1;
    public static final int BOTTOM = 2;
    public static final int H_CENTER = 4;
    public static final int V_CENTER = 8;
}
