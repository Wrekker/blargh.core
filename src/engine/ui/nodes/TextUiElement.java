package engine.ui.nodes;

import engine.misc.ColorF;
import engine.misc.GameTime;
import engine.misc.RectangleF;
import engine.misc.Size2F;
import engine.rendering.IImplicitOrderView;
import engine.rendering.RenderingParameters;
import engine.resources.typeface.GlyphParagraph;
import engine.resources.typeface.Typeface;

public class TextUiElement extends UiElement {
    private String _text;
    private float _x;
    private float _y;
    private Typeface _typeface;
    private ColorF _color = new ColorF(1,1,1,1);
    private boolean _autoSize;

    GlyphParagraph _paragraph;

    public TextUiElement() { }

    public String getText() { return _text; }
    public void setText(String text) {
        if (_text == null && text != null || !_text.equalsIgnoreCase(text)) {
            _text = text;
            _paragraph = _typeface.getGlyphs(_text.toCharArray());
        }
    }

    public Typeface getTypeface() { return _typeface; }
    public void setTypeface(Typeface typeface) { _typeface = typeface; }

    public ColorF getColor() { return _color; }
    public void setColor(ColorF color) { _color = color; }

    public void setAutoSize(boolean enable) { _autoSize = enable; }
    public boolean getAutoSize() { return _autoSize; }

    @Override
    public void setPreferredSize(Size2F rect) {
        super.setPreferredSize(rect);
    }

    @Override
    public Size2F getPreferredSize() {
        if (!_autoSize) {
            return super.getPreferredSize();
        } else {
            if (_typeface == null || _text == null || _paragraph == null) {
                return new Size2F(0,0);
            }
            return _paragraph.size;
        }
    }

    @Override
    public void render(IImplicitOrderView view, GameTime time) {
        RenderingParameters params = new RenderingParameters();
        params.color = _color;

        RectangleF r = getActualBounds();
        view.drawText(r.x, r.y, _text, _typeface, params);
    }
}