package engine.ui.nodes;

import engine.misc.GameTime;
import engine.misc.RectangleF;
import engine.rendering.IImplicitOrderView;
import engine.resources.texture.Texture;

public class ButtonUiElement extends UiElement {
    private Texture _texture;

    public ButtonUiElement() { }

    public Texture getTexture() { return _texture; }
    public void setTexture(Texture texture) { _texture = texture; }

    @Override
    public void render(IImplicitOrderView view, GameTime time) {
        RectangleF r = getActualBounds();
        view.drawTexture(r.x, r.y, r.w, r.h, 0, _texture);
    }
}
