package engine.ui.nodes;

import java.util.Iterator;
import java.util.LinkedList;


public class UiContainerLinkedList<T extends UiElement> implements Iterable<UiElement> {
    private final LinkedList<UiElement> _list = new LinkedList<UiElement>();
    private final UiContainer _parent;

    public UiContainerLinkedList(UiContainer parent) {
        _parent = parent;
    }

    public boolean isEmpty() {
        return _list.isEmpty();
    }

    public String toString() {
        return _list.toString();
    }

    public boolean equals(Object o) {
        return _list.equals(o);
    }

    public int hashCode() {
        return _list.hashCode();
    }

    public Iterator<UiElement> iterator() {
        return _list.iterator();
    }

    public UiElement getFirst() {
        return _list.getFirst();
    }

    public UiElement getLast() {
        return _list.getLast();
    }

    public UiElement removeFirst() {
        UiElement ret = _list.removeFirst();
        ret._parent = null;

        return ret;
    }

    public UiElement removeLast() {
        UiElement ret = _list.removeLast();
        ret._parent = null;

        return ret;
    }

    public void addFirst(UiElement t) {
        t._parent = _parent;
        _list.addFirst(t);
    }

    public void addLast(UiElement t) {
        t._parent = _parent;
        _list.addLast(t);
    }

    public boolean contains(Object o) {
        return _list.contains(o);
    }

    public int size() {
        return _list.size();
    }

    public boolean add(UiElement t) {
        t._parent = _parent;
        return _list.add(t);
    }

    public void clear() {
        _list.clear();
    }

    public UiElement peek() {
        return _list.peek();
    }

    public UiElement element() {
        return _list.element();
    }

    public UiElement poll() {
        return _list.poll();
    }

    public UiElement remove() {
        UiElement ret = _list.remove();
        ret._parent = null;

        return _list.remove();
    }

    public UiElement peekFirst() {
        return _list.peekFirst();
    }

    public UiElement peekLast() {
        return _list.peekLast();
    }

    public UiElement pollFirst() {
        return _list.pollFirst();
    }

    public UiElement pollLast() {
        return _list.pollLast();
    }

    public void push(UiElement t) {
        t._parent = _parent;
        _list.push(t);
    }

    public UiElement pop() {
        UiElement ret = _list.pop();
        ret._parent = null;

        return ret;
    }

    public Iterator<UiElement> descendingIterator() {
        return _list.descendingIterator();
    }

    public Object clone() {
        return _list.clone();
    }

    public Object[] toArray() {
        return _list.toArray();
    }

    public <T> T[] toArray(T[] a) {
        return _list.toArray(a);
    }
}
