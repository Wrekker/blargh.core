package engine.ui.nodes;

import engine.misc.GameTime;
import engine.misc.Point2F;
import engine.misc.PolygonF;
import engine.misc.RectangleF;
import engine.rendering.IImplicitOrderView;
import engine.ui.ILayout;
import engine.ui.UiHit;

import java.util.Deque;
import java.util.LinkedList;

public class UiContainer extends UiElement {
    private UiContainerLinkedList<UiElement> _children = new UiContainerLinkedList<UiElement>(this);
    private ILayout _layout;

    public UiContainer(){
        super();
    }

    public ILayout getLayout() { return _layout; }
    public void setLayout(ILayout layout) { _layout = layout; }
    public UiContainerLinkedList<UiElement> getChildren() { return _children; }

    public void hitTest(Point2F loc, Deque<UiHit> hitStack) {
        for (UiElement e :_children) {
            if (e.getActualBounds() == null) {
                continue;
            }

            RectangleF bb = e.getActualBounds();
            if (bb.isPointInside(loc.x, loc.y)) {
                PolygonF cb = e.getClipping();

                if (cb != null && cb.isPointInside(loc.x - bb.x, loc.y - bb.y)) {

                    hitStack.push(new UiHit(e, loc));
                }

                if (e instanceof UiContainer) {
                    ((UiContainer)e).hitTest(new Point2F(loc.x - bb.x, loc.y - bb.y), hitStack);
                }
            }
        }
    }

    @Override
    public void update(RectangleF actualBounds, GameTime time) {
        super.update(actualBounds, time);

        if (_layout != null) {
            LinkedList<RectangleF> rects = _layout.arrange(this);

            for (UiElement e : _children) {
                e.update(rects.pop(), time);
            }
        }
    }

    @Override
    public void render(IImplicitOrderView view, GameTime time) {
        for (UiElement e : _children) {
            e.render(view, time);
        }
    }

}
