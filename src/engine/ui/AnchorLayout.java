package engine.ui;

import engine.misc.RectangleF;
import engine.misc.Size2F;

import java.util.LinkedList;

public class AnchorLayout implements ILayout {
    public static LayoutProperty Anchor = LayoutProperty.create("Anchor", int.class, 0);

    @Override
    public LinkedList<RectangleF> arrange(UiContainer container) {
        LinkedList<RectangleF> ret = new LinkedList<RectangleF>();

        RectangleF parentRect = container
                .getActualBounds()
                .contract(container.getPadding());

        for (UiElement e : container.getChildren()) {
            float l = 0, t = 0;

            int anchor = (Integer)e.getLayoutProperty(Anchor);

            Size2F size = e.getPreferredSize();

            l = parentRect.x;
            t = parentRect.y;

            if ((anchor & AnchorPoint.RIGHT) == AnchorPoint.RIGHT) {
                l = parentRect.x + (parentRect.w - size.x);
            }

            if ((anchor & AnchorPoint.BOTTOM) == AnchorPoint.BOTTOM) {
                t = parentRect.y + (parentRect.h - size.y);
            }

            if ((anchor & AnchorPoint.H_CENTER) == AnchorPoint.H_CENTER) {
                l = parentRect.x + (parentRect.w - size.x) / 2;
            }

            if ((anchor & AnchorPoint.V_CENTER) == AnchorPoint.V_CENTER) {
                t = parentRect.y + (parentRect.h - size.y) / 2;
            }

            ret.add(new RectangleF(l, t, size.x, size.y));
        }

        return ret;
    }
}
