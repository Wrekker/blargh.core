package engine.ui;

import engine.misc.Point2F;
import model.menu.Bubble;

public interface IDehoverUiAction {
    Bubble onDehover(Point2F point);
}
