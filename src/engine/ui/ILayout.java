package engine.ui;

import engine.misc.RectangleF;
import engine.ui.nodes.UiContainer;

import java.util.LinkedList;

public interface ILayout {
    LinkedList<RectangleF> arrange(UiContainer container);
}
