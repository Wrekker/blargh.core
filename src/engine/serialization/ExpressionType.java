package engine.serialization;

public enum ExpressionType {
    Nil,
    Binary,
    Number,
    String,
    Enumeration,
    Array,
    Structure,
    Resource, Map
}
