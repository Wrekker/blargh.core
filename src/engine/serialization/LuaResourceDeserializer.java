package engine.serialization;

import engine.resources.IResourceLoader;
import org.luaj.vm2.ast.Exp;
import org.luaj.vm2.ast.TableConstructor;
import org.luaj.vm2.ast.TableField;
import org.luaj.vm2.parser.LuaParser;
import org.luaj.vm2.parser.ParseException;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.lang.reflect.Array;
import java.lang.reflect.Field;
import java.util.*;

public class LuaResourceDeserializer implements IResourceDeserializer {
    LuaParser _parser;
    Deque<LuaParser> _parsers;
    Map<Class<?>, IResourceLoader<?>>  _primitiveLoaders = new HashMap<Class<?>, IResourceLoader<?>>();

    public LuaResourceDeserializer() {
        _parsers = new LinkedList<LuaParser>();
        _primitiveLoaders = new HashMap<Class<?>, IResourceLoader<?>>();
    }

    public void registerPrimitive(IResourceLoader<?> loader) {
        _primitiveLoaders.put(loader.getResourceType(), loader);
    }

    @Override
    public ISpecification deserialize(InputStream is, Class<?> type) throws ParseException, IOException, NoSuchFieldException, IllegalAccessException, InstantiationException, DeserializationException {
        if (_parser == null) {
            _parser = new LuaParser(is);
        } else {
            _parser.ReInit(is);
        }

        Exp e = _parser.Exp();

        return (ISpecification)exp(e, type);
    }

    public ISpecification deserialize(StringReader ip, Class<?> type) throws ParseException, IOException, NoSuchFieldException, IllegalAccessException, InstantiationException, DeserializationException {
        if (_parser == null) {
            _parser = new LuaParser(ip);
        } else {
            _parser.ReInit(ip);
        }

        return (ISpecification)exp(_parser.Exp(), type);
    }

    private Object exp(Exp e, Class<?> type) throws NoSuchFieldException, InstantiationException, IllegalAccessException, IOException, DeserializationException {
        if (e instanceof Exp.Constant) {
            return constant(e, type);
        } else if (e instanceof Exp.NameExp) {
            return enumeration(e, type);
        } else if (e instanceof Exp.UnopExp) {
            return unop(e, type);
        } else if (e instanceof TableConstructor && type.isArray()) {
            return array(e, type);
        } else if (e instanceof TableConstructor && ISpecification.class.isAssignableFrom(type)) {
            return struct(e, type);
        } else if (e instanceof TableConstructor) {
            return map(e, type);
        }

        throw new DeserializationException("Unknown expression of unknown type: " + e.toString());
    }

    private Object enumeration(Exp e, Class<?> type) throws DeserializationException {
        Exp.NameExp n = (Exp.NameExp)e;

        Object[] enums = type.getEnumConstants();
        for (int i = 0; i < enums.length; i++) {
            if (enums[i].toString().equals(n.name.name)) {
                return enums[i];
            }
        }

        throw new DeserializationException("Enum with unknown value: " + n.name.name);
    }

    private Object unop(Exp e, Class<?> type) throws IOException, NoSuchFieldException, IllegalAccessException, InstantiationException, DeserializationException {
        Exp.UnopExp u = (Exp.UnopExp)e;

        switch (u.op) {
            case 19: // negation
                return -(Float)exp(u.rhs, type);
        }

        throw new DeserializationException("Unary operator of unknown type: " + u.op);
    }

    private Object array(Exp e, Class<?> type) throws IOException, NoSuchFieldException, IllegalAccessException, InstantiationException, DeserializationException {
        TableConstructor tc = ((TableConstructor)e);
        Object target = Array.newInstance(type.getComponentType(), tc.fields.size());

        int idx = 0;
        for (Object f : tc.fields) {
            TableField field = (TableField)f;
            Array.set(target, idx++, exp(field.rhs, type.getComponentType()));
        }

        return target;
    }

    private Object map(Exp e, Class<?> type) throws IllegalAccessException, InstantiationException, NoSuchFieldException, IOException, DeserializationException {
        Hashtable target = new Hashtable();

        for (Object f : ((TableConstructor)e).fields) {
            TableField field = (TableField)f;

            Object key = field.name == null ? exp(field.index, Object.class) : field.name;
            target.put(key, exp(field.rhs, Object.class));
        }

        return target;
    }

    private Object struct(Exp e, Class<?> type) throws IllegalAccessException, InstantiationException, NoSuchFieldException, IOException, DeserializationException {
        Object target = type.newInstance();

        for (Object f : ((TableConstructor)e).fields) {
            TableField field = (TableField)f;

            Field refField = type.getField(((TableField) f).name);
            refField.set(target, exp(field.rhs, refField.getType()));
        }

        return target;
    }

    private Object constant(Exp e, Class<?> type) throws IOException, DeserializationException {
        Exp.Constant constant = (Exp.Constant) e;

        IResourceLoader<?> loader;
        if ((loader = _primitiveLoaders.get(type)) != null) {
            // If a primitive, load the resource via a loader
            String path = constant.value.tojstring();
            return loader.load(path);
        }

        if (constant.value.isnil()) {
            return null;
        }

        if (constant.value.isnumber()) {
            return constant.value.tofloat();
        }

        if (constant.value.isstring()) {
            String ret = constant.value.tojstring();
            return ret;
        }

        throw new DeserializationException("Constant of unknown type: " + constant.value.type());
    }

}
