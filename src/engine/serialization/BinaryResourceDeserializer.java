package engine.serialization;

import engine.resources.IResource;
import engine.resources.IResourceLoader;
import org.luaj.vm2.parser.ParseException;
import sun.misc.Compare;
import sun.misc.Sort;

import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Array;
import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;

public class BinaryResourceDeserializer implements IResourceDeserializer {
    Map<Class<?>, IResourceLoader<?>>  _primitiveLoaders = new HashMap<Class<?>, IResourceLoader<?>>();

    public BinaryResourceDeserializer() {
        _primitiveLoaders = new HashMap<Class<?>, IResourceLoader<?>>();
    }

    public void registerPrimitive(IResourceLoader<?> loader) {
        _primitiveLoaders.put(loader.getResourceType(), loader);
    }

    @Override
    public ISpecification deserialize(InputStream is, Class<?> type) throws ParseException, IOException, NoSuchFieldException, IllegalAccessException, InstantiationException, DeserializationException, ClassNotFoundException {
        DataInputStream dis = new DataInputStream(is);

        return (ISpecification)exp(dis, type);
    }

    private Object exp(DataInputStream dis, Class<?> type) throws NoSuchFieldException, InstantiationException, IllegalAccessException, IOException, DeserializationException, ClassNotFoundException {
        if (type.isArray()) {
            return array(dis, type);
        } else if (ISpecification.class.isAssignableFrom(type)) {
            return struct(dis, type);
        } else if (Map.class.isAssignableFrom(type)) {
            return map(dis, type);
        } else if (type.isEnum()) {
            return enumeration(dis, type);
        } else if (type.equals(Object.class)) {
            return unknown(dis, type);
        } else {
            return constant(dis, type);
        }
    }

    private Object unknown(DataInputStream dis, Class<?> type) throws IOException, DeserializationException, ClassNotFoundException, NoSuchFieldException, InstantiationException, IllegalAccessException {
        ExpressionType et = (ExpressionType)enumeration(dis, ExpressionType.class);

        String typeName;

        switch (et) {
            case Nil:
                return null;
            case Binary:
                return constant(dis, Byte.class);
            case Number:
                return constant(dis, Float.class);
            case String:
                return constant(dis, String.class);
            case Enumeration:
                typeName = dis.readUTF();
                return enumeration(dis, Class.forName(typeName));
            case Array:
                typeName = dis.readUTF();
                return array(dis, Class.forName(typeName));
            case Structure:
                typeName = dis.readUTF();
                return struct(dis, Class.forName(typeName));
            case Map:
                return map(dis, type);
            case Resource:
                typeName = dis.readUTF();
                return constant(dis, Class.forName(typeName));
        }

        throw new DeserializationException("Unknown type: " + type.toString());
    }

    private Object enumeration(DataInputStream dis, Class<?> type) throws DeserializationException, IOException {
        return (Enum<?>)type.getEnumConstants()[dis.readByte()];
    }

    private Object array(DataInputStream dis, Class<?> type) throws IOException, NoSuchFieldException, IllegalAccessException, InstantiationException, DeserializationException, ClassNotFoundException {
        Class<?> comType = type.getComponentType();

        int count = dis.readInt();
        Object target = Array.newInstance(comType, count);

        for (int i = 0; i < count; i++) {
            Object value = exp(dis, comType);
            Array.set(target, i, value);
        }

        return target;
    }

    private Object map(DataInputStream dis, Class<?> type) throws IllegalAccessException, InstantiationException, NoSuchFieldException, IOException, DeserializationException, ClassNotFoundException {
        Map target = new HashMap();

        int count = dis.readShort();
        for (int i = 0; i < count; i++) {
            Object key = exp(dis, Object.class);
            Object value = exp(dis, Object.class);

            target.put(key, value);
        }

        return target;
    }

    private Object struct(DataInputStream dis, Class<?> type) throws IllegalAccessException, InstantiationException, NoSuchFieldException, IOException, DeserializationException, ClassNotFoundException {
        Object target = type.newInstance();
        Field[] fields = type.getFields();

        Sort.quicksort(fields, new Compare() {
            @Override
            public int doCompare(Object o, Object o1) {
                return ((Field) o).getName().compareTo(((Field) o1).getName());
            }
        });

        int m = dis.readInt();
        for (Field f : fields) {
            if ((m & 1) == 1) {
                f.set(target, exp(dis, f.getType()));
            }
            m >>= 1;
        }

        return target;
    }

    private Object constant(DataInputStream dis, Class<?> type) throws IOException, DeserializationException {
        if (type.equals(byte.class) || Byte.class.isAssignableFrom(type)) {
            return dis.readByte();
        } else if (type.equals(float.class) || Float.class.isAssignableFrom(type)) {
            return dis.readFloat();
        } else if (type.equals(String.class)) {
            return dis.readUTF();
        } else if (IResource.class.isAssignableFrom(type)) {
            IResourceLoader<?> loader = _primitiveLoaders.get(type);

            if (loader == null) {
                throw new DeserializationException("No loader registered for type " + type.toString());
            }

            // If a primitive, load the resource via a loader
            return loader.load(dis.readUTF());
        }

        throw new DeserializationException("Value of unknown type: " + type.getName());
    }



}
