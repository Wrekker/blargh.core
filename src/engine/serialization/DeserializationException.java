package engine.serialization;

public class DeserializationException extends Exception {

    public DeserializationException(String msg) {
        super(msg);
    }
}
