package engine.serialization;

import engine.resources.IResource;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.lang.reflect.Array;
import java.lang.reflect.Field;
import java.util.Map;

public class LuaResourceSerializer implements IResourceSerializer {
    private DataOutputStream _os;

    public LuaResourceSerializer() { }

    @Override
    public void serialize(ISpecification obj, OutputStream os) throws IOException, IllegalAccessException {
        _os = new DataOutputStream(os);
        exp(obj, new SerializationInfo());
        _os = null;
    }

    private void exp(Object e, SerializationInfo data) throws IOException, IllegalAccessException {
        if (e != null && e.getClass().isArray()) {
            array(e, data);
        } else if (e instanceof ISpecification) {
            struct(e, data);
        } else if (e instanceof Map) {
            map(e, data);
        } else if (e instanceof Enum<?>) {
            enumeration(e, data);
        } else {
            constant(e, data);
        }
    }

    private void array(Object e, SerializationInfo data) throws IOException, IllegalAccessException {
        int l = Array.getLength(e);

        _os.writeChars("{ ");
        if (data.lf()) {
            linefeed(true);
            indent(data.in());
        }

        for (int i = 0; i < l; i++) {
            exp(Array.get(e, i), data);
            _os.writeChars(", ");

            linefeed(data.lf());
        }

        _os.writeChars(" }");
    }

    private void enumeration(Object e, SerializationInfo data) throws IOException {
        _os.writeChars(((Enum<?>) e).name());
    }

    private void map(Object e, SerializationInfo data) throws IOException, IllegalAccessException {
        Map m = (Map) e;

        _os.writeChars("{ ");

        if (data.lf()) {
            linefeed(true);
            indent(data.in());
        }

        for (Object f : m.values()) {
            Map.Entry me = (Map.Entry)f;

            field(me.getKey());
            _os.writeChars(" = ");
            exp(me.getValue(), data);
            _os.writeChars(", ");

            linefeed(data.lf());
        }

        _os.writeChars("}");
    }

    private void struct(Object e, SerializationInfo data) throws IOException, IllegalAccessException {
        Class<?> cls = e.getClass();
        Field[] fields = cls.getFields();

        indent(data.in());
        _os.writeChars("{ ");
        if (linefeed(fields[0])) {
            linefeed(true);
        }

        for (Field f : fields) {
            boolean lf = linefeed(f);

            indent(data.in() + (indent(e) ? 1 : 0));
            field(f.getName());
            _os.writeChars(" = ");
            exp(f.get(e), data.lf(lf).in(true));
            _os.writeChars(", ");

            linefeed(lf);
        }

        indent(data.in());
        _os.writeChars("}");
    }

    private void constant(Object e, SerializationInfo data) throws IOException {
        if (e instanceof Float || (e != null && e.getClass() == float.class)) {
            if (((Float)e).intValue() == ((Float)e)) {
                _os.writeChars(Integer.toString(((Float) e).intValue()));
            } else {
                _os.writeChars(e.toString());
            }
        } else if (e instanceof String) {
            _os.writeChars("\"");
            _os.writeChars(escape((String) e));
            _os.writeChars("\"");
        } else if (e instanceof IResource) {
            constant(((IResource)e).getKey(), data);
        } else {
            _os.writeChars("nil");
        }
    }

    private void field(Object e) throws IOException {
        if (e instanceof Float || e.getClass() == float.class) {
            _os.writeChars("[");
            if (((Float)e).intValue() == ((Float)e)) {
                _os.writeChars(Integer.toString(((Float) e).intValue()));
            } else {
                _os.writeChars(e.toString());
            }
            _os.writeChars("]");
        } else if (e instanceof String) {
            if (needsEscape((String)e)) {
                _os.writeChars("[\"");
                _os.writeChars(escape((String)e));
                _os.writeChars("\"]");
            } else {
                _os.writeChars((String)e);
            }
        } else {
            _os.writeChars("nil");
        }
    }

    private String escape(String s) {
        return s
            .replace("\0x7", "\\a")
            .replace("\b", "\\b")
            .replace("\0xc", "\\f")
            .replace("\n", "\\n")
            .replace("\r", "\\r")
            .replace("\t", "\\t")
            .replace("\0xb", "\\v")
            .replace("\\", "\\\\")
            .replace("\"", "\\\"")
            .replace("'", "\\'")
            .replace("[", "\\[")
            .replace("]", "\\]");
    }

    private boolean needsEscape(String s) {
        for (int i = 0; i < s.length(); i++) {
             char c = s.charAt(i);

            if (    c == 7 ||
                    c == '\b' ||
                    c == 0xc ||
                    c == '\n' ||
                    c == '\r' ||
                    c == '\t' ||
                    c == 0xb ||
                    c == '\\' ||
                    c == '\"' ||
                    c == '\'' ||
                    c == '[' ||
                    c == ']')
                return true;
        }

        return false;
    }

    private boolean linefeed(Field o) {
        return o.getAnnotation(Linefeed.class) != null;
    }

    private void linefeed(boolean f) throws IOException {
        if (f) { _os.writeChars("\n"); }
    }


    private boolean indent(Object o) {
        return o.getClass().getAnnotation(Indent.class) != null;
    }

    private void indent(int i) throws IOException {
        for (int j = 0; j < i; j++) {
            _os.writeChars("\t");
        }
    }

    private class SerializationInfo {
        private final boolean _lf;
        private final int _indent;

        private SerializationInfo(boolean lf, int indent) {
            _lf = lf;
            _indent = indent;
        }

        private SerializationInfo() {
            _lf = false;
            _indent = 0;
        }

        public boolean lf() { return _lf; }
        public SerializationInfo lf(boolean lf) { return new SerializationInfo(lf, _indent); }

        public int in() { return _indent; }
        public SerializationInfo in(boolean in) {
            return new SerializationInfo(_lf, in ? _indent + 1 : 0);
        }
    }
}
