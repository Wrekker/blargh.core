package engine.serialization;

import java.io.IOException;
import java.io.OutputStream;

public interface IResourceSerializer {
    void serialize(ISpecification obj, OutputStream os) throws IOException, IllegalAccessException;
}
