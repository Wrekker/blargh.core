package engine.serialization;

import engine.resources.IResource;
import sun.misc.Compare;
import sun.misc.Sort;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.lang.reflect.Array;
import java.lang.reflect.Field;
import java.util.Map;

public class BinaryResourceSerializer implements IResourceSerializer {
    private DataOutputStream _os;

    public BinaryResourceSerializer() { }

    @Override
    public void serialize(ISpecification obj, OutputStream os) throws IOException, IllegalAccessException {
        _os = new DataOutputStream(os);
        exp(obj, obj.getClass());
        _os = null;
    }

    private void exp(Object e, Class<?> type) throws IOException, IllegalAccessException {
        if (e ==  null) {
            nil(e, type);
            return;
        } else if (e.getClass() == Object.class) {
            _type(e.getClass());
        }

        if (e.getClass().isArray()) {
            array(e, type);
        } else if (e instanceof ISpecification) {
            struct(e, type);
        } else if (e instanceof Map) {
            map(e, type);
        } else if (e instanceof Enum<?>) {
            enumeration(e, type);
        } else {
            constant(e, type);
        }
    }

    private void array(Object e, Class<?> type) throws IOException, IllegalAccessException {
        int l = Array.getLength(e);

        _os.writeInt(l);

        Class<?> comType = e.getClass().getComponentType();
        for (int i = 0; i < l; i++) {
            exp(Array.get(e, i), comType);
        }
    }

    private void enumeration(Object e, Class<?> type) throws IOException {
        _os.writeByte(((Enum<?>) e).ordinal());
    }

    private void map(Object e, Class<?> type) throws IOException, IllegalAccessException {
        Map m = (Map) e;

        _os.writeInt(m.size());
        for (Object f : m.values()) {
            Map.Entry me = (Map.Entry)f;

            _field(me.getKey(), Object.class);
            exp(me.getValue(), Object.class);
        }
    }

    private void struct(Object e, Class<?> type) throws IOException, IllegalAccessException {
        Class<?> cls = e.getClass();
        Field[] fields = cls.getFields();

        Sort.quicksort(fields, new Compare() {
            @Override
            public int doCompare(Object o, Object o1) {
                return ((Field)o).getName().compareTo(((Field)o1).getName());
            }
        });

        int m = 0;
        for (Field f : fields) {
            m = (m << 1) | (f.get(e) != null ? 1 : 0);
        }

        _os.writeInt(m);
        for (Field f : fields) {
            exp(f.get(e), f.getType());
        }
    }

    private void nil(Object e, Class<?> type) throws IOException {
        _type(ExpressionType.Nil);
    }

    private void constant(Object e, Class<?> type) throws IOException {
        if (e == null) {
            return;
        }

        if (e instanceof Byte || e.getClass() == byte.class) {
            _os.writeByte((Integer)e);
        }  else if (e instanceof Float || e.getClass() == float.class) {
            _os.writeFloat((Float)e);
        } else if (e instanceof String) {
            _os.writeUTF((String)e);
        } else if (e instanceof IResource) {
            constant(((IResource) e).getKey(), type);
        }
    }

    private void _field(Object e, Class<?> type) throws IOException {
        if (e instanceof Float || e.getClass() == float.class) {
            _os.writeUTF(e.toString());
        } else if (e instanceof String) {
            _os.writeUTF((String)e);
        }
    }

    private void _type(Class<?> c) throws IOException {
        if (c.isArray()) {
            _type(ExpressionType.Array);
            _os.writeUTF(c.getName());
        } else if (ISpecification.class.isAssignableFrom(c)) {
            _type(ExpressionType.Structure);
            _os.writeUTF(c.getName());
        } else if (Map.class.isAssignableFrom(c)) {
            _type(ExpressionType.Map);
        } else if (c.isEnum()) {
            _type(ExpressionType.Enumeration);
            _os.writeUTF(c.getName());
        } else if (Byte.class.isAssignableFrom(c)) {
            _type(ExpressionType.Binary);
        } else if (Float.class.isAssignableFrom(c)) {
            _type(ExpressionType.Number);
        } else if (String.class.equals(c)) {
            _type(ExpressionType.String);
        } else if (IResource.class.isAssignableFrom(c)) {
            _type(ExpressionType.Resource);
            _os.writeUTF(c.getName());
        }
    }

    private void _type(ExpressionType e) throws IOException {
        _os.writeByte(e.ordinal());
    }

}
