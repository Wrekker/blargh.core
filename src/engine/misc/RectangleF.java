package engine.misc;

public class RectangleF extends Point2F {
    public final float w;
    public final float h;

    public RectangleF(float x, float y, float w, float h) {
        super(x, y);
        this.w = w;
        this.h = h;
    }

    public boolean isPointInside(float px, float py) {
        return (px >= x &&
                py >= y &&
                px < x + w &&
                py < y + h);
    }

    public RectangleF contract(RectangleF rect) {
        return new RectangleF(
                this.x + rect.x,
                this.y + rect.y,
                this.w - rect.w - rect.x,
                this.h - rect.h - rect.y);
    }

    public RectangleF expand(RectangleF rect) {
        return new RectangleF(
                this.x - rect.x,
                this.y - rect.y,
                this.w + rect.w + rect.x,
                this.h + rect.h + rect.y);
    }

    public RectangleF intersect(RectangleF rect) {
        return new RectangleF(
                Math.max(this.x, rect.x),
                Math.max(this.y, rect.y),
                Math.min(this.x + this.w, rect.x + rect.w),
                Math.min(this.y + this.h, rect.y + rect.h)
        );
    }

    public RectangleF contain(RectangleF rect) {
        return new RectangleF(
                Math.min(this.x, rect.x),
                Math.min(this.y, rect.y),
                Math.max(this.x + this.w, rect.x + rect.w),
                Math.max(this.y + this.h, rect.y + rect.h));
    }

    @Override
    public String toString() {
        return "RectangleF{" +
                "x=" + x +
                ", y=" + y +
                ", w=" + w +
                ", h=" + h +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        RectangleF that = (RectangleF) o;

        if (Float.compare(that.h, h) != 0) return false;
        if (Float.compare(that.w, w) != 0) return false;
        if (Float.compare(that.x, x) != 0) return false;
        if (Float.compare(that.y, y) != 0) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (x != +0.0f ? Float.floatToIntBits(x) : 0);
        result = 31 * result + (y != +0.0f ? Float.floatToIntBits(y) : 0);
        result = 31 * result + (w != +0.0f ? Float.floatToIntBits(w) : 0);
        result = 31 * result + (h != +0.0f ? Float.floatToIntBits(h) : 0);
        return result;
    }

    public boolean intersects(RectangleF rect) {
        return intersects(rect.x, rect.y, rect.w, rect.h);
    }

    public boolean intersects(float x, float y, float w, float h) {
        float b = y + h;
        float r = x + w;

        return !(this.x > r ||
                this.x + this.w < x ||
                this.y > b ||
                this.y + this.h < y);
    }
}
