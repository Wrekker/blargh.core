package engine.misc.switches;

import engine.misc.GameTime;

public class PeriodicSwitch {
    private final float _interval;
    private float _lastTrigger = -1f;
    private boolean _enabled;

    public PeriodicSwitch(float interval) {
        this(interval, false);
    }

    public PeriodicSwitch(float interval, boolean enabled) {
        _interval = interval;
        _enabled = enabled;
    }

    public void enable(boolean flag) {
        _enabled = flag;
    }

    public boolean trigger(GameTime gt) {
        return trigger(gt, 1f);
    }

    public boolean trigger(GameTime gt, float timescale) {
        float scaledInterval = _interval / timescale;

        if (_enabled && (_lastTrigger < 0 || gt.getTotalSeconds() - _lastTrigger >= scaledInterval)) {
            _lastTrigger = gt.getTotalSeconds();

            if (gt.getTotalSeconds() - _lastTrigger >= scaledInterval) {
                _lastTrigger -= gt.getTotalSeconds() - _lastTrigger - scaledInterval;
            }

            return true;
        }

        return false;
    }
}
