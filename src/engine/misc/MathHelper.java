package engine.misc;

public class MathHelper {
    private static final float TWO_PI = (float)(java.lang.Math.PI * 2);

    public static float normalizeAngle(float angle) {
        return ((angle %= TWO_PI) >= 0 ?
                (angle < java.lang.Math.PI) ?
                    angle :
                    angle - TWO_PI :
                (angle >= -java.lang.Math.PI) ?
                    angle :
                    angle + TWO_PI);
    }

    public static float asDegrees(float radians) {
        return (float) (radians * (180f / Math.PI));
    }

    public static float asRadians(float degrees) {
        return degrees * (float)(Math.PI / 180f);
    }

    public static float lerp(float a, float b, float weight) {
        return a + (b - a) * weight;
    }

}




