package engine.misc;

public class ColorF {
    public final float r;
    public final float g;
    public final float b;
    public final float a;

    public ColorF(float r, float g, float b) {
        this(r, g, b, 1);
    }

    public ColorF(float r, float g, float b, float a) {
        this.r = r;
        this.g = g;
        this.b = b;
        this.a = a;
    }

    public static ColorF lerp(ColorF a, ColorF b, float weight) {
        return new ColorF(
                MathHelper.lerp(a.r, b.r, weight),
                MathHelper.lerp(a.g, b.g, weight),
                MathHelper.lerp(a.b, b.b, weight),
                MathHelper.lerp(a.a, b.a, weight));
    }

    public ColorF mul(float c) {
        return new ColorF(
                this.r * c,
                this.g * c,
                this.b * c,
                this.a * c);
    }
}
