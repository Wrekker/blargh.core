package engine.misc;

public class Point2F {
    public final float x;
    public final float y;

    public Point2F(float x, float y) {
        this.x = x;
        this.y = y;
    }

    @Override
    public String toString() {
        return "Point2F{" +
                "x=" + x +
                ", y=" + y +
                '}';
    }

    public Point2F mul(float factor) {
        return new Point2F(
                x * factor,
                y * factor);
    }

    public Point2F add(float term) {
        return new Point2F(
                x + term,
                y + term);
    }

    public Point2F add(Point3F other) {
        return new Point2F(
                x + other.x,
                y + other.y);
    }
}
