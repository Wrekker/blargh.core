package engine.misc;

public class LinearGradient {
    private final ColorF[] _colors;

    public LinearGradient(ColorF[] colors) {
        _colors = colors;
    }

    public ColorF getColor(float weight) {
        float c = _colors.length - 2;

        int n = (int)(weight * c);
        if (n > c) n = (int)c;

        float w = (weight * c) - (float)n;

        return ColorF.lerp(_colors[n], _colors[n + 1], w);
    }
}
