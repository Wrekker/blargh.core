package engine.misc;

public class Size2F extends Point2F {
    public Size2F(float x, float y) {
        super(x, y);
    }

    public Size2F(Point2F p) {
        super(p.x, p.y);
    }
}
