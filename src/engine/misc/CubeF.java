package engine.misc;

public class CubeF extends RectangleF {
    public final float z;
    public final float d;

    public CubeF(float x, float y, float z, float w, float h, float d) {
        super(x, y, w, h);
        this.z = z;
        this.d = d;
    }
}
