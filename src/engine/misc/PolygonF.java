package engine.misc;

import java.util.Arrays;

public class PolygonF {
    public final Point2F[] points;

    public PolygonF(Point2F[] points) {
        this.points = points;
    }

    public boolean isPointInside(float x, float y) {
        boolean c = false;

        for (int i = 0, j = points.length-1; i < points.length; j = i++) {
            if ((points[i].y > y) != (points[j].y > y) &&
                (x < (points[j].x-points[i].x) * (y-points[i].y) / (points[j].y-points[i].y) + points[i].x))
                c = !c;
        }

        return c;
    }

    @Override
    public String toString() {
        return "PolygonF{" +
                "points=" + (points == null ? null : Arrays.asList(points)) +
                '}';
    }
}
