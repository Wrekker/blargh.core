package engine.physics;

import org.jbox2d.dynamics.FixtureDef;

public class MaterialDef {
    private final float _friction;
    private final float _density;
    private final float _restitution;

    public MaterialDef(float friction, float density, float restitution) {
        _friction = friction;
        _density = density;
        _restitution = restitution;
    }

    public void assignTo(FixtureDef def) {
        def.density = _density;
        def.friction = _friction;
        def.restitution = _restitution;
    }
}