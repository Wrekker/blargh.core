package engine.entities;

import engine.misc.GameTime;
import engine.rendering.IExplicitOrderView;

public interface IEntityManager {
    Entity createEntity(IEntityDef def);

    void update(GameTime time);
    void render(IExplicitOrderView view, GameTime time);
}
