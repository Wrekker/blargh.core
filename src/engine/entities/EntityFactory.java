package engine.entities;

import engine.audio.IAudioManager;
import engine.particles.IParticleManager;
import org.jbox2d.dynamics.World;

import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Map;

public class EntityFactory {
    private final Map<Type, IEntityFactory> _entityFactories = new HashMap<Type, IEntityFactory>();

    private World _world;
    private IEntityManager _manager;
    private IAudioManager _audioManager;
    private IParticleManager _particleManager;

    public EntityFactory() { }

    public void initialize(World world,
                           IEntityManager manager,
                           IAudioManager audioManager,
                           IParticleManager particleManager) {
        _world = world;
        _manager = manager;
        _audioManager = audioManager;
        _particleManager = particleManager;
    }

    public void registerEntityFactory(IEntityFactory factory) {
        _entityFactories.put(factory.getEntityType(), factory);
    }

    public Entity create(IEntityDef entityDef) {
        IEntityFactory fac = _entityFactories.get(entityDef.getEntityType());
        Entity ret = fac.create(entityDef, _world, _manager, _audioManager, _particleManager);

        return ret;
    }
}
