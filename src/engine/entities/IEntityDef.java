package engine.entities;

import java.lang.reflect.Type;

public interface IEntityDef {
    Type getEntityType();
}
