package engine.entities;

import engine.misc.GameTime;
import engine.rendering.IExplicitOrderView;

import java.util.Deque;
import java.util.HashSet;
import java.util.LinkedList;

public class EntityManager implements IEntityManager {
    private final HashSet<Entity> _entities = new HashSet<Entity>();
    private final EntityFactory _entityFactory;
    private final Deque<Entity> _pendingDestructions = new LinkedList<Entity>();
    private final Deque<Entity> _pendingConstructions = new LinkedList<Entity>();

    public EntityManager(EntityFactory entityFactory) {
        _entityFactory = entityFactory;
    }

    @Override
    public Entity createEntity(IEntityDef def) {
        Entity e = _entityFactory.create(def);
        _pendingConstructions.add(e);
        return e;
    }

    @Override
    public void update(GameTime time) {
        while (!_pendingConstructions.isEmpty()) {
            _entities.add(_pendingConstructions.remove());
        }

        for (Entity entity : _entities) {
            entity.synchronize(time);
            if (entity.isDestroyed()) {
                _pendingDestructions.add(entity);
            }
        }

        while (!_pendingDestructions.isEmpty()) {
            Entity e = _pendingDestructions.remove();
            e.dispose();
            _entities.remove(e);
        }
    }

    @Override
    public void render(IExplicitOrderView view, GameTime time) {
        for (Entity entity : _entities) {
            entity.render(view, time);
        }
    }

}

