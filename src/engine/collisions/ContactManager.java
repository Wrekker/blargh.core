package engine.collisions;

import java.lang.reflect.Type;
import java.util.Deque;
import java.util.HashMap;
import java.util.LinkedList;

@SuppressWarnings("unchecked")
public class ContactManager implements IContactManager {
    HashMap<Type, Deque<IContactSolver>> _solvers = new HashMap<Type, Deque<IContactSolver>>();
    int _changes = 0;

    @Override
    public void registerSolver(Class type, IContactSolver solver) {
        LinkedList<IContactSolver> solvers = new LinkedList<IContactSolver>();
        solvers.add(solver);

        _solvers.put(type, solvers);
        _changes = _solvers.size(); // All solver lists have to be rebuilt.
    }

    @Override
    public void begin(Object data) {
        if (data == null) {
            return;
        }

        Deque<IContactSolver> solvers = getSolvers(data.getClass());
        if (solvers == null) {
            return;
        }

        for (IContactSolver solver : solvers) {
            solver.beginContact(data);
        }
    }

    @Override
    public void end(Object data) {
        if (data == null) {
            return;
        }

        Deque<IContactSolver> solvers = getSolvers(data.getClass());
        if (solvers == null) {
            return;
        }

        for (IContactSolver solver : solvers) {
            solver.endContact(data);
        }
    }

    private Deque<IContactSolver> getSolvers(Class actualType) {
        Deque<IContactSolver> ret;
        if ((ret = _solvers.get(actualType)) != null) {
            // Exact match or supersolver
            return ret;
        }

        if (_changes == 0) {
            // No lists remain to rebuild and no match was found - do nothing.
            return null;
        }

        // Rebuild list of superclassed solvers
        Deque<IContactSolver> solvers = new LinkedList<IContactSolver>();
        Class superType = actualType.getSuperclass();
        while (superType != Object.class) {
            if ((ret = _solvers.get(superType)) != null) {
                solvers.addAll(ret);
            }
            superType = superType.getSuperclass();
        }

        _solvers.put(actualType, solvers);
        _changes--;

        return solvers;
    }
}
