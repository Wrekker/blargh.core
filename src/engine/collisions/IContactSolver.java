package engine.collisions;

public interface IContactSolver {
    void beginContact(Object data);
    void endContact(Object data);
}
