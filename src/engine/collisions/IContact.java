package engine.collisions;

public interface IContact<TBody, TFixture> {
    TBody getBodyData();
    TFixture getFixtureData();
}
