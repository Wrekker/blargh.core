package engine.collisions;

public interface ICollidable {
    IContactManager getContactManager();
}
