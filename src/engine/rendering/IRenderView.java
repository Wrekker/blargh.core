package engine.rendering;

public interface IRenderView extends IExplicitOrderView {
    void initializeView(int width, int height);

    int getWidth();
    int getHeight();

    void beginFrame(ICamera viewport);
    void flushBatch();
    void endFrame();

    void destroyView();
}
