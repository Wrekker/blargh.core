package engine.rendering;

import engine.misc.*;
import engine.resources.texture.SubTexture;
import engine.resources.texture.Texture;
import engine.resources.typeface.Typeface;

/**
 * Transforming view for physics-based systems (such as entities)
 */
public class PhysicsTransformingView implements IExplicitOrderView {
    IRenderView _superView;

    public PhysicsTransformingView(IRenderView view) {
        _superView = view;
    }

    @Override
    public ICamera getRenderingCamera() {
        return _superView.getRenderingCamera();
    }

    @Override
    public void drawSubTexture(float x, float y, float z, float width, float height, float angle, SubTexture tex) {
        RectangleF s = scale(x, y, z, width, height);
        _superView.drawSubTexture(s.x, s.y, z, s.w, s.h, angle, tex);
    }

    @Override
    public void drawSubTexture(float x, float y, float z, float width, float height, float angle, SubTexture tex, RenderingParameters params) {
        RectangleF s = scale(x, y, z, width, height);
        _superView.drawSubTexture(s.x, s.y, z, s.w, s.h, angle, tex, params);
    }

    @Override
    public void drawTexture(float x, float y, float z, float width, float height, float angle, Texture tex) {
        RectangleF s = scale(x, y, z, width, height);
        _superView.drawTexture(s.x, s.y, z, s.w, s.h, angle, tex);
    }

    @Override
    public void drawTexture(float x, float y, float z, float width, float height, float angle, Texture tex, RenderingParameters params) {
        RectangleF s = scale(x, y, z, width, height);
        _superView.drawTexture(s.x, s.y, z, s.w, s.h, angle, tex, params);
    }

    @Override
    public void drawText(float x, float y, float z, String text, Typeface face) {
        RectangleF s = scale(x, y, z, 0, 0);
        _superView.drawText(s.x, s.y, z, text, face);
    }

    @Override
    public void drawText(float x, float y, float z, String text, Typeface face, RenderingParameters params) {
        RectangleF s = scale(x, y, z, 0, 0);
        _superView.drawText(s.x, s.y, z, text, face, params);
    }

    @Override
    public void drawPolygon(float x, float y, float z, float angle, Point2F[] points, ColorF color) {
        RectangleF s = scale(x, y, z, 0, 0);
        _superView.drawPolygon(s.x, s.y, z, angle, scale(points), color);
    }

    @Override
    public void drawPoint(float x, float y, float z, ColorF color) {
        RectangleF s = scale(x, y, z, 0, 0);
        _superView.drawPoint(s.x, s.y, z, color);
    }

    private RectangleF scale(float x, float y, float z, float w, float h) {
        Point2F v = Convert.physicsToGraphics(x, y);
        w = Convert.physicsToGraphicsScalar(w);
        h = Convert.physicsToGraphicsScalar(h);

        Point2F camTr = _superView.getRenderingCamera().getTranslation();
        float cw = _superView.getWidth();
        float ch = _superView.getHeight();

        CubeF camCube = new CubeF(camTr.x, camTr.y, 0, cw, ch, 256);

        RectangleF px = Parallax.offset(camCube, v.x, v.y, z, w, h);

        return new RectangleF(px.x, px.y, px.w, px.h);
    }

    private Point2F[] scale(Point2F[] points) {
        Point2F[] ret = new Point2F[points.length];

        Point2F camTr = _superView.getRenderingCamera().getTranslation();
        float w = _superView.getWidth();
        float h = _superView.getHeight();

        CubeF camCube = new CubeF(camTr.x, camTr.y, 0, w, h, 256);

        for (int i = 0; i < ret.length; i++) {
            Point2F px = Parallax.offset(camCube, points[i].x, points[i].y, 0, 0, 0);
            ret[i] = Convert.physicsToGraphics(px.x, px.y);
        }
        return ret;
    }

}
