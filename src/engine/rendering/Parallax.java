package engine.rendering;

import engine.misc.CubeF;
import engine.misc.RectangleF;

public class Parallax {
    public static RectangleF offset(CubeF cam, float x, float y, float z, float w, float h) {
        float ratioX = cam.w / 2 / cam.d;
        float ratioY = cam.h / 2 / cam.d;

        float fx = (1 + (z / cam.d) * ratioX);
        float fy = (1 + (z / cam.d) * ratioY);

        return new RectangleF(
            (x - cam.x) * fx + cam.x,
            (y - cam.y) * fy + cam.y,
            w * fx,
            h * fy);
    }
}
