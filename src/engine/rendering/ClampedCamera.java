package engine.rendering;

import engine.misc.Convert;
import engine.misc.Point2F;
import engine.misc.Size2F;
import org.jbox2d.common.Vec2;

/**
 * Camera that clamps against the boundaries of the world.
 */
public class ClampedCamera implements ICamera {
    private final Vec2 _wz;
    private final Vec2 _cz;
    private Vec2 _targetPos;

    /**
     * @param worldWidth In meters.
     * @param worldHeight In meters.
     * @param camWidth In meters.
     * @param camHeight In meters.
     */
    public ClampedCamera(float worldWidth, float worldHeight, float camWidth, float camHeight) {
        _wz = new Vec2(worldWidth, worldHeight);
        _cz = new Vec2(camWidth, camHeight);
        _targetPos = new Vec2(0,0);
    }

    @Override
    public void centerOn(float x, float y) {
        x = Math.min(x, (_wz.x - _cz.x) / 2);
        y = Math.min(y, (_wz.y - _cz.y) / 2);
        x = Math.max(x, (-_wz.x + _cz.x) / 2);
        y = Math.max(y, (-_wz.y + _cz.y) / 2);

        _targetPos = new Vec2(x, y);
    }

    @Override
    public Point2F getTranslation() {
        return Convert.physicsToGraphics(_targetPos.x, _targetPos.y);
    }

    @Override
    public Size2F getDimensions() {
        return new Size2F(Convert.physicsToGraphics(_cz.x, _cz.y));
    }
}
