package engine.rendering.renderObjects;

import engine.misc.ColorF;
import engine.misc.MathHelper;
import engine.misc.Point2F;
import engine.misc.RectangleF;
import engine.rendering.IRenderView;

import static org.lwjgl.opengl.GL11.*;

public class PolygonRenderObject extends OpenGlRenderObject {
    public final float angle;
    public final Point2F[] points;
    public final ColorF color;

    public PolygonRenderObject(float x, float y, float z, float angle, Point2F[] points, ColorF color) {
        super(x, y, z);
        this.angle = angle;
        this.points = points;
        this.color = color;
    }

    @Override
    public void render(RectangleF culling, IRenderView view) {
        glDisable(GL_TEXTURE_2D);

        glPushMatrix(); {
            glTranslatef(x, y, 0.0f);
            glRotatef(MathHelper.asDegrees(MathHelper.normalizeAngle(angle)), 0.0f, 0.0f, 1.0f);

            glColor4f(color.r, color.g, color.b, color.a);
            glLineWidth(2f);

            glBegin(GL_LINE_LOOP); {
                for (Point2F point : points) {
                    glVertex2f(point.x, point.y);
                }
            } glEnd();
        } glPopMatrix();

        glEnable(GL_TEXTURE_2D);
    }
}
