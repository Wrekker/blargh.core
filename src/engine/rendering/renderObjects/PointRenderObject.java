package engine.rendering.renderObjects;

import engine.misc.ColorF;
import engine.misc.RectangleF;
import engine.rendering.IRenderView;

import static org.lwjgl.opengl.GL11.*;

public class PointRenderObject extends OpenGlRenderObject {
    public final ColorF color;

    public PointRenderObject(float x, float y, float z, ColorF color) {
        super(x, y, z);
        this.color = color;
    }

    @Override
    public void render(RectangleF culling, IRenderView view) {
        glDisable(GL_TEXTURE_2D);
        glColor4f(color.r, color.g, color.b, color.a);

        glPushMatrix(); {
            glPointSize(5f);
            glBegin(GL_POINTS);{
                glVertex2f(x, y);
            } glEnd();
        } glPopMatrix();

        glEnable(GL_TEXTURE_2D);
    }
}
