package engine.rendering.renderObjects;

import engine.misc.RectangleF;
import engine.misc.Size2F;
import engine.rendering.IRenderView;
import engine.rendering.RenderingParameters;
import engine.resources.typeface.Glyph;
import engine.resources.typeface.GlyphLine;
import engine.resources.typeface.GlyphParagraph;
import engine.resources.typeface.Typeface;

import static org.lwjgl.opengl.GL11.GL_TEXTURE_2D;
import static org.lwjgl.opengl.GL11.glBindTexture;

public class TextRenderObject extends OpenGlRenderObject {
    public final String text;
    public final Typeface typeface;
    public final RenderingParameters parameters;

    public TextRenderObject(float x,
                            float y,
                            float z,
                            String text,
                            Typeface typeface,
                            RenderingParameters parameters) {
        super(x, y, z);
        this.text = text;
        this.typeface = typeface;
        this.parameters = parameters;
    }

    @Override
    public void render(RectangleF culling, IRenderView view) {
        GlyphParagraph glyphs = typeface.getGlyphs(text.toCharArray());
        Size2F size = glyphs.size;

        if (!culling.intersects(x, y-size.y, size.x, size.y)) {
            return;
        }

        glBindTexture(GL_TEXTURE_2D, typeface.getTexture().getTxId());
        for (GlyphLine glyphLine : glyphs.lines) {
            RectangleF lineRect = glyphLine.bounds;
            for (Glyph glyph : glyphLine.glyphs) {
                RectangleF rt = glyph.textureCoords;

                drawQuad(x + lineRect.x + glyph.glyphCoords.x,
                        y + lineRect.y + glyph.glyphCoords.y,
                        0,
                        glyph.glyphCoords.w, glyph.glyphCoords.h,
                        rt.x, rt.y, rt.w, rt.h,
                        0, 0,
                        parameters);
            }
        }

        //drawDebugQuad(x, y-size.y, 0, size.x, size.y, 0, 0, parameters);
    }
}
