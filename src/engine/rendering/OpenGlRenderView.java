package engine.rendering;

import engine.misc.ColorF;
import engine.misc.Point2F;
import engine.misc.RectangleF;
import engine.misc.Size2F;
import engine.rendering.renderObjects.*;
import engine.resources.texture.SubTexture;
import engine.resources.texture.Texture;
import engine.resources.typeface.Typeface;
import org.lwjgl.LWJGLException;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.DisplayMode;

import java.util.ArrayList;
import java.util.Map;
import java.util.TreeMap;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.util.glu.GLU.gluOrtho2D;

/**
 * An OpenGL implementation of IRenderView, using immediate mode drawing.
 */
public class OpenGlRenderView implements IRenderView {
    private final RenderingParameters _defaultParameters = new RenderingParameters();
    private final TreeMap<Float, ArrayList<OpenGlRenderObject>> _renderObjects;
    private final int _frameRate;

    private ICamera _renderingCamera;
    private int _width;
    private int _height;

    public OpenGlRenderView(int frameRate) {
        _frameRate = frameRate;
        _renderObjects = new TreeMap<Float, ArrayList<OpenGlRenderObject>>();
    }

    @Override
    public int getWidth() { return _width; }
    @Override
    public int getHeight() { return _height; }

    @Override
    public void initializeView(int width, int height) {
        _width = width;
        _height = height;

        initDisplay(width, height);
        initGl(width, height);
    }

    @Override
    public void flushBatch() {
        Point2F pos = _renderingCamera.getTranslation();
        Size2F dim = _renderingCamera.getDimensions();

//        float radius = Math.max(dim.x, dim.y) / 2f;
//        radius = (float)Math.sqrt(radius * radius);
//
        int sx = (int)((_width - dim.x) / 2f);
        int sy = (int)((_height - dim.y) / 2f);

        RectangleF cull = new RectangleF(pos.x - dim.x / 2f, pos.y - dim.y / 2f, dim.x, dim.y);

        //GL11.glScissor(sx, sy, (int) dim.x, (int) dim.y);

        for (ArrayList<OpenGlRenderObject> object : _renderObjects.values()) {
            for (OpenGlRenderObject renderObject : object) {
                renderObject.render(cull, this);
            }
        }

        _renderObjects.clear();
    }

    @Override
    public void beginFrame(ICamera viewport) {
        _renderingCamera = viewport;

        glClear(GL_COLOR_BUFFER_BIT);
        glPushMatrix();

        Point2F t = viewport.getTranslation();
        glTranslatef(-t.x, -t.y, 0);
    }

    @Override
    public void endFrame() {
        flushBatch();

        Display.update();
        //Display.sync(_frameRate);

        glPopMatrix();

        _renderingCamera = null;
    }

    @Override
    public void destroyView() {
        Display.destroy();
    }

    @Override
    public ICamera getRenderingCamera() {
        return _renderingCamera;
    }

    @Override
    public void drawSubTexture(float x, float y, float z, float width, float height, float angle, SubTexture tex){
        drawSubTexture(x, y, z, width, height, angle, tex, _defaultParameters);
    }

    @Override
    public void drawSubTexture(float x, float y, float z, float width, float height, float angle, SubTexture tex, RenderingParameters params) {
        insertRenderObject(new SubTextureRenderObject(x, y, z, width, height, angle, tex, params));
    }

    @Override
    public void drawTexture(float x, float y, float z, float width, float height, float angle, Texture tex){
        drawTexture(x, y, z, width, height, angle, tex, _defaultParameters);
    }

    @Override
    public void drawTexture(float x, float y, float z, float width, float height, float angle, Texture tex, RenderingParameters params) {
        insertRenderObject(new TextureRenderObject(x, y, z, width, height, angle, tex, params));
    }

    @Override
    public void drawText(float x, float y, float z, String text, Typeface face) {
        drawText(x, y, z, text, face, _defaultParameters);
    }

    @Override
    public void drawText(float x, float y, float z, String text, Typeface face, RenderingParameters params) {
        insertRenderObject(new TextRenderObject(x, y, z, text, face, params));
    }

    @Override
    public void drawPolygon(float x, float y, float z, float angle, Point2F[] points, ColorF color) {
        insertRenderObject(new PolygonRenderObject(x, y, z, angle, points, color));
    }

    @Override
    public void drawPoint(float x, float y, float z, ColorF color){
        insertRenderObject(new PointRenderObject(x, y, z, color));
    }

    private void insertRenderObject(OpenGlRenderObject o) {
        Map.Entry<Float, ArrayList<OpenGlRenderObject>> ro = _renderObjects.floorEntry(o.z);

        ArrayList<OpenGlRenderObject> l;
        if (ro == null || !ro.getKey().equals(o.z)) {
            l = new ArrayList<OpenGlRenderObject>();
            _renderObjects.put(o.z, l);
        } else {
            l = ro.getValue();
        }

        l.add(o);
    }

    private void initDisplay(int width, int height) {
        if (Display.isCreated()) {
            Display.destroy();
        }

        try {
            Display.setDisplayMode(new DisplayMode(width, height));
            Display.create();
            //Display.setVSyncEnabled(true);
        } catch (LWJGLException ex) { }
    }

    private void initGl(int width, int height) {
        glViewport(0, 0, width, height);
        glMatrixMode(GL_PROJECTION);
        glLoadIdentity();
        gluOrtho2D(0, width, 0, height);
        glMatrixMode(GL_MODELVIEW);
        glLoadIdentity();
        // Translate origin to center of view (to match physics)
        glTranslatef((width / 2f) + 0.375f, (height / 2f) + 0.375f, 0f);
        glScaled(1, 1, 1);

        glClearColor(0, 0.2f, 0.2f, 1);
        glDisable(GL_DEPTH_TEST);
        glEnable(GL_TEXTURE_2D);
        glDisable(GL_STENCIL);
        glEnable(GL_SCISSOR_TEST);

        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
        glEnable(GL_BLEND);
    }



}
