package engine.rendering;

import engine.misc.Point2F;
import engine.misc.Size2F;

/**
 * Interface for view cameras.
 */
public interface ICamera {
    void centerOn(float x, float y);
    Point2F getTranslation();
    Size2F getDimensions();
}
