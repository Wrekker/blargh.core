package engine.rendering;

import engine.misc.Size2F;
import org.jbox2d.common.MathUtils;

public class BoundingBox {
    public static Size2F forSize2F(Size2F f, float angle) {
        float c = Math.abs(MathUtils.cos(angle));
        float s = Math.abs(MathUtils.sin(angle));

        float bx = c * f.x + s * f.y;
        float by = s * f.x + c * f.y;

        return new Size2F(bx, by);
    }
}
