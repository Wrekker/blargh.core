package engine.events;

public class PeriodicEvent<T extends EventArgs> extends Event<T> {
    private long _periodMs;
    private long _lastActivation = 0;

    public PeriodicEvent(long periodMs) {
        _periodMs = periodMs;
    }

    @Override
    public void raiseEvent(T arg) {
        long ms = getTime();

        if (ms - _lastActivation >= _periodMs) {
            if (_lastActivation == 0) {
                _lastActivation = ms;
            } else {
                _lastActivation += _periodMs;
            }
            super.raiseEvent(arg);
        }
    }

    private long getTime() {
        return (long) (System.nanoTime() / 1E6);
    }
}
