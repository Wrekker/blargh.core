package engine.events;

public class ForwardingSubscriber<T extends EventArgs> implements ISubscriber<T> {
    private Event<T> _evt;

    public ForwardingSubscriber(Event<T> target) {
        _evt = target;
    }

    @Override
    public void handleEvent(T arg) {
        _evt.raiseEvent(arg);
    }
}
