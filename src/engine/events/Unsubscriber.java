package engine.events;

public class Unsubscriber<T extends EventArgs> {
    private final Event<T> _event;
    private final ISubscriber<T> _subscriber;

    public Unsubscriber(Event<T> event, ISubscriber<T> subscriber) {
        _event = event;
        _subscriber = subscriber;
    }

    public ISubscriber<T> getSubscriber() {
        return _subscriber;
    }

    public void Unsubscribe() {
        _event.removeSubscriber(_subscriber);
    }
}
