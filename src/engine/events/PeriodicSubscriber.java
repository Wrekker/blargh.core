package engine.events;

public abstract class PeriodicSubscriber<T extends EventArgs> implements ISubscriber<T> {
    private long _periodMs;
    private long _lastActivation = 0;

    public PeriodicSubscriber(long periodMs) {
        _periodMs = periodMs;
    }

    @Override
    public void handleEvent(T arg) {
        long ms = getTime();

        if (ms - _lastActivation >= _periodMs) {
            if (_lastActivation == 0) {
                _lastActivation = ms;
            } else {
                _lastActivation += _periodMs;
            }
            handlePeriodicEvent(arg);
        }
    }

    private long getTime() {
        return (long) (System.nanoTime() / 1E6);
    }

    public abstract void handlePeriodicEvent(T arg);
}
