package engine.input;

import engine.misc.GameTime;

public interface IInputManager {
    void push(IInputMap map);
    IInputMap pop();
    void update(GameTime gt);
}
