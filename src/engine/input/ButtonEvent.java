package engine.input;

public class ButtonEvent {
    private final int _button;
    private final boolean _isKeyDown;
    private final int _scroll;
    private final int _x;
    private final int _y;

    public ButtonEvent(int button, boolean keyDown, int scroll, int x, int y) {
        _button = button;
        _isKeyDown = keyDown;
        _scroll = scroll;
        _x = x;
        _y = y;
    }

    public boolean isButtonDown() { return _isKeyDown; }
    public int getButton() { return _button; }
    public int getScroll() { return _scroll; }
    public int getX() { return _x; }
    public int getY() { return _y; }
}
