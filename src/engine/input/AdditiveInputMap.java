package engine.input;

/**
 * Input map that adds but does not overwrite input bindings.
 */
public class AdditiveInputMap extends InputMapBase {
    public AdditiveInputMap() { }

    @Override
    public boolean onButton(ButtonEvent ev) {
        IButtonInteraction i;

        if (_next != null && _next.hasButton(ev.getButton())) {
            return _next.onButton(ev);
        } else {
            if ((i = _buttons.get(ev.getButton())) != null) {
                return i.onButton(ev);
            }
        }

        return false;
    }

    @Override
    public boolean onKey(KeyEvent ev) {
        IKeyInteraction k;

        if (_next != null && _next.hasKey(ev.getKey())) {
            return _next.onKey(ev);
        } else {
            if ((k = _keys.get(ev.getKey())) != null) {
                return k.onKey(ev);
            }
        }

        return false;
    }

    @Override
    public boolean onMouse(MouseEvent ev) {
        if (_next != null && _next.hasMouse()) {
            return _next.onMouse(ev);
        } else {
            if (_mouse != null) {
                return _mouse.onMouse(ev);
            }
        }

        return false;
    }

}
