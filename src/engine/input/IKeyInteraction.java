package engine.input;

public interface IKeyInteraction {
    boolean onKey(KeyEvent ev);
}
