package engine.input;

import engine.misc.GameTime;
import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;

import java.util.Stack;

/**
 * Input manager that allows for multiple input maps simultaneously, by
 * placing them in a stack and activating them top-down.
 */
public class LayeredInputManager implements IInputManager {
    private Stack<InputMapBase> _maps = new Stack<InputMapBase>();

    public LayeredInputManager() { }

    public void push(IInputMap map) {
        if (!_maps.isEmpty()) {
            ((InputMapBase)map).setNext(_maps.peek());
        }
        _maps.push((InputMapBase)map);
    }

    public IInputMap pop() {
        return _maps.pop();
    }

    @Override
    public void update(GameTime gt) {
        if (_maps.isEmpty()) {
            return;
        }

        IInputMap top = _maps.peek();
        while (Keyboard.next()) {
            top.onKey(new KeyEvent(Keyboard.getEventKey(), Keyboard.getEventKeyState()));
        }

        boolean hasMouseMoved = false;
        while (Mouse.next()) {
            if (Mouse.getEventButton() != -1) {
                top.onButton(new ButtonEvent(Mouse.getEventButton(), Mouse.getEventButtonState(), 0, Mouse.getX(), Mouse.getY()));
            }

            if (Mouse.getEventDWheel() != 0) {
                top.onButton(new ButtonEvent(0, false, Mouse.getEventDWheel(), Mouse.getX(), Mouse.getY()));
            }

            top.onMouse(new MouseEvent(
                    Mouse.getX(),
                    Mouse.getY(),
                    Mouse.getDX(),
                    Mouse.getDY()));

            hasMouseMoved = true;
        }

        if (!hasMouseMoved) {
            top.onMouse(new MouseEvent(
                    Mouse.getX(),
                    Mouse.getY(),
                    0,
                    0));
        }
    }
}
