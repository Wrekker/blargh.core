package engine.input;

public interface IButtonInteraction {
    boolean onButton(ButtonEvent ev);
}

