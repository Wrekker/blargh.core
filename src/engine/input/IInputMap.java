package engine.input;

public interface IInputMap extends IButtonInteraction, IKeyInteraction, IMouseInteraction {
    void addKey(int key, IKeyInteraction interaction);
    void addButton(int button, IButtonInteraction interaction);
    void addMouse(IMouseInteraction interaction);

    boolean hasKey(int key);
    boolean hasButton(int button);
    boolean hasMouse();
}
