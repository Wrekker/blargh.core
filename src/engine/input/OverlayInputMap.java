package engine.input;

/**
 * Input map that passes on the activation if not handled.
 */
public class OverlayInputMap extends InputMapBase {
    public OverlayInputMap() { }

    @Override
    public boolean onButton(ButtonEvent ev) {
        boolean handled = false;

        IButtonInteraction i;
        if ((i = _buttons.get(ev.getButton())) != null) {
            handled = i.onButton(ev);
        }

        if (!handled && _next != null) {
            handled  = _next.onButton(ev);
        }

        return handled;
    }

    @Override
    public boolean onKey(KeyEvent ev) {
        boolean handled = false;

        IKeyInteraction k;
        if ((k = _keys.get(ev.getKey())) != null) {
            handled = k.onKey(ev);
        }

        if (!handled && _next != null) {
            handled = _next.onKey(ev);
        }

        return handled;
    }

    @Override
    public boolean onMouse(MouseEvent ev) {
        boolean handled = false;

        if (_mouse != null) {
            handled = _mouse.onMouse(ev);
        }

        if (!handled && _next != null) {
            handled = _next.onMouse(ev);
        }
        return handled;
    }
}
