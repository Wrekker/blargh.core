package engine.input;

import java.util.HashMap;
import java.util.Map;

/**
 * Abstract class for implementing stacking input maps.
 */
public abstract class InputMapBase implements IInputMap {
    Map<Integer, IKeyInteraction> _keys = new HashMap<Integer, IKeyInteraction>();
    Map<Integer, IButtonInteraction> _buttons = new HashMap<Integer, IButtonInteraction>();
    IMouseInteraction _mouse = null;

    IInputMap _next;

    protected InputMapBase() { }

    public void setNext(IInputMap map) {
        _next = map;
    }

    @Override
    public void addKey(int key, IKeyInteraction interaction) { _keys.put(key, interaction); }

    @Override
    public void addButton(int button, IButtonInteraction interaction) { _buttons.put(button, interaction); }

    @Override
    public void addMouse(IMouseInteraction interaction) { _mouse = interaction; }

    @Override
    public boolean hasKey(int key) { return _keys.containsKey(key); }

    @Override
    public boolean hasButton(int button) { return _buttons.containsKey(button); }

    @Override
    public boolean hasMouse() { return _mouse != null; }

    @Override
    public abstract boolean onButton(ButtonEvent ev);

    @Override
    public abstract boolean onKey(KeyEvent ev);

    @Override
    public abstract boolean onMouse(MouseEvent ev);
}
