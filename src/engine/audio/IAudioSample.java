package engine.audio;

public interface IAudioSample {
    int getSampleId();
}
