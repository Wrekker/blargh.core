package engine.audio;

import engine.audio.channels.ParametricAudioChannel;
import engine.misc.GameTime;

public interface IAudioManager {
    void update(GameTime time);

    ParametricAudioChannel createManualChannel();
    ParametricAudioChannel createAutoChannel();
}
