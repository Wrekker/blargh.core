package engine.audio;

import engine.misc.GameTime;
import engine.misc.Point2F;

public interface IAudioListener {
    void update(GameTime time);
}
