package engine.audio.channels;

import engine.audio.IAudioSample;
import engine.misc.GameTime;

public interface IAudioChannel {
    IAudioSample getSample();

    void setOverlay(IAudioChannel channel);

    void play(IAudioSample sample);
    void restart();
    void stop();
    boolean isPlaying();
    void dispose();

    void update(GameTime time);

    // "Fluent" methods
    IAudioChannel asLooping(boolean b);
    IAudioChannel atGain(float f);
}
