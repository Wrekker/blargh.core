package engine.audio.channels;

import engine.audio.IAudioSample;
import engine.entities.Entity;
import engine.misc.GameTime;
import engine.misc.Point2F;

import static org.lwjgl.openal.AL10.*;

public class ParametricAudioChannel implements IAudioChannel {
    protected IAudioSample _sample;
    protected int _sid;
    private IAudioChannel _overlay;

    public ParametricAudioChannel() {
        _sid = alGenSources();

        alSourcei(_sid, AL_SOURCE_ABSOLUTE, 1);
        alSourcei(_sid, AL_DISTANCE_MODEL, AL_INVERSE_DISTANCE_CLAMPED);
        alSourcei(_sid, AL_REFERENCE_DISTANCE, 10);
    }

    @Override
    public IAudioSample getSample() {
        return _sample;
    }

    @Override
    public void setOverlay(IAudioChannel channel) {
        _overlay = channel;
    }

    @Override
    public void play(IAudioSample sample) {
        if (sample == _sample) {
            restart();
            return;
        }

        alSourcei(_sid, AL_BUFFER, sample.getSampleId());
        alSourcePlay(_sid);

        _sample = sample;
    }

    @Override
    public void stop() {
        alSourceStop(_sid);
    }

    @Override
    public void restart() {
        //alSourceRewind(_sid);
        alSourcePlay(_sid);
    }

    @Override
    public void dispose() {
        _sample = null;
        alDeleteSources(_sid);
    }

    @Override
    public void update(GameTime time) {
        if (_overlay != null)
            _overlay.update(time);
    }

    @Override
    public boolean isPlaying() {
        return alGetSourcei(_sid, AL_PLAYING) == 1;
    }

    @Override
    public IAudioChannel asLooping(boolean b) {
        alSourcei(_sid, AL_LOOPING, b ? 1 : 0);
        return this;
    }

    @Override
    public IAudioChannel atGain(float f) {
        alSourcef(_sid, AL_GAIN, f);
        return this;
    }

    public EntityAudioChannel atEntity(Entity e) {
        return new EntityAudioChannel(e, this);
    }


    public StaticAudioChannel atLocation(Point2F location) {
        return new StaticAudioChannel(location, this);
    }


}
