package engine.audio.channels;

import engine.audio.IAudioSample;
import engine.misc.GameTime;
import engine.misc.Point2F;

import static org.lwjgl.openal.AL10.*;

public class StaticAudioChannel implements IAudioChannel {
    private final ParametricAudioChannel _inner;

    public StaticAudioChannel(Point2F location, ParametricAudioChannel inner) {
        _inner = inner;
        _inner.setOverlay(this);

        alSource3f(_inner._sid, AL_VELOCITY, 0, 0, 0);
        alSource3f(_inner._sid, AL_POSITION, location.x, location.y, 0);
    }

    public void update(GameTime time) {

    }

    public boolean isPlaying() {
        return _inner.isPlaying();
    }

    public IAudioSample getSample() {
        return _inner.getSample();
    }

    @Override
    public void setOverlay(IAudioChannel channel) {
        _inner.setOverlay(channel);
    }

    public void play(IAudioSample sample) {
        _inner.play(sample);
    }

    public void restart() {
        _inner.restart();
    }

    public void stop() {
        _inner.stop();
    }

    public void dispose() {
        _inner.dispose();
    }

    public IAudioChannel asLooping(boolean b) {
        return _inner.asLooping(b);
    }

    public IAudioChannel atGain(float f) {
        return _inner.atGain(f);
    }
}
