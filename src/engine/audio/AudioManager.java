package engine.audio;

import engine.audio.channels.IAudioChannel;
import engine.audio.channels.ParametricAudioChannel;
import engine.misc.GameTime;
import org.lwjgl.LWJGLException;
import org.lwjgl.openal.AL;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;


public class AudioManager implements IAudioManager {
    LinkedList<IAudioChannel> _autos;
    LinkedList<IAudioChannel> _channels;

    Map<String, IAudioChannel> _namedChannels;
    private IAudioListener _listener;

    public AudioManager() {
        _autos = new LinkedList<IAudioChannel>();
        _channels = new LinkedList<IAudioChannel>();
        _namedChannels = new HashMap<String, IAudioChannel>();

        try {
            AL.create();
        } catch (LWJGLException e) {
            e.printStackTrace();
        }
    }

    public void setListener(IAudioListener listener) {
        if (_listener == null)
            _listener = listener;
    }

    @Override
    public void update(GameTime time) {
        _listener.update(time);

        for (Iterator<IAudioChannel> iterator = _autos.iterator(); iterator.hasNext(); ) {
            IAudioChannel po = iterator.next();

            if (!po.isPlaying()) {
                iterator.remove();
                continue;
            }

            po.update(time);
        }

        for (Iterator<IAudioChannel> iterator = _channels.iterator(); iterator.hasNext(); ) {
            IAudioChannel po = iterator.next();

            po.update(time);
        }
    }

    @Override
    public ParametricAudioChannel createManualChannel() {
        ParametricAudioChannel ret = new ParametricAudioChannel();
        _channels.add(ret);
        return ret;
    }

    @Override
    public ParametricAudioChannel createAutoChannel() {
        ParametricAudioChannel ret = new ParametricAudioChannel();
        _autos.add(ret);
        return ret;
    }

}
