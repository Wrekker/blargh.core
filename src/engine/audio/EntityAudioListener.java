package engine.audio;

import engine.entities.Entity;
import engine.misc.GameTime;
import engine.misc.Vector;
import org.jbox2d.common.Vec2;
import org.lwjgl.BufferUtils;

import java.nio.FloatBuffer;

import static org.lwjgl.openal.AL10.*;


public class EntityAudioListener implements IAudioListener {
    private final Entity _entity;


    public EntityAudioListener(Entity entity) {
        _entity = entity;
    }

    @Override
    public void update(GameTime time) {
        Vec2 p = _entity.getBody().getPosition();
        Vec2 v = _entity.getBody().getLinearVelocity();
        Vec2 a = Vector.angleToVector(_entity.getAngle());

        alListener3f(AL_POSITION, p.x, p.y, 0);
        alListener3f(AL_VELOCITY, v.x, v.y, 0);

        FloatBuffer _ori = BufferUtils.createFloatBuffer(6);
        _ori.put(new float[]{ a.x, a.y, 0f, 0f, 0f, 1f });
        _ori.rewind();
        alListener(AL_ORIENTATION, _ori);
    }
}

