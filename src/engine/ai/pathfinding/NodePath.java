package engine.ai.pathfinding;

import java.util.Deque;
import java.util.LinkedList;

public class NodePath {

    private Deque<PathNode> _path;
    private float totalLength = 0f;

    public NodePath() {
        _path = new LinkedList<PathNode>();
    }

    public void increaseLength(float length){
        totalLength += length;
    }

    public float getTotalLength() {
        return totalLength;
    }

    public Deque<PathNode> getPath() {
        return _path;
    }

    public void addToPath(PathNode node){
        this._path.addLast(node);
    }

    public void addAllToPath(Deque<PathNode> nodes){
        this._path.addAll(nodes);
    }

    public void removeFromPath(){
        this._path.remove();
    }
}
