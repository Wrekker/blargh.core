package engine.ai.pathfinding;

import engine.misc.Reference;
import org.jbox2d.callbacks.RayCastCallback;
import org.jbox2d.common.Vec2;
import org.jbox2d.dynamics.Body;
import org.jbox2d.dynamics.Fixture;
import org.jbox2d.dynamics.World;
public class RayCaster {
    private final World _b2dWorld;

    public RayCaster(World b2dWorld) {
        _b2dWorld = b2dWorld;
    }

    //Returns closest fixture so that we can walk along it.
    public TargetFixture castRay(Vec2 source, Vec2 target) {

        final Vec2 s = source;
        final Vec2 t = target;
        final Reference<Vec2> nearestCollision = new Reference<Vec2>(null);
        final Reference<Fixture> nearestFixture = new Reference<Fixture>(null);

        RayCastCallback rayCastCallback = new RayCastCallback() {
            @Override
            public float reportFixture(Fixture fixture, Vec2 vec2, Vec2 vec21, float v) {
                nearestCollision.value = vec2.clone();
                nearestFixture.value = fixture;
                return v;
            }
        };

        _b2dWorld.raycast(rayCastCallback, s, t);

        return new TargetFixture(nearestFixture.value, nearestCollision.value);

    }

    //Checks if it is a visible point we are casting to.
    public boolean VisiblePoint(Vec2 enemy, Vec2 point) {

        final Reference<Boolean> visible = new Reference<Boolean>(true);
        RayCastCallback rayCastCallback = new RayCastCallback() {
            @Override
            public float reportFixture(Fixture fixture, Vec2 vec2, Vec2 vec21, float v) {
                //must set userdata to true in order to see the object if ray hits it.
                //if (fixture.getUserData() != null && (Boolean) fixture.getUserData()) {
                    visible.value = true;
                    return v;
                //}

//                visible[0] = false;
//                return 0;
            }
        };

        _b2dWorld.raycast(rayCastCallback, enemy, point);
        return visible.value;
    }

    //Checks if it is a visible entity we are casting to.
    public boolean isEntityVisible(Vec2 point, Body target) {
        final Reference<Boolean> visible = new Reference<Boolean>(true);
        final Body tempTarget = target;
        RayCastCallback rayCastCallback = new RayCastCallback() {
            @Override
            public float reportFixture(Fixture fixture, Vec2 vec2, Vec2 vec21, float v) {
                if (fixture.getBody() == tempTarget) {
                    visible.value = true;
                    return v;
                }

                visible.value = false;
                return 0;
            }
        };

        _b2dWorld.raycast(rayCastCallback, point, target.getPosition());
        return visible.value;
    }
}

