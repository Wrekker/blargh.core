package engine.ai.pathfinding;

import org.jbox2d.collision.shapes.CircleShape;
import org.jbox2d.collision.shapes.ShapeType;
import org.jbox2d.dynamics.Body;

import java.util.Deque;
import java.util.LinkedList;

public class EntityMoveQueue {
    private final RayCaster _caster;
    private final EntityNodeGraph _grapher;

    public EntityMoveQueue(RayCaster caster, EntityNodeGraph grapher) {
        _caster = caster;
        _grapher = grapher;
    }

    //Produces a move queue to the target out of moveQueue list, returns moveQueue in any case.
    public Deque<PathNode> getNodes(Deque<PathNode> moveQueue, Body source, Body target) {

        NodePath nodePath1 = new NodePath();  //Create a pair of NodePaths, cause we only have two ways to test.
        NodePath nodePath2 = new NodePath();

        float length1;   //Length of the path.
        float length2;

        if (_caster.isEntityVisible(source.getPosition(), target)) {   // What if we see the target from start? Add target as next PathNode.
            moveQueue.add(new PathNode(target.getPosition()));
            return moveQueue;
        } else {
            TargetFixture targetFixture = _caster.castRay(source.getPosition(), target.getPosition()); //Gets the fixture that is in the way.
            LinkedList<PathNode> nodeList = _grapher.getNodes(targetFixture, getSourceOffset(source));   //Gets the nodes of the fixture with an offset (size of source).

            PathNode[] circularNodePath = new PathNode[nodeList.size()];  //Create circular list for path.
            PathNode closestToCollision = _grapher.getClosestNodeToPoint(nodeList, targetFixture.collision); //Closest node to collision-point of ray.

            nodePath1.addToPath(closestToCollision);    //Add the collision node to both.
            nodePath2.addToPath(closestToCollision);

            int startIndex = 0;
            for (int i = 0; i < nodeList.size(); i++){          //Add all elements and try to
                circularNodePath[i] = nodeList.get(i);
                if (circularNodePath[i].equals(closestToCollision)){
                    startIndex = i;
                }
            }

            length1 = checkCCWPath(nodePath1, circularNodePath, target, source, startIndex);    //Gets length of paths
            length2 = checkCWPath(nodePath2, circularNodePath, target, source, startIndex);
        }

        if (length1 > length2){
            moveQueue.addAll(nodePath2.getPath());
        } else {
            moveQueue.addAll(nodePath1.getPath());
        }

        return moveQueue;
    }

    public float checkCWPath(NodePath nodePath, PathNode[] circularNodePath, Body target, Body source, int startIndex){
        PathNode tempNode;

        int i = startIndex;
        int s = i;

        if (i == 0) { i = circularNodePath.length - 1; }
        while (i > 0 && (i - 1) != s) {
            i = (i - 1);

            nodePath.addToPath(circularNodePath[i]);
            nodePath.increaseLength(circularNodePath[i].nodePos.sub(source.getPosition()).lengthSquared());
            tempNode = circularNodePath[i];

            if (_caster.isEntityVisible(tempNode.nodePos, target)) {   // What if we see the target from start? Add target as next PathNode.
                nodePath.addToPath(new PathNode(target.getPosition()));
                nodePath.increaseLength(target.getPosition().sub(source.getPosition()).lengthSquared());
                return nodePath.getTotalLength();
            }


            if (i == 0) { i = circularNodePath.length; }

        }
        return 0;
    }

    public float checkCCWPath(NodePath nodePath, PathNode[] circularNodePath, Body target, Body source, int startIndex) {
        PathNode tempNode;

        int i = startIndex;
        int s = i;

        while ((i + 1) % circularNodePath.length != s) {
            i = (i + 1) % circularNodePath.length;


            nodePath.addToPath(circularNodePath[i]);
            nodePath.increaseLength(circularNodePath[i].nodePos.sub(source.getPosition()).lengthSquared());
            tempNode = circularNodePath[i];

            if (_caster.isEntityVisible(tempNode.nodePos, target)) {   // What if we see the target from start? Add target as next PathNode.
                nodePath.addToPath(new PathNode(target.getPosition()));
                nodePath.increaseLength(target.getPosition().sub(source.getPosition()).lengthSquared());
                return nodePath.getTotalLength();
            }
        }

        return 0;
    }

    public static float getSourceOffset(Body source) {
        if (source.getFixtureList().getShape().getType() == ShapeType.CIRCLE) {    //What offset do we want for our source?
            CircleShape bodyRadius = (CircleShape) source.getFixtureList().getShape();
            return bodyRadius.m_radius * 1.3f;
        } else if (source.getFixtureList().getShape().getType() == ShapeType.POLYGON) {
            return 4;
            //PolygonShape bodySize = (PolygonShape) source.getFixtureList().getShape();
            //return bodySize.getVertex(0).sub(source.getFixtureList().getAABB().getCenter()).length();
        }
        return 0f;
    }
}
