package engine.ai.pathfinding;

import org.jbox2d.common.Vec2;

public class PathNode {
    public final Vec2 nodePos;
    public final float length;

    public PathNode(Vec2 nodePos) {
        this.nodePos = nodePos;
        this.length =  nodePos.lengthSquared();
    }
}
