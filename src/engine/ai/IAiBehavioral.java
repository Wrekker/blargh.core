package engine.ai;

import engine.ai.behavior.IAiBehaviourManager;

public interface IAiBehavioral {
    IAiBehaviourManager getBehaviourManager();
}
