package engine.ai.actions;

import engine.ai.behavior.IAiBehavior;

public abstract class AiBehavioralAction implements IAiAction {
    private final IAiBehavior _source;

    public AiBehavioralAction(IAiBehavior source) {
        _source = source;
    }

    public IAiBehavior getSource() { return _source; }
}
