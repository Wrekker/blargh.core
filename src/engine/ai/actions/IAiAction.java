package engine.ai.actions;

import engine.misc.GameTime;

public interface IAiAction {
    boolean isValid(GameTime gt);
    boolean perform(GameTime gt);
}
