package engine.ai.behavior;

import engine.misc.GameTime;

public interface IAiBehaviourManager {
    //Deque<IAiAction> getActionQueue();
    boolean performNext(GameTime gt);
    boolean think(GameTime gt);
}
