package engine.ai.behavior;

import engine.ai.actions.IAiAction;
import engine.misc.GameTime;

import java.util.Deque;
import java.util.LinkedList;

public class AiBehaviorTree implements IAiBehaviourManager {
    private final AiBehaviourNode _root;
    private Deque<IAiAction> _queue;

    public AiBehaviorTree(AiBehaviourNode root) {
        _queue = new LinkedList<IAiAction>();
        _root = root;
    }

    @Override
    public boolean performNext(GameTime gt) {
        while (!_queue.isEmpty()) {
            if (!_queue.peek().isValid(gt)) {
                _queue.remove();
                continue;
            }

            boolean linger = _queue.peek().perform(gt);
            if (!linger) {
                _queue.remove();
            }
            return true;
        }

        return false;
    }

    @Override
    public boolean think(GameTime gt) {
        return _root.think(gt, _queue);
    }
}
