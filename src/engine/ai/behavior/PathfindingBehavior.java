package engine.ai.behavior;

import engine.ai.actions.IAiAction;
import engine.ai.pathfinding.EntityMoveQueue;
import engine.ai.pathfinding.EntityNodeGraph;
import engine.ai.pathfinding.PathNode;
import engine.ai.pathfinding.RayCaster;
import engine.entities.Entity;
import engine.misc.GameTime;
import model.ai.MoveToBehavioralAction;

import java.util.Deque;
import java.util.LinkedList;

public class PathfindingBehavior implements IAiBehavior {
    private final Entity _self;
    private final Entity _target;
    private EntityMoveQueue _queuer;

    public PathfindingBehavior(Entity self, Entity target) {
        _self = self;
        _target = target;
    }

    @Override
    public boolean think(GameTime gt, Deque<IAiAction> actionQueue) {
        if (_queuer == null) {
            RayCaster caster = new RayCaster(_self.getBody().getWorld());
            _queuer = new EntityMoveQueue(caster, new EntityNodeGraph(caster));
        }

        Deque<PathNode> nodes = new LinkedList<PathNode>();

        _queuer.getNodes(nodes, _self.getBody(), _target.getBody());
        for (PathNode node : nodes) {
            actionQueue.add(new MoveToBehavioralAction(_self, node));
        }

        return false;
    }
}
