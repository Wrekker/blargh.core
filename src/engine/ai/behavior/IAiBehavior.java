package engine.ai.behavior;

import engine.ai.actions.IAiAction;
import engine.misc.GameTime;

import java.util.Deque;

public interface IAiBehavior {
    boolean think(GameTime gt, Deque<IAiAction> actionQueue);
}
