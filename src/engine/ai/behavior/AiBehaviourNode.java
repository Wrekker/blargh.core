package engine.ai.behavior;

import engine.ai.actions.IAiAction;
import engine.misc.GameTime;

import java.util.Deque;
import java.util.LinkedList;

public class AiBehaviourNode implements IAiBehavior {
    Deque<AiBehaviourNode> _nodes;
    private final IAiBehavior _behavior;

    public AiBehaviourNode(IAiBehavior behavior) {
        _behavior = behavior;
        _nodes = new LinkedList<AiBehaviourNode>();
    }

    public Deque<AiBehaviourNode> getNodes() {
        return _nodes;
    }

    @Override
    public boolean think(GameTime gt, Deque<IAiAction> actionQueue) {
        if (!_behavior.think(gt, actionQueue)) {
            for (AiBehaviourNode n : _nodes) {
                if (n.think(gt, actionQueue)) {
                    return true;
                }
            }
        }

        return false;
    }
}
