package engine.resources;

import java.io.IOException;

public interface IResourceLoader<T extends IResource> {
    Class<T> getResourceType();
    T load(String path) throws IOException;
}


