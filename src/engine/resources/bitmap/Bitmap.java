package engine.resources.bitmap;

import engine.resources.IResourceLoader;
import engine.resources.ManagedResource;

import java.awt.image.BufferedImage;

public class Bitmap extends ManagedResource {
    private final String _path;
    private BufferedImage _buffer;

    public Bitmap(String path, BufferedImage buffer) {
        super(path);
        _path = path;
        _buffer = buffer;
    }

    public BufferedImage getBuffer() { return _buffer; }

    @Override
    public void dispose(IResourceLoader<?> loader) { _buffer = null; }
}
