package engine.resources.bitmap;

import engine.resources.IDataLoader;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class BmpImageLoader implements IDataLoader<BufferedImage> {
    @Override
    public Class<BufferedImage> getDataType() {
        return BufferedImage.class;
    }

    @Override
    public BufferedImage load(String path) throws IOException {
        return ImageIO.read(new File(path));
    }
}
