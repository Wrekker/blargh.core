package engine.resources.audio;

import engine.resources.CachedResourceLoaderBase;
import org.lwjgl.util.WaveData;

import java.io.InputStream;

import static org.lwjgl.openal.AL10.*;

public class AudioLoader extends CachedResourceLoaderBase<AudioSample> {
    public AudioLoader() {
        super("audio", "nosound.wav");
    }

    @Override
    protected AudioSample onCacheMiss(String path, String qualifiedPath, InputStream is) {
        WaveData wave = WaveData.create(is);
        int format = wave.format;
        int samplerate = wave.samplerate;

        int soundId = alGenBuffers();
        alBufferData(soundId, format, wave.data, samplerate);

        wave.dispose();

        return new AudioSample(soundId, path, samplerate, format);
    }

    @Override
    public Class<AudioSample> getResourceType() { return AudioSample.class; }
}