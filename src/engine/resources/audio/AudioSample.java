package engine.resources.audio;

import engine.audio.IAudioSample;
import engine.resources.IResourceLoader;
import engine.resources.ManagedResource;

import static org.lwjgl.openal.AL10.alDeleteBuffers;

public class AudioSample extends ManagedResource implements IAudioSample {
    private final int soundId;
    private final String path;
    private final int sampleRate;
    private final int format;

    public AudioSample(int soundId, String path, int sampleRate, int format) {
        super(path);
        this.soundId = soundId;
        this.path = path;
        this.sampleRate = sampleRate;
        this.format = format;
    }

    public int getSampleId() { return soundId; }
    public int getSampleRate() { return sampleRate; }
    public int getFormat() { return format; }

    @Override
    public String getKey() { return path; }

    @Override
    public void dispose(IResourceLoader<?> loader) {
        alDeleteBuffers(soundId);
    }
}
