package engine.resources;

import engine.animation.AnimationSet;
import engine.animation.IAnimation;
import engine.audio.IAudioSample;
import engine.resources.animation.AnimationSetLoader;
import engine.resources.animation.TextureSheetAnimationLoader;
import engine.resources.audio.AudioLoader;
import engine.resources.bitmap.Bitmap;
import engine.resources.bitmap.BitmapLoader;
import engine.resources.gamemap.GameMapLoader;
import engine.resources.texture.Texture;
import engine.resources.texture.TextureLoader;
import engine.resources.texturesheet.TextureSheet;
import engine.resources.texturesheet.TextureSheetLoader;
import engine.resources.typeface.Typeface;
import engine.resources.typeface.TypefaceLoader;
import engine.world.GameMap;

@Deprecated
public class Loaders {
    private static Loaders _instance;

    public static Loaders getInstance() { return _instance; }
    public static void initialize(BitmapLoader bitmapLoader,
                                  TextureLoader textureLoader,
                                  TypefaceLoader typefaceLoader,
                                  TextureSheetLoader textureSheetLoader,
                                  TextureSheetAnimationLoader textureSheetAnimationLoader,
                                  AnimationSetLoader animationSetLoader,
                                  GameMapLoader mapLoader,
                                  AudioLoader audioLoader) {

        if (_instance != null) {
            return;
        }

        _instance = new Loaders(
                bitmapLoader,
                textureLoader,
                typefaceLoader,
                textureSheetLoader,
                textureSheetAnimationLoader,
                animationSetLoader,
                mapLoader,
                audioLoader);
    }

    private final TextureLoader _textureLoader;
    private final BitmapLoader _bitmapLoader;
    private final TypefaceLoader _typefaceLoader;
    private final TextureSheetLoader _textureSheetLoader;
    private final TextureSheetAnimationLoader _textureSheetAnimationLoader;
    private final AnimationSetLoader _animationSetLoader;
    private final GameMapLoader _mapLoader;
    private final AudioLoader _audio;

    public Loaders(BitmapLoader bitmapLoader,
                   TextureLoader textureManager,
                   TypefaceLoader typefaceLoader,
                   TextureSheetLoader textureSheetLoader,
                   TextureSheetAnimationLoader textureSheetAnimationLoader,
                   AnimationSetLoader animationSetLoader,
                   GameMapLoader mapLoader,
                   AudioLoader audioLoader) {

        _bitmapLoader = bitmapLoader;
        _textureLoader = textureManager;
        _typefaceLoader = typefaceLoader;
        _textureSheetLoader = textureSheetLoader;
        _textureSheetAnimationLoader = textureSheetAnimationLoader;
        _animationSetLoader = animationSetLoader;
        _mapLoader = mapLoader;
        _audio = audioLoader;
    }

    public Bitmap bitmap(String name) {
        return _bitmapLoader.load(name);
    }

    public Texture texture(String name) {
        return _textureLoader.load(name);
    }

    public Typeface typeface(String name) {
        return _typefaceLoader.load(name);
    }

    public TextureSheet sprite(String name) {
        return _textureSheetLoader.load(name);
    }

    public IAnimation animation(String name) {
        return _textureSheetAnimationLoader.load(name);
    }

    public AnimationSet animationSet(String name) {
        return _animationSetLoader.load(name);
    }

    public GameMap map(String name) {
        return _mapLoader.load(name);
    }

    public IAudioSample audio(String name) {
        return _audio.load(name);
    }
}
