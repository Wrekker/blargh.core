package engine.resources;

import engine.serialization.ISpecification;

public interface IResourceMapper<TTarget, TSource extends ISpecification> {
    Class<TSource> getSourceType();
    Class<TTarget> getTargetType();

    TTarget map(String path, String qualifiedPath, TSource source);
}


