package engine.resources.typeface;

import engine.resources.IResourceMapper;
import engine.resources.typeface.specification.GlyphSpecification;
import engine.resources.typeface.specification.KerningSpecification;
import engine.resources.typeface.specification.TypefaceSpecification;

import java.util.ArrayList;

public class TypefaceMapper implements IResourceMapper<Typeface, TypefaceSpecification> {
    @Override
    public Class<TypefaceSpecification> getSourceType() {
        return TypefaceSpecification.class;
    }

    @Override
    public Class<Typeface> getTargetType() {
        return Typeface.class;
    }

    @Override
    public Typeface map(String path, String qualifiedPath, TypefaceSpecification def) {
        ArrayList<GlyphInfo> gs = new ArrayList<GlyphInfo>();
        for (GlyphSpecification g : def.glyphs) {
            gs.add(new GlyphInfo((char)g.charCode, g.sheetX, g.sheetY, g.width, g.height, (int)g.xOffset, (int)g.yOffset, (int)g.xAdvance));
        }

        ArrayList<KerningInfo> ks = new ArrayList<KerningInfo>();
        for (KerningSpecification k : def.kernings) {
            ks.add(new KerningInfo((char)k.first, (char)k.second, (int)k.xOffset));
        }

        return new Typeface(path,
                def.texture,
                (int)def.lineHeight,
                gs,
                ks);
    }
}
