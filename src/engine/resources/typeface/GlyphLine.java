package engine.resources.typeface;


import engine.misc.RectangleF;

public class GlyphLine {
    public final RectangleF bounds;
    public final Glyph[] glyphs;

    public GlyphLine(RectangleF rect, Glyph[] glyphs) {
        bounds = rect;
        this.glyphs = glyphs;
    }
}
