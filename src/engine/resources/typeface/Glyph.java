package engine.resources.typeface;

import engine.misc.RectangleF;

public class Glyph {
    public final char glyph;
    public final RectangleF glyphCoords;
    public final RectangleF textureCoords;

    public Glyph(char glyph, RectangleF glyphCoords, RectangleF textureCoords) {
        this.glyph = glyph;
        this.glyphCoords = glyphCoords;
        this.textureCoords = textureCoords;
    }
}
