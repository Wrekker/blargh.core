package engine.resources.typeface.specification;

import engine.serialization.ISpecification;

public class KerningSpecification implements ISpecification {
    public float first;
    public float second;
    public float xOffset;
}
