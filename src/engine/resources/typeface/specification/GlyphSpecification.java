package engine.resources.typeface.specification;

import engine.serialization.ISpecification;

public class GlyphSpecification implements ISpecification {
    public float sheetX;
    public float sheetY;
    public float width;
    public float height;
    public float charCode;
    public float xOffset;
    public float yOffset;
    public float xAdvance;
}
