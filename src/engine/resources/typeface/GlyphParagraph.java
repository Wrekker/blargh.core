package engine.resources.typeface;

import engine.misc.Size2F;

public class GlyphParagraph {
    public final Size2F size;
    public final GlyphLine[] lines;

    public GlyphParagraph(Size2F size, GlyphLine[] lines) {
        this.size = size;
        this.lines = lines;
    }
}
