package engine.resources.texture;

public class SubTexture {
    private final Texture _texture;
    private final float _x;
    private final float _y;
    private final float _width;
    private final float _height;

    public SubTexture(Texture texture, int x, int y, int width, int height) {
        _texture = texture;
        _x = x / (float)texture.getWidth();
        _y = y / (float)texture.getHeight();
        _width = width / (float)texture.getWidth();
        _height = height / (float)texture.getHeight();
    }

    public Texture getSuperTexture() { return _texture; }
    public float getY() { return _y; }
    public float getX() { return _x; }
    public float getWidth() { return _width; }
    public float getHeight() { return _height; }
}
