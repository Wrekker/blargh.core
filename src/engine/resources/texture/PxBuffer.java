package engine.resources.texture;

import java.nio.ByteBuffer;

public class PxBuffer {
    private final int _width;
    private final int _height;
    private final int _depth;
    private final ByteBuffer _data;

    public PxBuffer(int width, int height, int depth, ByteBuffer data) {
        _width = width;
        _height = height;
        _depth = depth;
        _data = data;
    }

    public int getWidth() { return _width; }
    public int getHeight() { return _height; }
    public int getDepth() { return _depth; }
    public ByteBuffer getData() { return _data; }
}
