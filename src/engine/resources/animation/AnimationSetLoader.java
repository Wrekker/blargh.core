package engine.resources.animation;

import engine.animation.AnimationSet;
import engine.animation.IAnimation;
import engine.resources.CachedResourceLoaderBase;
import engine.resources.IResourceLoader;

import java.io.IOException;
import java.io.InputStream;
import java.util.Scanner;

public class AnimationSetLoader extends CachedResourceLoaderBase<AnimationSet> {
    private final IResourceLoader<IAnimation> _animLoader;

    public AnimationSetLoader(IResourceLoader<IAnimation> animLoader) {
        super("animations", "noanimset.animationset");
        _animLoader = animLoader;
    }

    @Override
    protected AnimationSet onCacheMiss(String path, String qualifiedPath, InputStream is) {
        Scanner sc = new Scanner(is);

        int count = sc.nextInt();
        String[] names = new String[count];
        IAnimation[] anims = new IAnimation[count];

        for (int i = 0; i < count; i++) {
            String animName = sc.next();
            String spriteAnimName = sc.next();

            names[i] = animName;
            try { anims[i] = _animLoader.load(spriteAnimName); }
            catch (IOException e) { }
        }

        return new AnimationSet(path, names, anims);
    }

    @Override
    public Class<AnimationSet> getResourceType() { return AnimationSet.class; }
}
