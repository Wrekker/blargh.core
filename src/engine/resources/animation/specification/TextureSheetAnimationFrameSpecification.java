package engine.resources.animation.specification;

import engine.serialization.ISpecification;

public class TextureSheetAnimationFrameSpecification implements ISpecification {
    public float frame;
    public float duration;
    public String trigger;
}
