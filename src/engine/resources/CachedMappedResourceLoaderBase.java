package engine.resources;

import engine.serialization.IResourceDeserializer;
import engine.serialization.ISpecification;

import java.io.*;
import java.util.Map;
import java.util.WeakHashMap;

public abstract class CachedMappedResourceLoaderBase<TTarget extends IResource, TSource extends ISpecification> implements IResourceLoader<TTarget> {
    private final Map<String, TTarget> _cache = new WeakHashMap<String, TTarget>();

    private final String _basePath;
    private final String _defaultResourcePath;
    private final IResourceMapper<TTarget, TSource> _mapper;
    private final IResourceDeserializer _deserializer;

    protected CachedMappedResourceLoaderBase(String basePath, String defaultResourcePath, IResourceDeserializer deserializer, IResourceMapper<TTarget, TSource> mapper) {
        _deserializer = deserializer;

        _basePath = basePath;
        _defaultResourcePath = defaultResourcePath;
        _mapper = mapper;
    }

    public String getBasePath() {
        return "assets/" + _basePath;
    }

    public TTarget load(String path) {
        String fullPath = getBasePath() + "/" + path;

        TTarget ret;
        if ((ret = _cache.get(fullPath)) == null) {
            InputStream is = open(fullPath);
            if (is == null) {
                return load(_defaultResourcePath);
            }

            TSource luaFile = null;
            try {
                luaFile = (TSource) _deserializer.deserialize(is, _mapper.getSourceType());
            } catch (Exception e) {
                try { is.close(); }
                catch (IOException e1) { return load(_defaultResourcePath); }
            }

            ret = _mapper.map(path, fullPath, luaFile);

            try { is.close(); }
            catch (IOException e) { e.printStackTrace(); }

            if (ret == null) { return load(_defaultResourcePath); }

            _cache.put(fullPath, ret);
        }

        return ret;
    }

    protected InputStream open(String path) {
        try {
            return new BufferedInputStream(new FileInputStream(path));
        } catch (FileNotFoundException e) {
            return null;
        }
    }
}
