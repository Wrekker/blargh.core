package engine.resources;

import java.io.*;
import java.util.Map;
import java.util.WeakHashMap;

/**
 * Abstract base class for resource loaders, that caches the loaded resource according to
 * filepath.
 * @param <T>
 */
public abstract class CachedResourceLoaderBase<T extends IResource> implements IResourceLoader<T> {
    private final Map<String, T> _cache = new WeakHashMap<String, T>();
    private final String _basePath;
    private final String _defaultResourcePath;

    protected CachedResourceLoaderBase(String basePath, String defaultResourcePath) {
        _basePath = basePath;
        _defaultResourcePath = defaultResourcePath;
    }

    public String getBasePath() {
        return "assets/" + _basePath;
    }

    public T load(String path) {
        String fullPath = getBasePath() + "/" + path;

        T ret;
        if ((ret = _cache.get(fullPath)) == null) {
            InputStream is = open(fullPath);
            if (is == null) { return load(_defaultResourcePath); }

            ret = onCacheMiss(path, fullPath, is);
            try { is.close(); }
            catch (IOException e) { e.printStackTrace(); }

            if (ret == null) { return load(_defaultResourcePath); }

            _cache.put(fullPath, ret);
        }

        return ret;
    }

    protected abstract T onCacheMiss(String path, String qualifiedPath, InputStream is);

    protected InputStream open(String path) {
        try {
            return new BufferedInputStream(new FileInputStream(path));
        } catch (FileNotFoundException e) {
            return null;
        }
    }

}
