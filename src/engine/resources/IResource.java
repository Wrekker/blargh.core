package engine.resources;

public interface IResource {
    String getKey();
    void dispose(IResourceLoader<?> loader);
}
