package engine.resources.texturesheet.specification;

import engine.serialization.ISpecification;

public class TextureStripSpecification implements ISpecification {
    public String name;
    public float frameCount;
}
