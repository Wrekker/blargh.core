package engine.resources.texturesheet.specification;

import engine.resources.texture.Texture;
import engine.serialization.ISpecification;

public class TextureSheetSpecification implements ISpecification {
    public Texture texture;
    public float width;
    public float height;
    public TextureStripSpecification[] strips;
}
