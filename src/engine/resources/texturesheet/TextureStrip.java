package engine.resources.texturesheet;

public class TextureStrip {
    private final int _index;
    private final String _stripName;
    private final int _frameCount;

    public TextureStrip(int index, String stripName, int frameCount) {
        _index = index;
        _stripName = stripName;
        _frameCount = frameCount;
    }

    public int getIndex() { return _index; }
    public String getStripName() { return _stripName; }
    public int getFrameCount() { return _frameCount; }
}
