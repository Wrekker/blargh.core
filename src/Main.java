import engine.audio.AudioManager;
import engine.audio.EntityAudioListener;
import engine.audio.IAudioManager;
import engine.collisions.B2dContactListener;
import engine.entities.EntityFactory;
import engine.entities.EntityManager;
import engine.entities.IEntityManager;
import engine.input.*;
import engine.management.Audio;
import engine.misc.*;
import engine.particles.IParticleEmitter;
import engine.particles.IParticleManager;
import engine.particles.ParticleManager;
import engine.rendering.*;
import engine.resources.Loaders;
import engine.resources.animation.AnimationSetLoader;
import engine.resources.animation.TextureSheetAnimationLoader;
import engine.resources.audio.AudioLoader;
import engine.resources.bitmap.BitmapLoader;
import engine.resources.bitmap.PngImageLoader;
import engine.resources.gamemap.GameMapLoader;
import engine.resources.texture.TextureLoader;
import engine.resources.texturesheet.TextureSheetLoader;
import engine.resources.typeface.Typeface;
import engine.resources.typeface.TypefaceLoader;
import engine.serialization.LuaResourceDeserializer;
import engine.ui.*;
import engine.ui.layouts.*;
import engine.world.GameMap;
import engine.world.World;
import model.HealthBarUiElement;
import model.ScoreBar;
import model.definitions.entities.*;
import model.definitions.particles.FireParticleDef;
import model.definitions.particles.SmokeParticleDef;
import model.entities.PlayerEntity;
import model.factories.*;
import model.menu.Bubble;
import model.menu.Menu;
import org.jbox2d.common.Vec2;
import org.lwjgl.opengl.Display;

public class Main {

    private static final int framerate = 60;

    public static void main(String[] args) throws InterruptedException {
        BitmapLoader bitmapLoader = new BitmapLoader();
        bitmapLoader.registerFormatLoader("png", new PngImageLoader());
        TextureLoader textureLoader = new TextureLoader(bitmapLoader);

        LuaResourceDeserializer deserializer = new LuaResourceDeserializer();
        deserializer.registerPrimitive(textureLoader);

        TypefaceLoader typefaceLoader = new TypefaceLoader(deserializer);
        TextureSheetLoader textureSheetLoader = new TextureSheetLoader(deserializer);
        deserializer.registerPrimitive(textureSheetLoader);

        TextureSheetAnimationLoader textureSheetAnimationLoader = new TextureSheetAnimationLoader(deserializer);

        AnimationSetLoader animationSetLoader = new AnimationSetLoader(textureSheetAnimationLoader);
        GameMapLoader mapLoader = new GameMapLoader(textureLoader);
        AudioLoader audioLoader = new AudioLoader();

        Loaders.initialize(
                bitmapLoader,
                textureLoader,
                typefaceLoader,
                textureSheetLoader,
                textureSheetAnimationLoader,
                animationSetLoader,
                mapLoader,
                audioLoader);

        float scale = 32; // pixels per meter
        final float camWidth = 28; // meters
        final float camHeight = 21; // meters

        Convert.initialize(scale, camWidth, camHeight);

        final IRenderView canvas = new OpenGlRenderView(framerate);
        canvas.initializeView(
                (int) Convert.physicsToGraphicsScalar(camWidth),
                (int) Convert.physicsToGraphicsScalar(camHeight));

        final LayeredInputManager inputs = new LayeredInputManager();
        final UiManager uiManager = new UiManager(
                (int) Convert.physicsToScreenScalar(camWidth),
                (int) Convert.physicsToScreenScalar(camHeight));

        Menu m = new Menu(inputs);
        final int[] menuState = new int[1];
        m.getNewGameButton().setClickAction(new IClickUiAction() {
            @Override
            public Bubble onClick(int button, Point2F point) {
                menuState[0] = 1;
                return Bubble.FALSE;
            }
        });

        m.getExitGameButton().setClickAction(new IClickUiAction() {
            @Override
            public Bubble onClick(int button, Point2F point) {
                menuState[0] = 2;
                return Bubble.FALSE;
            }
        });

        ICamera uiCamera = new ClampedCamera(camWidth, camHeight, camWidth, camHeight);

//        GameTime time = GameTime.get();
//        loop: while (!Display.isCloseRequested()) {
//            switch (menuState[0]) {
//                case 1:
                    startGame(camWidth, camHeight, canvas, inputs, uiManager);
//                case 2:
//                    break loop;
//            }
//            menuState[0] = 0;
//
//
//            time = GameTime.next(time);
//
//            inputs.update(time);
//            canvas.beginFrame(uiCamera); {
//                uiManager.render(new UiTransformingView(canvas), time);
//
//                canvas.swapFrame();
//            } canvas.endFrame();
//        }

        // cleanup
        org.lwjgl.openal.AL.destroy();
        canvas.destroyView();
    }

    private static void startGame(float camWidth, float camHeight, IRenderView canvas, LayeredInputManager inputs, UiManager uiManager) {
        GameMap map =  Loaders.getInstance().map("gamemap1.map");
        World world = new World(map, new B2dContactListener());

        float worldWidth = map.getPhysicalWorldSize().x;
        float worldHeight = map.getPhysicalWorldSize().y;

        PlayerEntity player;
        ICamera playerCam;
        IEntityManager entityManager;
        IParticleManager particleManager = new ParticleManager();
        IAudioManager audioManager = new AudioManager();

        // Set up the entity factories.
        // A small hack is needed here to let zombies home in on the player.
        EntityFactory entFac = new EntityFactory();
        {
            entFac.registerEntityFactory(new PlayerEntityFactory());
            entFac.registerEntityFactory(new BoxEntityFactory());
            entFac.registerEntityFactory(new ShotEntityFactory());
            entFac.registerEntityFactory(new StaticEntityFactory());
            entFac.registerEntityFactory(new SpawnerEntityFactory());

            entityManager = new EntityManager(entFac);
            entFac.initialize(world.getBox2dWorld(), entityManager, audioManager, particleManager);

            playerCam = new ClampedCamera(worldWidth, worldHeight, camWidth, camHeight);
            player = makePlayer(playerCam, entityManager);

            // bit of a hack
            entFac.registerEntityFactory(new ZombieEntityFactory(player));
        }

        ((AudioManager)audioManager).setListener(new EntityAudioListener(player));

        placeMapEntities(worldWidth, worldHeight, entityManager);
        makeHud(player, uiManager, camWidth, camHeight);

        inputs.push(bindPlayerControls(player));
        inputs.push(bindUi(uiManager));

        // main loop
        GameTime time = GameTime.start(framerate);

        Audio.initialize(audioManager);

        IParticleEmitter fire = particleManager.createEmitter(new FireParticleDef(new Point2F(0.1f, 0.2f)));
        IParticleEmitter smoke = particleManager.createEmitter(new SmokeParticleDef(new Point2F(0.1f, 0.2f)));

        Audio.play(Loaders.getInstance().audio("effects/fire1.wav"))
                .atLocation(new Point2F(5, 5f))
                .asLooping(true);

        Typeface debugFace = Loaders.getInstance().typeface("trebuchet16.typeface");

        long frame = 0;
        long ns = 0;
        float timing = 0;

        while (!Display.isCloseRequested()) {
            time = time.next();

            world.update(time);
            inputs.update(time);
            entityManager.update(time);

            Audio.update(time);

            uiManager.update(time);

            canvas.beginFrame(playerCam); {
                ns = System.nanoTime();

                world.render(new PhysicsTransformingView(canvas), time);
                canvas.flushBatch();


                if (Rnd.chance(2)) {
                    //for (int i = 0; i < 50; i++) {
                        fire.emit(5, 5);
                        smoke.emit(5, 5);
                    //}
                }

                particleManager.render(new PhysicsTransformingView(canvas), time);
                entityManager.render(new PhysicsTransformingView(canvas), time);
                canvas.flushBatch();

                uiManager.render(new UiTransformingView(canvas), time);

                RenderingParameters rp = new RenderingParameters();
                rp.color = new ColorF(1f, 0, 0);

                if (frame % framerate == 0) {
                    timing = (System.nanoTime() - ns) / (float)1e6;
                }

                canvas.drawText(0, 0, 1000,
                        (1000 / timing) + "\n" +
                                time.getFrameTime() + "\n " +
                                time.getIsRunningSlowly(),
                        debugFace,
                        rp);

                frame++;
            } canvas.endFrame();
        }
    }

    /**
     * Binds the hooks needed for the UI.
     *
     * @param uiManager
     * @return
     */
    private static OverlayInputMap bindUi(UiManager uiManager) {
        OverlayInputMap oim = new OverlayInputMap();
        oim.addButton(Buttons.BUTTON_PRIMARY, uiManager.getClickInteraction());
        oim.addMouse(uiManager.getHoverInteraction());
        return oim;
    }

    /**
     * Binds all the keys needed to control the player character.
     *
     * @param player
     * @return
     */
    private static BlockingInputMap bindPlayerControls(PlayerEntity player) {
        BlockingInputMap bim = new BlockingInputMap();
        bim.addKey(Keys.KEY_UP, player.forwardInteraction());
        bim.addKey(Keys.KEY_LEFT, player.leftInteraction());
        bim.addKey(Keys.KEY_RIGHT, player.rightInteraction());
        bim.addKey(Keys.KEY_DOWN, player.backInteraction());

        // for demo purposes
//        bim.addKey(Keys.KEY_1, player.pickWeaponInteraction(new Machinegun()));
//        bim.addKey(Keys.KEY_2, player.pickWeaponInteraction(new Shotgun()));
//        bim.addKey(Keys.KEY_3, player.pickWeaponInteraction(new VulcanCannon()));

        bim.addButton(Buttons.BUTTON_PRIMARY, player.shootInteraction());
        bim.addButton(Buttons.BUTTON_SECONDARY, player.gripInteraction());
        bim.addMouse(player.aimInteraction());
        return bim;


    }


    /**
     * Creates and adds the health and score bar to the UI.
     *
     * @param player
     * @param uiManager
     */
    private static void makeHud(PlayerEntity player, UiManager uiManager, float camWidth, float camHeight) {
        uiManager.setLayout(new AnchorLayout());

        HealthBarUiElement healthBar = new HealthBarUiElement(player);
        healthBar.setPreferredSize(new Size2F(Convert.physicsToGraphicsScalar(camWidth), Convert.physicsToGraphicsScalar(camHeight)));
        healthBar.setLayoutProperty(AbsoluteLayout.X, 0f);
        healthBar.setLayoutProperty(AbsoluteLayout.Y, 0f);
        GridLayoutPriority priority = new GridLayoutPriority();
        priority.setFirstPriority(GridPriorityDirections.RIGHT);
        priority.setSecondaryPriority(GridPriorityDirections.UP);
        healthBar.setLayout(new GridLayout(2,3, priority));

        healthBar.setClickAction(new IClickUiAction() {
            @Override
            public Bubble onClick(int button, Point2F point) {
                System.out.println("Yes, you're clicking me.");
                return Bubble.TRUE;
            }
        });

        ScoreBar scoreBar = new ScoreBar(player);
        scoreBar.setPreferredSize(new Size2F(100, 24));
        scoreBar.setPadding(new RectangleF(0, 0, 0, 0));
        scoreBar.setLayoutProperty(AbsoluteLayout.X, 0f);
        scoreBar.setLayoutProperty(
                AbsoluteLayout.Y,
                Convert.physicsToGraphicsScalar(camHeight) - scoreBar.getPreferredSize().y
        );


        scoreBar.setClickAction(new IClickUiAction() {
            @Override
            public Bubble onClick(int button, Point2F point) {
                System.out.println("Yes, you're clicking me too.");
                return Bubble.TRUE;
            }
        });

        uiManager.getChildren().add(healthBar);
        uiManager.getChildren().add(scoreBar);
    }

    /**
     * Creates the player entity.
     *
     * @param playerCam
     * @param entities
     * @return
     */
    private static PlayerEntity makePlayer(ICamera playerCam, IEntityManager entities) {
        PlayerEntity player;
        {
            PlayerEntityDef def = new PlayerEntityDef();
            def.position = new Vec2(0, 0);
            def.angle = 0;
            def.animationSet = "player.animationset";

            player = (PlayerEntity)entities.createEntity(def);
            player.setCamera(playerCam);
        }
        return player;
    }

    /**
     * Stuff temporarily created here. (Should be part of the map.)
     *
     * @param worldWidth
     * @param worldHeight
     * @param entities
     */
    private static void placeMapEntities(float worldWidth, float worldHeight, IEntityManager entities) {
        {
            BoxEntityDef def = new BoxEntityDef();
            def.position = new Vec2(-1, 2);
            def.angle = 0;
            def.size = 1;

            entities.createEntity(def);
        }

        {
            BoxEntityDef def = new BoxEntityDef();
            def.position = new Vec2(-3, -3);
            def.angle = 0;
            def.size = 2;

            entities.createEntity(def);
        }

        {
            BoxEntityDef def = new BoxEntityDef();
            def.position = new Vec2(-6, 3);
            def.angle = 0;
            def.size = 3;

            entities.createEntity(def);
        }

        for (int x=0;x<2;x++) {
            for (int y = 0; y < 4; y++) {
                {
                    StaticEntityDef def = new StaticEntityDef();
                    def.position = new Vec2(
                            -worldWidth / 2f +  6f + x * 8,
                            worldHeight / 2f - 26f - y * 8f);
                    def.type = StaticEntityDef.CIRCLE;
                    def.radius = 1f;

                    entities.createEntity(def);
                }
            }
        }

        for (int x=0;x<2;x++) {
            for (int y = 0; y < 2; y++) {
                SpawnerEntityDef def = new SpawnerEntityDef();
                def.interval = 10f + x * 0.5f;
                def.position = new Vec2(
                        (-worldWidth / 2f) + 9 + x * 2,
                        worldHeight / 2f - 47f + y * 2);
                def.spawnDef = new ZombieEntityDef();

                entities.createEntity(def);
            }
        }

    }
}
