import com.sun.xml.internal.messaging.saaj.util.ByteOutputStream;
import engine.resources.animation.TextureSheetAnimationLoader;
import engine.resources.animation.specification.TextureSheetAnimationSpecification;
import engine.resources.bitmap.BitmapLoader;
import engine.resources.bitmap.PngImageLoader;
import engine.resources.texture.TextureLoader;
import engine.resources.texturesheet.TextureSheetLoader;
import engine.serialization.BinaryResourceDeserializer;
import engine.serialization.BinaryResourceSerializer;
import engine.serialization.DeserializationException;
import engine.serialization.LuaResourceDeserializer;
import org.luaj.vm2.parser.ParseException;
import org.lwjgl.LWJGLException;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.DisplayMode;

import java.io.ByteArrayInputStream;
import java.io.FileInputStream;
import java.io.IOException;

public class Test {
    public static void main(String[] args)
            throws ParseException, LWJGLException, IOException, NoSuchFieldException, InstantiationException, IllegalAccessException, DeserializationException, ClassNotFoundException {

        //StringReader ip = new StringReader(
        //        "{ texture = \"blood.png\", width = 200, height = 300, junk = { name = \"abc\", length = 3, stuffs = { \"q\", \"w\", \"e\" } }, mappings = { [\"z\"] = 5, [4] = 0, [2] = 1 } }");

        BitmapLoader bmpLoader = new BitmapLoader();
        bmpLoader.registerFormatLoader("png", new PngImageLoader());

        Display.setDisplayMode(new DisplayMode(400, 300));
        Display.create();

        //LuaResourceSerializer ls = new LuaResourceSerializer();

        LuaResourceDeserializer ldes = new LuaResourceDeserializer();
        BinaryResourceSerializer bser = new BinaryResourceSerializer();
        BinaryResourceDeserializer bdes = new BinaryResourceDeserializer();

        bdes.registerPrimitive(new TextureLoader(bmpLoader));
        bdes.registerPrimitive(new TextureSheetLoader(bdes));
        bdes.registerPrimitive(new TextureSheetAnimationLoader(bdes));

        ldes.registerPrimitive(new TextureLoader(bmpLoader));
        ldes.registerPrimitive(new TextureSheetLoader(ldes));
        ldes.registerPrimitive(new TextureSheetAnimationLoader(ldes));

        FileInputStream is = new FileInputStream("assets/animations/player_walk.animation");

        TextureSheetAnimationSpecification spec = (TextureSheetAnimationSpecification)ldes.deserialize(is, TextureSheetAnimationSpecification.class);
        ByteOutputStream os = new ByteOutputStream();

        bser.serialize(spec, os);

        os.flush();

        ByteArrayInputStream bis = new ByteArrayInputStream(os.getBytes());

        spec = (TextureSheetAnimationSpecification)bdes.deserialize(bis, TextureSheetAnimationSpecification.class);

        Display.destroy();
    }

}


